<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Index extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->library('excel');
		$this->load->model('login_model','login');
		$this->load->model('Query_Model','query');
		$this->load->model('Insert_Update_Model','in_up');
		$this->load->model('Admin_Model','admin');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->viewname = 'index';
		$this->load->library('googlemaps');

		$config['center'] = '24.9617, 121.2078';
		$config['zoom'] = 'auto';
		$this->googlemaps->initialize($config);

		$marker = array();
		// $marker['position'] = '25.021, 121.2656';
		$this->googlemaps->add_marker($marker);
		$data['map'] = $this->googlemaps->create_map();
		$this->load->view('includes/user_header',$data);
		$this->load->view('mainview',$data);
		$this->load->view('includes/footer');
	}

	// 登入
	public function loginpage(){
		$this->load->view('loginpage');
	}

	public function login(){
		$data = $this->input->post();
		$check_login = $this->login->checkacc($data);
		if($check_login){
			redirect(base_url('index/renovate'));
		}else{
			redirect(base_url());
		}
		
	}

	

	public function logout()
	{
		$this->login->logout();
	}


	// 工程履歷
	public function renovate(){
		$data['area_msg'] = $this->admin->get_area_msg();
		$this->load->view('includes/user_header');
		$this->load->view('user/renovate',$data);
		$this->load->view('includes/footer');
	}

	public function edit_renovate(){
		$data = $this->input->post();
		// $data_type = $this->input->post('list_type');
		$remsg = $this->in_up->edit_renovate($data);
		echo $remsg;
	}

	// 依照編號類型
	public function get_msg_for_id()
	{
		$notype = $this->input->post('atype');
		$id = $this->input->post('id');
		$msg = $this->query->get_msg_for_id($notype,$id);
		echo json_encode($msg);
	}

	// 資產列表
	public function asset(){
		$this->load->view('includes/user_header');
		$this->load->view('user/asset');
		$this->load->view('includes/footer');
	}

	// 新增資產
	public function addasset($no = null,$btype = null){
		if(!empty($no)){
			$data['item_msg'] = json_encode($this->query->get_msg_for_id($btype,$no));
			$data['item_type'] = $btype;
		}else{
			$data['item_msg'] = "''";
			$data['item_type'] =  "";
		}
		$data['area_msg'] = $this->admin->get_area_msg();
		$data['tree_type'] = $this->query->get_tree_type();
		$this->load->view('includes/user_header');
		$this->load->view('user/addasetview',$data);
		$this->load->view('includes/footer');
	}

	public function send_add_asset(){
		$data = $this->input->post();
		$remsg = $this->in_up->send_add_asset($data);
		echo $remsg;
	}

	// 取得所有資料
	public function get_msg()
	{
		$param = $this->input->post('param');
		if($param['type'] == 'road' ){
			$data['list'] = $this->query->get_list_road($param);
			$data['searchcount'] = count($this->query->get_list_road($param,'count'));
		}
		if($param['type'] == 'bridge' ){
			$data['list'] = $this->query->get_list_bridge($param);
			$data['searchcount'] = count($this->query->get_list_bridge($param,'count'));
		}
		if($param['type'] == 'light' ){
			$data['list'] = $this->query->get_list_light($param);
			$data['searchcount'] = $this->query->get_list_light($param,'count');
		}
		if($param['type'] == 'tree' ){
			$data['list'] = $this->query->get_list_tree($param);
			$data['searchcount'] = $this->query->get_list_tree($param,'count');
		}
		if($param['type'] == 'sidewalk' ){
			$data['list'] = $this->query->get_list_sidewalk($param);
			$data['searchcount'] = $this->query->get_list_sidewalk($param,'count');
		}
		if($param['type'] == 'groove' ){
			$data['list'] = $this->query->get_list_groove($param);
			$data['searchcount'] = $this->query->get_list_groove($param,'count');
			
		}
		if($param['type'] == 'cable' ){
			$data['list'] = $this->query->get_list_cable($param);
			$data['searchcount'] = $this->query->get_list_cable($param,'count');
		}

		$data['count'] = $this->query->get_list_count($param);
		
		echo json_encode($data);
	}
	public function get_msg_renovate()
	{
		$param = $this->input->post('param');
		if($param['type'] == 'road' ){
			$data['list'] = $this->query->get_list_road_renovate($param);
			$data['searchcount'] = count($this->query->get_list_road_renovate($param,'count'));
		}
		if($param['type'] == 'bridge' ){
			$data['list'] = $this->query->get_list_bridge_renovate($param);
			$data['searchcount'] = count($this->query->get_list_bridge_renovate($param,'count'));
		}
		if($param['type'] == 'light' ){
			$data['list'] = $this->query->get_list_light_renovate($param);
			$data['searchcount'] = $this->query->get_list_light_renovate($param,'count');
		}
		if($param['type'] == 'tree' ){
			$data['list'] = $this->query->get_list_tree_renovate($param);
			$data['searchcount'] = $this->query->get_list_tree_renovate($param,'count');
		}
		if($param['type'] == 'sidewalk' ){
			$data['list'] = $this->query->get_list_sidewalk_renovate($param);
			$data['searchcount'] = $this->query->get_list_sidewalk_renovate($param,'count');
		}
		if($param['type'] == 'groove' ){
			$data['list'] = $this->query->get_list_groove_renovate($param);
			$data['searchcount'] = $this->query->get_list_groove_renovate($param,'count');
			
		}
		if($param['type'] == 'cable' ){
			$data['list'] = $this->query->get_list_cable_renovate($param);
			$data['searchcount'] = $this->query->get_list_cable_renovate($param,'count');
		}

		$data['count'] = $this->query->get_list_count_renovate($param);
		
		echo json_encode($data);
	}

	// 編列預算

	public function money()
	{
		$data['budget_msg'] = $this->query->get_budget_list();
		$allmoney = 0;
		foreach ($data['budget_msg']->result_array() as $value) {
			$allmoney += (int)$value['budget_money'];
		}
		$data['allmoney'] = $allmoney;
		foreach ($data['budget_msg']->result_array() as $budget_val) {
			foreach (json_decode($budget_val['budget_quantity_msg'],true) as $key => $budget_val_list) {
				$detailed_msg[$budget_val['budget_type']][$budget_val_list] = $budget_val_list;
			}
			// $detailed_msg[$budget_val['budget_type']] = $budget_val['budget_quantity_msg'];
		}
		// var_dump($detailed_msg);exit;
		$data['detailed_msg'] = @$detailed_msg;
		$this->load->view('includes/user_header');
		$this->load->view('user/moneypage',$data);
		$this->load->view('includes/footer');
	}

	public function getsearch()
	{
		$data = $this->input->post();
		$msg = $this->query->get_search_msg($data);
		echo json_encode($msg);
	}

	public function send_money()
	{
		$data = $this->input->post();
		$msg = $this->in_up->addbudget($data);
		echo json_encode($msg);
	}

	// 已編列列表
	public function viewmoneylist()
	{
		$data['budget_msg'] = $this->query->get_budget_list();
		$allmoney = 0;
		foreach ($data['budget_msg']->result_array() as $value) {
			$allmoney += (int)$value['budget_money'];
		}
		$data['allmoney'] = $allmoney;
		$this->load->view('includes/user_header');
		$this->load->view('user/moneylist',$data);
		$this->load->view('includes/footer');
	}

	public function get_budget_msg()
	{
		$type = $this->input->post('type');
		$date_y = date('Y')-1911+1;
		$remsg = $this->query->get_budget_msg($type,$date_y);
		echo json_encode($remsg);
	}

	// 刪除已選取項目
	public function delete_money_detailed()
	{
		$id = $this->input->post('id');
		$type = $this->input->post('notype');
		if($this->in_up->delete_money_detailed($id,$type) == true)
		{
			echo true;
		}else{
			echo false;
		}
	}


	// 核定預算

	public function checkmoney()
	{
		$data['budget_msg'] = $this->query->get_budget_list();
		$allmoney = 0;
		foreach ($data['budget_msg']->result_array() as $value) {
			$allmoney += (int)$value['budget_money'];
		}
		$data['allmoney'] = $allmoney;
		$this->load->view('includes/user_header');
		$this->load->view('user/checkmoney',$data);
		$this->load->view('includes/footer');
	}

	public function get_year_list_msg()
	{
		// $id = $this->input->post('id');
		$type = $this->input->post('budtype');
		$msg = $this->query->get_year_list_msg($type);
		echo json_encode($msg);
	}

	// 設定

	public function profile(){
		$this->load->view('includes/user_header');
		$this->load->view('user/useredit');
		$this->load->view('includes/footer');
	}
	


	public function updata()
	{
		$this->load->view('includes/user_header');
		$this->load->view('code_import');
		$this->load->view('includes/footer');
	}

	public function import()
	{
		error_reporting(0);
		set_time_limit(0);
		$data = $this->input->post();
		$targetPath = '';

		if(is_array($_FILES)){
			if(is_uploaded_file($_FILES['file']['tmp_name'])){
				$sourcePath = $_FILES['file']['tmp_name'];
				$targetPath = FCPATH."uploads/import/".$data['filetype'].'-'.date('Ymd-His', time()).'.'.explode('.', $_FILES['file']['name'])[1];
				move_uploaded_file($sourcePath,$targetPath);
			}else{
				echo '未上傳檔案';
				exit;
			}
		}else{
			echo '未上傳檔案';
			exit;
		}

		$reader= PHPExcel_IOFactory::createReaderForFile($targetPath);
		$reader->setReadDataOnly(true);
		$excel= $reader->load($targetPath);
		$sheet = $excel->getSheet(0);
		// 道路
			if($data['filetype'] == "p1")
			{
				
			}
		// 橋梁
			if($data['filetype'] == "p2")
			{
				
			}
		// 路燈
			if($data['filetype'] == "p3")
			{
				$highestRow = $sheet->getHighestRow();
				echo '資料總筆數'.($highestRow-1)."\r\n";
				$errorcount = 0;
				for ($row = 2; $row <= $highestRow; $row++){
					$isError = false;
					$error_text = '';

					$data['light_objectid'] = $sheet->getCellByColumnAndRow(0, $row)->getValue();
					$data['light_sysid'] = $sheet->getCellByColumnAndRow(1, $row)->getValue();
					$data['light_numid'] = $sheet->getCellByColumnAndRow(2, $row)->getValue();
					$data['light_road_name'] = $sheet->getCellByColumnAndRow(3, $row)->getValue();
					$data['light_m'] = $sheet->getCellByColumnAndRow(4, $row)->getValue();
					$data['light_xcode'] = $sheet->getCellByColumnAndRow(5, $row)->getValue();
					$data['light_ycode'] = $sheet->getCellByColumnAndRow(6, $row)->getValue();
					$data['light_town'] = $sheet->getCellByColumnAndRow(7, $row)->getValue();
					$data['light_village'] = $sheet->getCellByColumnAndRow(8, $row)->getValue();
					$data['light_search_date'] = $sheet->getCellByColumnAndRow(9, $row)->getValue();

					if($data['light_objectid'] == '')
					{
						$error_text .= "未填寫OBJECTID"." ";
						$isError = true;
					}
					if($data['light_road_name'] == '')
					{
						$error_text .= "未填寫路名"." ";
						$isError = true;
					}

					if(!$isError){
						if($this->query->get_objectid('light',$data['light_objectid']) > 0){
							//echo "此填造單位代碼已經存在"." ";
							echo "第".$row."行".":此OBJECTID已經存在\r\n";
							$errorcount++;
						}else{
							
							$insert_rn = $this->in_up->insert_light($data);
							if($insert_rn == true){
								//echo "匯入成功"." "."機關名稱：".$organ_data["organ_name"]." "."填造單位代碼".$data["code_write_number"]." "."填造單位名稱".$data["code_write_name"]." ";
							}else{
								echo "新增時出現錯誤"." ";
								$errorcount++;
							}
						}
					}else{
						echo "第".$row."行".":".$error_text."\r\n";
						$errorcount++;
					}
				}
			}
		// 路樹
			if($data['filetype'] == "p4")
			{
				$highestRow = $sheet->getHighestRow();
				echo '資料總筆數'.($highestRow-1)."\r\n";
				$errorcount = 0;
				for ($row = 2; $row <= $highestRow; $row++){
					$isError = false;
					$error_text = '';

					$data['tree_objectid'] = $sheet->getCellByColumnAndRow(0, $row)->getValue();
					$data['tree_no'] = $sheet->getCellByColumnAndRow(1, $row)->getValue();
					$data['tree_type_name'] = $sheet->getCellByColumnAndRow(2, $row)->getValue();
					$data['tree_diameter'] = $sheet->getCellByColumnAndRow(3, $row)->getValue();
					$data['tree_high'] = $sheet->getCellByColumnAndRow(4, $row)->getValue();
					$data['tree_crown'] = $sheet->getCellByColumnAndRow(5, $row)->getValue();
					$data['tree_surverDate'] = $sheet->getCellByColumnAndRow(6, $row)->getValue();
					$data['tree_county'] = $sheet->getCellByColumnAndRow(7, $row)->getValue();
					$data['tree_road'] = $sheet->getCellByColumnAndRow(8, $row)->getValue();
					$data['tree_growing_type'] = $sheet->getCellByColumnAndRow(9, $row)->getValue();
					$data['tree_manageunit'] = $sheet->getCellByColumnAndRow(10, $row)->getValue();
					$data['tree_url'] = $sheet->getCellByColumnAndRow(11, $row)->getValue();
					if($data['tree_objectid'] == '')
					{
						$error_text .= "未填寫OBJECTID"." ";
						$isError = true;
					}
					if($data['tree_no'] == '')
					{
						$error_text .= "未填寫樹籍編號"." ";
						$isError = true;
					}
					if($data['tree_type_name'] == '')
					{
						$error_text .= "未填寫樹種類型"." ";
						$isError = true;
					}
					if($data['tree_road'] == '')
					{
						$error_text .= "未填寫路段"." ";
						$isError = true;
					}

					if(!$isError){
						if($this->query->get_objectid('tree',$data['tree_objectid']) > 0){
							//echo "此填造單位代碼已經存在"." ";
							echo "第".$row."行".":此OBJECTID已經存在\r\n";
							$errorcount++;
						}else{
							
							$insert_rn = $this->in_up->insert_tree($data);
							if($insert_rn == true){
								echo "匯入成功";
								//echo "匯入成功"." "."機關名稱：".$organ_data["organ_name"]." "."填造單位代碼".$data["code_write_number"]." "."填造單位名稱".$data["code_write_name"]." ";
							}else{
								echo "新增時出現錯誤"." ";
								$errorcount++;
							}
						}
					}else{
						echo "第".$row."行".":".$error_text."\r\n";
						$errorcount++;
					}
				}
			}
		// 人行道
			if($data['filetype'] == "p5")
			{
				$highestRow = $sheet->getHighestRow();
				echo '資料總筆數'.($highestRow-1)."\r\n";
				$errorcount = 0;
				for ($row = 2; $row <= $highestRow; $row++){
					$isError = false;
					$error_text = '';

					$data['sw_objectid'] = $sheet->getCellByColumnAndRow(0, $row)->getValue();
					$data['sw_serial_num'] = $sheet->getCellByColumnAndRow(1, $row)->getValue();
					$data['sw_road_name'] = $sheet->getCellByColumnAndRow(2, $row)->getValue();
					$data['sw_road_start'] = $sheet->getCellByColumnAndRow(3, $row)->getValue();
					$data['sw_road_end'] = $sheet->getCellByColumnAndRow(4, $row)->getValue();
					$data['sw_road_direction'] = $sheet->getCellByColumnAndRow(5, $row)->getValue();
					$data['sw_length'] = $sheet->getCellByColumnAndRow(6, $row)->getValue();
					$data['sw_width_u'] = $sheet->getCellByColumnAndRow(7, $row)->getValue();
					$data['sw_width_c'] = $sheet->getCellByColumnAndRow(8, $row)->getValue();
					$data['sw_leng'] = $sheet->getCellByColumnAndRow(9, $row)->getValue();
					$data['sw_wth'] = $sheet->getCellByColumnAndRow(10, $row)->getValue();
					$data['swd_wth'] = $sheet->getCellByColumnAndRow(11, $row)->getValue();
					$data['swt_wth'] = $sheet->getCellByColumnAndRow(12, $row)->getValue();
					$data['sww_wth'] = $sheet->getCellByColumnAndRow(13, $row)->getValue();
					$data['sw_pave'] = $sheet->getCellByColumnAndRow(14, $row)->getValue();
					$data['shape_ar'] = $sheet->getCellByColumnAndRow(15, $row)->getValue();
					$data['sw_area'] = $sheet->getCellByColumnAndRow(16, $row)->getValue();
					$data['sw_bk_l'] = $sheet->getCellByColumnAndRow(17, $row)->getValue();
					$data['sw_bk_b'] = $sheet->getCellByColumnAndRow(18, $row)->getValue();
					$data['sw_bk_e'] = $sheet->getCellByColumnAndRow(19, $row)->getValue();
					$data['sw_bk_p'] = $sheet->getCellByColumnAndRow(20, $row)->getValue();
					$data['sw_bk_o'] = $sheet->getCellByColumnAndRow(21, $row)->getValue();
					$data['sw_hole'] = $sheet->getCellByColumnAndRow(22, $row)->getValue();
					$data['sw_tree'] = $sheet->getCellByColumnAndRow(23, $row)->getValue();
					$data['sw_ramp'] = $sheet->getCellByColumnAndRow(24, $row)->getValue();
					$data['sw_carramp'] = $sheet->getCellByColumnAndRow(25, $row)->getValue();
					$data['sw_blok_name'] = $sheet->getCellByColumnAndRow(26, $row)->getValue();
					$data['sw_arcade'] = $sheet->getCellByColumnAndRow(27, $row)->getValue();
					$data['sw_ac_even'] = $sheet->getCellByColumnAndRow(28, $row)->getValue();
					$data['area_name'] = $sheet->getCellByColumnAndRow(29, $row)->getValue();
					$data['sw_theme'] = $sheet->getCellByColumnAndRow(30, $row)->getValue();
					$data['sw_cost'] = $sheet->getCellByColumnAndRow(31, $row)->getValue();
					$data['sw_score'] = $sheet->getCellByColumnAndRow(32, $row)->getValue();
					if($data['sw_objectid'] == '')
					{
						$error_text .= "未填寫OBJECTID"." ";
						$isError = true;
					}
					if($data['sw_road_name'] == '')
					{
						$error_text .= "未填寫道路名稱"." ";
						$isError = true;
					}
					if($data['sw_road_start'] == '')
					{
						$error_text .= "未填寫道路起點型"." ";
						$isError = true;
					}
					if($data['sw_road_end'] == '')
					{
						$error_text .= "未填寫道路迄點"." ";
						$isError = true;
					}
					if($data['sw_road_direction'] == '')
					{
						$error_text .= "未填寫道路方向"." ";
						$isError = true;
					}

					if(!$isError){
						if($this->query->get_objectid('sidewalk',$data['sw_objectid']) > 0){
							//echo "此填造單位代碼已經存在"." ";
							echo "第".$row."行".":此OBJECTID已經存在\r\n";
							$errorcount++;
						}else{
							
							$insert_rn = $this->in_up->insert_sw($data);
							if($insert_rn == true){
								//echo "匯入成功"." "."機關名稱：".$organ_data["organ_name"]." "."填造單位代碼".$data["code_write_number"]." "."填造單位名稱".$data["code_write_name"]." ";
							}else{
								echo "新增時出現錯誤"." ";
								$errorcount++;
							}
						}
					}else{
						echo "第".$row."行".":".$error_text."\r\n";
						$errorcount++;
					}
				}
				
			}
		// 邊溝
			if($data['filetype'] == "p6")
			{
				$highestRow = $sheet->getHighestRow();
				echo '資料總筆數'.($highestRow-1)."\r\n";
				$errorcount = 0;
				for ($row = 2; $row <= $highestRow; $row++){
					$isError = false;
					$error_text = '';

					$data['groove_objectid'] = $sheet->getCellByColumnAndRow(0, $row)->getValue();
					$data['groove_sysid'] = $sheet->getCellByColumnAndRow(1, $row)->getValue();
					$data['groove_numid'] = $sheet->getCellByColumnAndRow(2, $row)->getValue();
					$data['groove_road'] = $sheet->getCellByColumnAndRow(3, $row)->getValue();
					$data['groove_length'] = $sheet->getCellByColumnAndRow(4, $row)->getValue();
					$data['groove_width'] = $sheet->getCellByColumnAndRow(5, $row)->getValue();
					$data['groove_direction'] = $sheet->getCellByColumnAndRow(6, $row)->getValue();
					$data['groove_type'] = $sheet->getCellByColumnAndRow(7, $row)->getValue();
					$data['groove_m'] = $sheet->getCellByColumnAndRow(8, $row)->getValue();
					$data['groove_town'] = $sheet->getCellByColumnAndRow(9, $row)->getValue();
					$data['groove_village'] = $sheet->getCellByColumnAndRow(10, $row)->getValue();
					$data['groove_search_year'] = $sheet->getCellByColumnAndRow(11, $row)->getValue();
					if($data['groove_objectid'] == '')
					{
						$error_text .= "未填寫OBJECTID"." ";
						$isError = true;
					}
					if($data['groove_road'] == '')
					{
						$error_text .= "未填寫道路名稱"." ";
						$isError = true;
					}
					if($data['groove_town'] == '')
					{
						$error_text .= "未填寫區域名稱"." ";
						$isError = true;
					}
					if($data['groove_village'] == '')
					{
						$error_text .= "未填寫村里名"." ";
						$isError = true;
					}

					if(!$isError){
						if($this->query->get_objectid('groove',$data['groove_objectid']) > 0){
							//echo "此填造單位代碼已經存在"." ";
							echo "第".$row."行".":此OBJECTID已經存在\r\n";
							$errorcount++;
						}else{
							
							$insert_rn = $this->in_up->insert_groove($data);
							if($insert_rn == true){
								//echo "匯入成功"." "."機關名稱：".$organ_data["organ_name"]." "."填造單位代碼".$data["code_write_number"]." "."填造單位名稱".$data["code_write_name"]." ";
							}else{
								echo "新增時出現錯誤"." ";
								$errorcount++;
							}
						}
					}else{
						echo "第".$row."行".":".$error_text."\r\n";
						$errorcount++;
					}
				}
			}
		// 天空纜線
			if($data['filetype'] == "p7")
			{
				
			}
	}

	




}
