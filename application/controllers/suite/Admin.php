<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('Admin_Model','admin');
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	//  會員管理
	public function index()
	{
		$this->pagename = '會員列表';
		$data['userlist'] = $this->admin->get_user_msg();
		$data['alluserlist'] = array();
		$alluser =  $this->admin->get_user_msg();
		foreach($alluser as $user_code){
			$data['alluserlist'][$user_code['user_id']] = $user_code;
		}

		$data['area_list'] = $this->admin->get_area_msg();
		$data['group_list'] = $this->admin->get_group_msg();
		$this->load->view('admin/includes/header');
		$this->load->view('admin/userpage',$data);
		$this->load->view('admin/includes/footer');
	}


	public function user_send()
	{
		$data = $this->input->post();
		if($this->admin->user_send($data)){
			echo true;
		}else{
			echo false;
		}
	}

	public function get_user_area()
	{
		$user_id = $this->input->post('user_id');
		$msg = $this->admin->get_area_froid($user_id);
		echo json_encode($msg);
	}
	// 停權
	public function change_user_status()
	{
		$user_id = $this->input->post('user_id');
		$type = $this->input->post('change_type');
		if($this->admin->user_status_change($user_id,$type)){
			echo true;
		}else{
			echo false;
		}
	}

	// 群組管理
	public function group()
	{
		$this->pagename = '群組列表';
		$data['group_msg'] = $this->admin->get_group_msg();
		$data['allgrouplist'] = array();
		$allgroup =  $this->admin->get_group_msg();
		foreach($allgroup as $group_code){
			$data['allgrouplist'][$group_code['group_id']] = $group_code;
		}
		$this->load->view('admin/includes/header');
		$this->load->view('admin/grouppage',$data);
		$this->load->view('admin/includes/footer');
	}

	// 群組send
	public function group_send()
	{
		$data = $this->input->post();
		if($this->admin->group_send($data)){
			echo true;
		}else{
			echo false;
		}
	}

	// 群組 刪除
	public function group_del()
	{
		$id = $this->input->post('group_id');
		if($this->admin->delete_msg('sys_group','group_id',$id)){
			echo true;
		}else{
			echo false;
		}
	}



	
	// 區域管理
	public function area()
	{
		$this->pagename = '區域列表';
		$data['area_msg'] = $this->admin->get_area_msg();
		$this->load->view('admin/includes/header');
		$this->load->view('admin/areapage',$data);
		$this->load->view('admin/includes/footer');
	}


	// 更新區域資料
	public function update_area_msg()
	{
		$json_string = file_get_contents("https://data.tycg.gov.tw/api/v1/rest/datastore/72d64eff-88f1-4788-9da4-12ba5a6ab469?format=json&limit=600");
		$result = json_decode($json_string, true);
		if($result['success'] == true){
			$update_msg = $result['result']['records'];
			if($this->admin->update_area_msg($update_msg))
			{
				echo json_encode(true);
			}else{
				echo json_encode(false);
			}
		}
	}

}
