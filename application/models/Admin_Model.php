<?php
class Admin_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	public function get_user_msg(){
		$this->db->select('*');
		$this->db->from('sys_user');
		$this->db->join('sys_group','sys_user.user_group = sys_group.group_id');
		$query = $this->db->get();
		$result = $query->result_array();
		return $result;
	}

	public function user_send($data)
	{
		$user_array = array(
			'user_name' => $data['user_name'],
			'user_account' => $data['user_account'],
			'user_status' => 'Y',
			'user_group' => $data['user_group']
		);
		if($data['user_pwd'] != null){
			$user_array['user_pwd'] = password_hash($data['user_pwd'],PASSWORD_DEFAULT);
		}
		$this->db->trans_start();
		if($data['user_type'] == 'add'){
			// check account
			$this->db->select('*');
			$this->db->from('sys_user');
			$this->db->where('user_account',$data['user_account']);
			$query = $this->db->get();
			$row = $query->num_rows();
			if($row >0){
				return false;
			}else{
				$user_array['user_create_date'] = date('Y-m-d H:i:s');
				$user_array['user_update_date'] = date('Y-m-d H:i:s');
				$this->db->insert('sys_user',$user_array);

				$user_id = $this->db->insert_id();
				foreach ($data['area_name'] as $area_val) {
					$area_val_array = array(
						'user_id' => $user_id,
						'area_id' => $area_val
					);
					$this->db->insert('sys_user_area',$area_val_array);
				}

			}
		}else{
			$user_array['user_update_date'] = date('Y-m-d H:i:s');
			$this->db->where('user_id',$data['user_id']);
			$this->db->update('sys_user',$user_array);

			$this->db->where('user_id',$data['user_id']);
			$this->db->delete('sys_user_area');
			if(!empty($data['area_name'])){
				foreach ($data['area_name'] as $area_val) {
					$area_val_array = array(
						'user_id' => $data['user_id'],
						'area_id' => $area_val
					);
					$this->db->insert('sys_user_area',$area_val_array);
				}
			}
			

		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}

	public function user_status_change($user_id,$type)
	{
		
		$this->db->trans_start();

		$this->db->where('user_id',$user_id);
		$this->db->set('user_status',$type);
		$this->db->update('sys_user');

		
		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}


	public function get_area_froid($user_id)
	{
		$this->db->select('*');
		$this->db->from('sys_user_area');
		$this->db->where('user_id',$user_id);
		$query = $this->db->get();
		$result = $query->result_array();
		$remsg = array();
		foreach ($result as $value) {
			$remsg[] = $value['area_id'];
		}
		// log_message('error',print_r($remsg,true));
		return $remsg;
	}
	// group
	public function get_group_msg(){
		$query = $this->db->get('sys_group');
		$result = $query->result_array();
		return $result;
	}

	public function group_send($data){
		$group_array = array(
			'group_name' => $data['group_name'],
			'group_status' => 'Y',
			'group_msg' => json_encode($data['pagecheckbox'])
		);
		
		$this->db->trans_start();

		if($data['group_type'] == 'add'){
			// check name
			$this->db->select('*');
			$this->db->from('sys_group');
			$this->db->where('group_name',$data['group_name']);
			$query = $this->db->get();
			$row = $query->num_rows();
			if($row > 0){
				return false;
			}else{
				$group_array['group_create_date'] = date('Y-m-d H:i:s');
				$this->db->insert('sys_group',$group_array);
			}
		}else{
			
			$group_array['group_update_date'] = date('Y-m-d H:i:s');
			$this->db->where('group_id',$data['group_id']);
			$this->db->update('sys_group',$group_array);
		}

		if ($this->db->trans_status() === FALSE)
		{
			$this->db->trans_rollback();
			return false;
		}
		else
		{
			$this->db->trans_commit();
			return true;
		}
	}



	// delete msg
	public function delete_msg($tablename,$field,$id){
		
		$this->db->trans_start();

		$this->db->where($field,$id);
		$this->db->delete($tablename);

		
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}
	// area
	public function get_area_msg(){
		$query = $this->db->get('sys_area');
		$result = $query->result_array();
		return $result;
	}
	
	public function update_area_msg($msg = array()){
		$this->db->trans_start();
		$this->db->truncate('sys_area');
		foreach ($msg as $key => $value) {

			$insert_msg = array(
				'area_name' => $value['區別'],
				// 'village_name' => $value['里別']
			);
			// check name 
			$this->db->select("*");
			$this->db->from('sys_area');
			$this->db->where('area_name',$insert_msg['area_name']);
			$query = $this->db->get();
			$row = $query->num_rows();
			if($row == 0 ){
				$this->db->insert('sys_area',$insert_msg);
			}
			
		}
		
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}

}