<?php
class Query_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}
	// 工程履歷
		// 橋梁
		public function get_list_bridge_renovate($param,$counttype = null)
		{
			$this->db->select('bridge_id,name,river,bridge_town,bridge_deru,bridge_test_date');
			$this->db->from('facility_'.$param['type']);
			if($param['search'] != ''){
				$this->db->like('bridge_id',$param['search']);
				$this->db->or_like('name',$param['search']);
				$this->db->or_like('river',$param['search']);
				$this->db->or_like('bridge_deru',$param['search']);
				$this->db->or_like('bridge_test_date',$param['search']);
				$this->db->or_like('bridge_town',$param['search']);
			}
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		// 路燈
		public function get_list_light_renovate($param,$counttype=null)
		{
			
			$this->db->select('light_id,light_numid,light_town,light_road_name,light_test_date');
			$this->db->from('facility_'.$param['type']);
			
			if($param['search'] != ''){
				$this->db->like('light_id',$param['search']);
				$this->db->or_like('light_numid',$param['search']);
				$this->db->or_like('light_town',$param['search']);
				$this->db->or_like('light_road_name',$param['search']);
				$this->db->or_like('light_test_date',$param['search']);
			}
			
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		// 路樹
		public function get_list_tree_renovate($param,$counttype=null)
		{
			
			$this->db->select('tree_id,tree_no,tree_town,tree_road,tree_type_name,tree_manageunit,tree_test_date,');
			$this->db->from('facility_'.$param['type']);
			
			if($param['search'] != ''){
				$this->db->like('tree_id',$param['search']);
				$this->db->or_like('tree_no',$param['search']);
				$this->db->or_like('tree_town',$param['search']);
				$this->db->or_like('tree_road',$param['search']);
				$this->db->or_like('tree_type_name',$param['search']);
				$this->db->or_like('tree_manageunit',$param['search']);
				$this->db->or_like('tree_test_date',$param['search']);
			}
			
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		// 人行道
		public function get_list_sidewalk_renovate($param,$counttype=null)
		{
			$this->db->select('sidewalk_id,sw_town,sw_road_name,sw_road_start,sw_road_end,sww_wth,sw_pave,sidewalk_fci,sidwealk_test_date');
			$this->db->from('facility_'.$param['type']);
			if($param['search'] != ''){
				$this->db->like('sidewalk_id',$param['search']);
				$this->db->or_like('sw_town',$param['search']);
				$this->db->or_like('sidewalk_road_name',$param['search']);
				$this->db->or_like('sidewalk_road_start',$param['search']);
				$this->db->or_like('sw_road_end',$param['search']);
				$this->db->or_like('sww_wth',$param['search']);
				$this->db->or_like('sw_pave',$param['search']);
				$this->db->or_like('sidewalk_fci',$param['search']);
				$this->db->or_like('sidwealk_test_date',$param['search']);
			}
			
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		
		// 邊溝
		public function get_list_groove_renovate($param,$counttype=null)
		{
			$this->db->select('groove_id,groove_numid,groove_road,groove_town,groove_village,groove_type,groove_test_date');
			$this->db->from('facility_'.$param['type']);
			if($param['search'] != ''){
				$this->db->like('groove_id',$param['search']);
				$this->db->or_like('groove_numid',$param['search']);
				$this->db->or_like('groove_road',$param['search']);
				$this->db->or_like('groove_town',$param['search']);
				$this->db->or_like('groove_village',$param['search']);
				$this->db->or_like('groove_type',$param['search']);
				$this->db->or_like('groove_test_date',$param['search']);
			}
			
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			$result = array();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		public function get_list_count_renovate($param)
		{
			$this->db->select('count('.$param['type'].'_id) as count');
			$this->db->from('facility_'.$param['type']);
			$query = $this->db->get();
			$num = $query->row_array();
			return $num['count'];

		}

		public function get_msg_for_id($tablename,$id)
		{
			$this->db->select('*');
			$this->db->from('facility_'.$tablename);
			$this->db->where($tablename.'_id',$id);
			$query = $this->db->get();
			$row = $query->row_array();
			return $row;
		}
	// 資產列表
		// 橋梁
		public function get_list_bridge($param,$counttype = null)
		{
			$this->db->select('bridge_id,name,river,bridge_town,bridge_build_cost,bridge_repair_cost');
			$this->db->from('facility_'.$param['type']);
			if($param['search'] != ''){
				$this->db->like('bridge_id',$param['search']);
				$this->db->or_like('name',$param['search']);
				$this->db->or_like('river',$param['search']);
				$this->db->or_like('bridge_build_cost',$param['search']);
				$this->db->or_like('bridge_repair_cost',$param['search']);
				$this->db->or_like('bridge_town',$param['search']);
			}
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		// 路燈
		public function get_list_light($param,$counttype=null)
		{
			
			$this->db->select('light_id,light_numid,light_town,light_road_name,light_build_cost,light_repair_cost');
			$this->db->from('facility_'.$param['type']);
			
			if($param['search'] != ''){
				$this->db->like('light_id',$param['search']);
				$this->db->or_like('light_numid',$param['search']);
				$this->db->or_like('light_town',$param['search']);
				$this->db->or_like('light_road_name',$param['search']);
				$this->db->or_like('light_build_cost',$param['search']);
				$this->db->or_like('light_repair_cost',$param['search']);
			}
			
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		// 路樹
		public function get_list_tree($param,$counttype=null)
		{
			
			$this->db->select('tree_id,tree_no,tree_town,tree_road,tree_type_name,tree_manageunit,tree_build_cost,tree_repair_cost');
			$this->db->from('facility_'.$param['type']);
			
			if($param['search'] != ''){
				$this->db->like('tree_id',$param['search']);
				$this->db->or_like('tree_no',$param['search']);
				$this->db->or_like('tree_town',$param['search']);
				$this->db->or_like('tree_road',$param['search']);
				$this->db->or_like('tree_type_name',$param['search']);
				$this->db->or_like('tree_manageunit',$param['search']);
				$this->db->or_like('tree_build_cost',$param['search']);
				$this->db->or_like('tree_repair_cost',$param['search']);
			}
			
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		// 人行道
		public function get_list_sidewalk($param,$counttype=null)
		{
			$this->db->select('sidewalk_id,sw_town,sw_road_name,sw_road_start,sw_road_end,sww_wth,sw_pave,sidewalk_build_cost,sidewalk_repair_cost');
			$this->db->from('facility_'.$param['type']);
			if($param['search'] != ''){
				$this->db->like('sidewalk_id',$param['search']);
				$this->db->or_like('sw_town',$param['search']);
				$this->db->or_like('sidewalk_road_name',$param['search']);
				$this->db->or_like('sidewalk_road_start',$param['search']);
				$this->db->or_like('sw_road_end',$param['search']);
				$this->db->or_like('sww_wth',$param['search']);
				$this->db->or_like('sw_pave',$param['search']);
				$this->db->or_like('sw_repair_cost',$param['search']);
				$this->db->or_like('sw_build_cost',$param['search']);
			}
			
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		// 邊溝
		public function get_list_groove($param,$counttype=null)
		{
			$this->db->select('groove_id,groove_numid,groove_road,groove_town,groove_village,groove_type,groove_build_cost,groove_repair_cost');
			$this->db->from('facility_'.$param['type']);
			if($param['search'] != ''){
				$this->db->like('groove_id',$param['search']);
				$this->db->or_like('groove_numid',$param['search']);
				$this->db->or_like('groove_road',$param['search']);
				$this->db->or_like('groove_town',$param['search']);
				$this->db->or_like('groove_village',$param['search']);
				$this->db->or_like('groove_type',$param['search']);
				$this->db->or_like('groove_build_cost',$param['search']);
				$this->db->or_like('groove_repair_cost',$param['search']);
			}
			
			if($counttype == null){
				$this->db->limit($param['limit'],$param['start']);
			}
			// if($tabname == 'facility_tree'){
			// 	$this->db->join('facility_tree_type','facility_tree_type.tree_type_id = facility_tree.tree_type_id');
			// }
			$query = $this->db->get();
			$result = array();
			foreach($query->result_array() as $key => $row){
				$result[$key] = array_values($row);
			}
			return $result;
		}
		public function get_list_count($param)
		{
			$this->db->select('count('.$param['type'].'_id) as count');
			$this->db->from('facility_'.$param['type']);
			$query = $this->db->get();
			$num = $query->row_array();
			return $num['count'];

		}

		public function get_objectid($tabname,$objectid)
		{
			$this->db->select('*');
			$this->db->from('facility_'.$tabname);
			$this->db->where($tabname.'_objectid',$objectid);
			$query = $this->db->get();
			$num = $query->num_rows();
			return $num;
		}

		public function get_tree_type()
		{
			$query = $this->db->get('facility_tree_type');
			$result = $query->result_array();
			return $result;
		}
	// 編列預算
		// 取得資料
		public function get_search_msg($data)
		{
			
			switch ($data['money_type']) {
				case '1':
					$tabname = 'road';
					break;
				
				case '2':
					$tabname = 'bridge';
					break;
				
				case '3':
					$tabname = 'light';
					break;
				
				case '4':
					$tabname = 'tree';
					break;
				
				case '5':
					$tabname = 'sidewalk';
					break;
				
				case '6':
					$tabname = 'groove';
					break;
				
				default:
					$tabname = 'cable';
					break;
			}
			
			if($data['r1'] == 'money_no'){
				$money['type'] = null;
			}elseif($data['r1'] == 'money_ase'){
				$money['type'] = 'ase';
			}elseif($data['r1'] == 'money_desc'){
				$money['type'] = 'desc';
			}else{
				$money['type'] = 'interval';
				$money['s'] = $data['money_interval_s'];
				$money['e'] = $data['money_interval_e'];
			}

			if($data['r2'] == 'score_no'){
				$score['type'] = null;
			}elseif($data['r2'] == 'score_ase'){
				$score['type'] = 'ase';
			}elseif($data['r2'] == 'score_desc'){
				$score['type'] = 'desc';
			}else{
				$score['type'] = 'interval';
				$score['s'] = $data['score_interval_s'];
				$score['e'] = $data['score_interval_e'];
			}

			if($data['limit_num'] == '0'){
				$limit = null;
			}else{
				$limit = $data['limit_num'];
			}

			$remsg = $this->get_money_msg($tabname,$money,$score,$limit);
			return $remsg;
		}

		public function get_money_msg($tabname,$money,$score,$limit)
		{
			/**
			 * 名稱 行政區 分數 維修金額
			 * 
			 */
			 
			$this->db->select('*');
			$this->db->from('facility_'.$tabname);
			if($money['type'] == 'interval')
			{
				if(!empty($money['s'])){
					$this->db->where($tabname.'_repair_cost >=',$money['s']);
				}
				if(!empty($money['e'])){
					$this->db->where($tabname.'_repair_cost <=',$money['e']);
				}
				
			}elseif($money['type'] == 'ase'){
				$this->db->order_by($tabname.'_repair_cost','DESC');
			}elseif($money['type'] == 'desc'){
				$this->db->order_by($tabname.'_repair_cost','ASC');
			}

			if($score['type'] == 'interval'){
				if(!empty($score['s'])){
					$this->db->where($tabname.'_score >=',$score['s']);
				}
				if(!empty($score['e'])){
					$this->db->where($tabname.'_score <=',$score['e']);
				}
			}elseif($score['type'] == 'ase'){
				$this->db->order_by($tabname.'_score','DESC');
			}elseif($score['type'] == 'desc'){
				$this->db->order_by($tabname.'_score','ASC');
			}

			$query = $this->db->get();
			$result = $query->result_array();
			return $result;
			
		}

		public function get_row_msg($tabname,$id)
		{
			$this->db->select('*');
			$this->db->from('facility_'.$tabname);
			$this->db->where($tabname.'_id',$id);
			$query  = $this->db->get();
			$row = $query->row_array();
			return $row;

		}

		/**
		 * 
		 * 年度		類型查詢是否已建立
		 * 
		 */

		 public function get_budget($type,$year)
		 {
			 $this->db->select('*');
			 $this->db->from('sys_budget');
			 $this->db->where('budget_type',$type);
			 $this->db->where('budget_year',$year);
			 $query = $this->db->get();
			 return $query;
		 }

		 public function get_budget_list()
		 {
			 $this->db->select('*');
			 $this->db->from('sys_budget');
			 $this->db->where('budget_year',date('Y')-1911+1);
			 $query = $this->db->get();
			 
			 return $query;
		 }

		/**
		 * 
		 * 類別、年度查詢單筆資料
		 * 
		 */
		public function get_budget_msg($type,$year)
		{
			$this->db->select('*');
			$this->db->from('sys_budget');
			$this->db->where('budget_type',$type);
			$this->db->where('budget_year',$year);
			$query = $this->db->get();
			$row = $query->row_array();

			$quantity = json_decode($row['budget_quantity_msg'],true);
			
			$msg_ary = array();

			foreach ($quantity as $value) {
				$msg = $this->get_row_msg($row['budget_type'],$value);
				$msg_ary[$msg[$row['budget_type'].'_id']] = $msg;
			}
			return $msg_ary;
		}
	// 核定預算
		public function get_year_list_msg($type)
		{
			$this->db->select('*');
			$this->db->from('sys_budget');
			$this->db->where('budget_type',$type);
			// $this->db->where('budget_id',$id);
			$this->db->order_by('budget_year','desc');
			$query = $this->db->get();
			$result = $query->result_array();
			return $result;
		}
}