<?php
class Insert_Update_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
		$this->load->model('Query_Model','query');
	}


	// 工程履歷
	public function edit_renovate($data){
		// $updata = array();
		$this->db->trans_begin();
		switch ($data['list_type']) {
			case 'road':
				# code...
				break;
			case 'bridge':
				$updata = array(
					'bridge_deru' => $data['bridge_deru'],
					'bridge_test_date' => $data['bridge_test_date'],
				);
				$this->db->where('bridge_id',$data['id']);
				$this->db->update('facility_'.$data['list_type'],$updata);
			case 'light':
				$updata = array(
					'light_test_date' => $data['light_test_date'],
				);
				$this->db->where('light_id',$data['id']);
				$this->db->update('facility_'.$data['list_type'],$updata);
				break;
			case 'tree':
				$updata = array(
					'tree_test_date' => $data['tree_test_date'],
				);
				$this->db->where('tree_id',$data['id']);
				$this->db->update('facility_'.$data['list_type'],$updata);
				break;
			case 'sidewalk':
				$updata = array(
					'sidwealk_fci' => $data['sidwealk_fci'],
					'sidwealk_test_date' => $data['sidwealk_test_date'],
				);
				$this->db->where('sidwealk_id',$data['id']);
				$this->db->update('facility_'.$data['list_type'],$updata);
				break;
			case 'groove':
				$updata = array(
					'groove_test_date' => $data['groove_test_date'],
				);
				$this->db->where('groove_id',$data['id']);
				$this->db->update('facility_'.$data['list_type'],$updata);
				break;
			case 'cable':
				break;
			default:
				return false;
				break;
		}
		
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}

	// 預算
	public function addbudget($data)
	{
		
		$this->db->trans_begin();

		// 檢查是否有編列過
		$year = date('Y')-1911+1;
		$allmoney = 0;
		$get_query = $this->query->get_budget($data['send_type'],$year);

		if($data['send_type'] == 'road'){
			$type_cn = '道路';
		}
		if($data['send_type'] == 'bridge'){
			$type_cn = '橋梁';
		}
		if($data['send_type'] == 'light'){
			$type_cn = '路燈';
		}
		if($data['send_type'] == 'tree'){
			$type_cn = '路樹';
		}
		if($data['send_type'] == 'sidewalk'){
			$type_cn = '人行道';
		}
		if($data['send_type'] == 'groove'){
			$type_cn = '側溝';
		}
		if($data['send_type'] == 'cable'){
			$type_cn = '天空纜線';
		}
		if($get_query->num_rows() > 0){
			$get_row = $get_query->row_array();
			$t_quantity_msg = json_decode($get_row['budget_quantity_msg']);
			foreach ($data['selects'] as $value) {
				if(!in_array($value,$t_quantity_msg)){
					array_push($t_quantity_msg,$value);
				}
			}
			
			$quantity = count($t_quantity_msg);
			$quantity_msg = json_encode($t_quantity_msg);
			foreach ($t_quantity_msg as $val) {
				$allmoney += (int)$this->query->get_row_msg($data['send_type'],$val)[$data['send_type'].'_repair_cost'];
			}
			$up_array = array(
				'budget_type_cn' => $type_cn,
				'budget_type' => $data['send_type'],
				'budget_year' => $year,
				'budget_money' => $allmoney,
				'budget_quantity' => $quantity,
				'budget_quantity_msg' => $quantity_msg,
				'budget_update_date' => date('Y-m-d H:i:s')
			);
			$this->db->where('budget_id',$get_row['budget_id']);
			$this->db->update('sys_budget',$up_array);

		}else{
			$quantity = count($data['selects']);
			$quantity_msg = json_encode($data['selects']);

			foreach ($data['selects'] as $value) {
				$allmoney += (int)$this->query->get_row_msg($data['send_type'],$value)[$data['send_type'].'_repair_cost'];
			}
			$in_array = array(
				'budget_type_cn' => $type_cn,
				'budget_type' => $data['send_type'],
				'budget_year' => $year,
				'budget_money' => $allmoney,
				'budget_quantity' => $quantity,
				'budget_quantity_msg' => $quantity_msg,
				'budget_create_date' => date('Y-m-d H:i:s')
			);
			$this->db->insert('sys_budget',$in_array);
			
		}
		
			
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}

	public function delete_money_detailed($id,$type)
	{
		
		$this->db->trans_begin();
		
		$allmoney = 0;
		$this->db->select('*');
		$this->db->from('sys_budget');
		$this->db->where('budget_year',date('Y')-1911+1);
		$this->db->where('budget_type',$type);
		$query = $this->db->get();
		$row = $query->row_array();
		$msg = json_decode($row['budget_quantity_msg'],true);
		if(($key = array_search($id,$msg)) !== false){
			unset($msg[$key]);
		}
		
		$quantity = count($msg);
		foreach ($msg as $val) {
			$allmoney += (int)$this->query->get_row_msg($type,$val)[$type.'_repair_cost'];
		}
		$update_ary = array(
			'budget_quantity' => $quantity,
			'budget_money' => $allmoney,
			'budget_quantity_msg' => json_encode($msg),
			'budget_update_date' => date('Y-m-d H:i:s')
		);
		
		log_message('error',print_r(json_encode($update_ary),true));
		$this->db->where('budget_id',$row['budget_id']);
		$this->db->update('sys_budget',$update_ary);
		// return json_decode($row['budget_quantity_msg'],true);
		
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}

	// 資產

	public function send_add_asset($data)
	{
		$this->db->trans_begin();
		switch ($data['asset_type']) {
			case 'road':
				break;
			case 'bridge':
				$in_array = array(
					'numero' => $data['asset_number'],
					'name' => $data['asset_bridge_name'],
					'bridge_town' => $data['asset_town'],
					'mile' => $data['asset_bridge_mile'],
					'length' => $data['asset_bridge_lenght'],
					'width' => $data['asset_bridge_width'],
					'area' => $data['asset_bridge_area'],
					'holes' => $data['asset_bridge_holes'],
					'span' => $data['asset_bridge_span'],
					'load' => $data['asset_bridge_load'],
					'structure_t' => $data['asset_bridge_structure_t'],
					'structure_b' => $data['asset_bridge_structure_b'],
					'item_top' => $data['asset_bridge_item_top'],
					'item_bottom' => $data['asset_bridge_item_bottom'],
					'river' => $data['asset_bridge_river_name'],
					'bridge_build_cost' => $data['asset_bridge_build_cost'],
					'bridge_repair_cost' => $data['asset_bridge_repair_cost'],
					'center_x' => $data['asset_bridge_center_x'],
					'center_y' => $data['asset_bridge_center_y'],
					'rings' => $data['asset_bridge_rings'],
					'created_at' => date('Y-m-d'),
				);
				break;
			case 'light':
				$in_array = array(
					'light_numid' => $data['asset_number'],
					'light_road_name' => $data['asset_light_name'],
					'light_town' => $data['asset_town'],
					'light_xcode' => $data['asset_light_center_x'],
					'light_ycode' => $data['asset_light_center_y'],
					'light_build_cost' => $data['asset_light_build_cost'],
					'light_repair_cost' => $data['asset_light_repair_cost'],
					'light_create_date' => date('Y-m-d'),
				);
				break;
			case 'tree':
				$in_array = array(
					'tree_no' => $data['asset_number'],
					'tree_town' => $data['asset_town'],
					'tree_type_name' => $data['asset_tree_type'],
					'tree_diameter' => $data['asset_tree_diameter'],
					'tree_high' => $data['asset_tree_high'],
					'tree_crown' => $data['asset_tree_crown'],
					'tree_road' => $data['asset_tree_name'],
					'tree_growing_type' => $data['asset_tree_growing'],
					'tree_url' => $data['asset_tree_url'],
					'tree_xcode' => $data['asset_tree_center_x'],
					'tree_ycode' => $data['asset_tree_center_y'],
					'tree_build_cost' => $data['asset_tree_build_cost'],
					'tree_repair_cost' => $data['asset_tree_repair_cost'],
					'tree_create_date' => date('Y-m-d'),
				);
				break;
			case 'sidewalk':
				$in_array = array(
					'sw_serial_num_no' => $data['asset_number'],
					'sw_town' => $data['asset_town'],
					'sw_road_name' => $data['asset_sw_name'],
					'sw_road_start' => $data['asset_sw_road_s'],
					'sw_road_end' => $data['asset_sw_road_e'],
					'sw_road_direction' => $data['asset_sw_direction'],
					'sw_pave' => $data['asset_sw_pave'],
					'sw_length' => $data['asset_sw_length'],
					'sw_width_u' => $data['asset_sw_width_u'],
					'sw_width_c' => $data['asset_sw_width_c'],
					'sw_leng' => $data['asset_sw_leng'],
					'sw_wth' => $data['asset_sw_wth'],
					'sw_xcode' => $data['asset_sw_center_x'],
					'sw_ycode' => $data['asset_sw_center_y'],
					'sw_rings' => $data['asset_sw_rings'],
					'sidewalk_build_cost' => $data['asset_tree_build_cost'],
					'sidewalk_repair_cost' => $data['asset_tree_repair_cost'],
					'sw_create_date' => date('Y-m-d'),
				);
				break;
			case 'groove':
				$in_array = array(
					'groove_numid' => $data['asset_number'],
					'groove_town' => $data['asset_town'],
					'groove_road' => $data['asset_groove_road'],
					'groove_length' => $data['asset_groove_length'],
					'groove_width' => $data['asset_groove_width'], 
					'groove_type' => $data['asset_groove_type'],
					'groove_xcode' => $data['asset_groove_center_x'],
					'groove_ycode' => $data['asset_groove_center_y'],
					'groove_rings' => $data['asset_groove_rings'],
					'groove_build_cost' => $data['asset_tree_build_cost'],
					'groove_repair_cost' => $data['asset_tree_repair_cost'],
					'groove_create_date' => date('Y-m-d'),
				);
				break;
			case 'cable':
				break;
			default:
				return false;
				break;
		}

		if($data['select_type'] == 'add'){
			$this->db->insert('facility_'.$data['asset_type'],$in_array);
		}else{
			$this->db->where($data['asset_type'].'_id',$data['edit_id']);
			$this->db->update('facility_'.$data['asset_type'],$in_array);
		}
		
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}

	public function insert_light($data)
	{
		$insert_array = array(
			'light_objectid' => $data['light_objectid'],
			'light_sysid' => $data['light_sysid'],
			'light_numid' => $data['light_numid'],
			'light_road_name' => $data['light_road_name'],
			'light_m' => $data['light_m'],
			'light_xcode' => $data['light_xcode'],
			'light_ycode' => $data['light_ycode'],
			'light_town' => $data['light_town'],
			'light_village' => $data['light_village'],
			'light_search_date' => $data['light_search_date'],
			'light_create_date' => date('Y-m-d H:i:s')
		);

		$this->db->trans_begin();
		
		$this->db->insert('facility_light',$insert_array);

			
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}

	public function insert_tree($data)
	{
		$this->db->select('*');
		$this->db->from('facility_tree_type');
		$this->db->where('tree_type_name',$data['tree_type_name']);
		$query = $this->db->get();
		$row = $query->row_array();
		$tree_type_id  = $row['tree_type_id'];
		$insert_array = array(
			'tree_objectid' => $data['tree_objectid'],
			'tree_no' => $data['tree_no'],
			'tree_type_id' => $tree_type_id,
			'tree_diameter' => $data['tree_diameter'],
			'tree_high' => $data['tree_high'],
			'tree_crown' => $data['tree_crown'],
			'tree_surverDate' => $data['tree_surverDate'],
			'tree_county' => $data['tree_county'],
			'tree_road' => $data['tree_road'],
			'tree_growing_type' => $data['tree_growing_type'],
			'tree_manageunit' => $data['tree_manageunit'],
			'tree_url' => $data['tree_url'],
			'tree_create_date' => date('Y-m-d H:i:s')
		);

		$this->db->trans_begin();
		
		$this->db->insert('facility_tree',$insert_array);

			
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}
	
	public function insert_sw($data)
	{
		if($data['sw_arcade'] == '是'){
			$sw_arcade = 'Y';
		}else{
			$sw_arcade = 'N';
		}
		if($data['sw_ac_even'] == '是'){
			$sw_ac_even = 'Y';
		}else{
			$sw_ac_even = 'N';
		}
		$insert_array = array(
			'sw_objectid' => $data['sw_objectid'],
			'sw_serial_num' => $data['sw_serial_num'],
			'sw_road_name' => $data['sw_road_name'],
			'sw_road_start' => $data['sw_road_start'],
			'sw_road_end' => $data['sw_road_end'],
			'sw_road_direction' => $data['sw_road_direction'],
			'sw_length' => $data['sw_length'],
			'sw_width_u' => $data['sw_width_u'],
			'sw_width_c' => $data['sw_width_c'],
			'sw_leng' => $data['sw_leng'],
			'sw_wth' => $data['sw_wth'],
			'swd_wth' => $data['swd_wth'],
			'swt_wth' => $data['swt_wth'],
			'sww_wth' => $data['sww_wth'],
			'sw_pave' => $data['sw_pave'],
			'shape_ar' => $data['shape_ar'],
			'sw_area' => $data['sw_area'],
			'sw_bk_l' => $data['sw_bk_l'],
			'sw_bk_b' => $data['sw_bk_b'],
			'sw_bk_e' => $data['sw_bk_e'],
			'sw_bk_p' => $data['sw_bk_p'],
			'sw_bk_o' => $data['sw_bk_o'],
			'sw_hole' => $data['sw_hole'],
			'sw_tree' => $data['sw_tree'],
			'sw_ramp' => $data['sw_ramp'],
			'sw_carramp' => $data['sw_carramp'],
			'sw_blok_name' => $data['sw_blok_name'],
			'sw_arcade' => $sw_arcade,
			'sw_ac_even' => $sw_ac_even,
			'area_name' => $data['area_name'],
			'sw_theme' => $data['sw_theme'],
			'sw_cost' => $data['sw_cost'],
			'sw_score' => $data['sw_score'],
			'sw_create_date' => date('Y-m-d H:i:s')
		);

		$this->db->trans_begin();
		
		$this->db->insert('facility_sidewalk',$insert_array);

			
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}

	public function insert_groove($data)
	{
		$insert_array = array(
			'groove_objectid' => $data['groove_objectid'],
			'groove_sysid' => $data['groove_sysid'],
			'groove_numid' => $data['groove_numid'],
			'groove_road' => $data['groove_road'],
			'groove_length' => $data['groove_length'],
			'groove_width' => $data['groove_width'],
			'groove_direction' => $data['groove_direction'],
			'groove_type' => $data['groove_type'],
			'groove_m' => $data['groove_m'],
			'groove_town' => $data['groove_town'],
			'groove_village' => $data['groove_village'],
			'groove_search_year' => $data['groove_search_year'],
			'groove_create_date' => date('Y-m-d H:i:s')
		);

		$this->db->trans_begin();
		
		$this->db->insert('facility_groove',$insert_array);

			
		if ($this->db->trans_status() === FALSE)
		{
				$this->db->trans_rollback();
				return false;
		}
		else
		{
				$this->db->trans_commit();
				return true;
		}
	}
}