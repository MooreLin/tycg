<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	
    <!-- Fontfaces CSS-->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/fontawesome-free/css/all.min.css');?>">
    <link href="<?=base_url();?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstr<?=base_url();?>ap CSS-->
    <link href="<?=base_url();?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor <?=base_url();?>CSS-->
    <link href="<?=base_url();?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/select2/select2-bootstrap4.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/datatables/datatables.min.css" rel="stylesheet" media="all">

	<script src="<?=base_url('suite_vendor/plugins/chart.js/Chart.min.js');?>"></script>
    <!-- Main CSS-->
    <link href="<?=base_url();?>css/theme.css" rel="stylesheet" media="all">
    
    
    <!-- Datatables -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
    
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css');?>">
    <!-- jQuery -->
    <script src="<?=base_url('suite_vendor/plugins/jquery/jquery.min.js');?>"></script>
    <!-- SweetAlert2 -->
    <script src="<?=base_url('suite_vendor/plugins/sweetalert2/sweetalert2.min.js');?>"></script>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="<?=base_url()?>images/logo.png" height="40">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <?php if(@$_SESSION['uid'] == NULL){?>
                    <li class="nav-item">
                        <a class="btn btn-outline-primary" href="<?=base_url('index/loginpage')?>">登入</a>
                    </li>
                    <?php }else{?>
                    <li class="nav-item dropdown dropdown-lg">
                        <a class="nav-link dropdown-toggle text-dark" href="#nav-user-dropdown" role="button" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user fa-lg"></i>
                        </a>
                        <div class="dropdown-menu border-lg-1" aria-labelledby="navbarUser" id="nav-user-dropdown">
                            <a href="<?=base_url('suite/admin')?>" class="dropdown-item">
                                <i class="fas fa-users-cog"></i>
                                後臺管理
                            </a>
                        <div class="dropdown-divider"></div>
                            <a href="<?=base_url('index/renovate');?>" class="dropdown-item">
                                <i class="fas fa-wrench"></i>
                                工程履歷
                            </a>
                            <a href="<?=base_url('index/asset');?>" class="dropdown-item">
                                <i class="fas fa-archive"></i>
                                資產列表
                            </a>
                            <a href="<?=base_url('index/money');?>" class="dropdown-item">
                                <i class="fas fa-money-bill-alt"></i>
                                編列預算
                            </a>
                            <a href="<?=base_url('index/checkmoney');?>" class="dropdown-item">
                                <i class="fas fa-money-bill-alt"></i>
                                核定預算
                            </a>
                            <a href="<?=base_url('index/profile');?>" class="dropdown-item">
                                <i class="fas fa-cog"></i>
                                設定
                            </a>
                        <div class="dropdown-divider"></div>
                        
                            <a href="<?=base_url('index/logout');?>" class="dropdown-item">
                                <i class="fas fa-sign-out-alt"></i>
                                登出
                            </a>
                        </div>
                    </li> 
                    <?php }?>
                </ul>  
            </div>
        </div>
    </nav>