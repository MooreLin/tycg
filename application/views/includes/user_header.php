<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html><html lang="zh-tw" style="height: auto;">
<head>
	<?php if(!empty($this->viewname)){echo $map['js']; }?>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="j1bwKDkYuyFha9Z6tBfffSTsjg9RPNvpwJnDxaqI">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="locale" content="zh-TW">
    <meta name="copyright" content="桃園市政府工務局">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">


	<title>列表頁 | 會員管理 | 後臺管理</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/fontawesome-free/css/all.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/jqvmap/jqvmap.min.css');?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/dist/css/adminlte.min.css');?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/daterangepicker/daterangepicker.css');?>">
    <!-- summernote -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/summernote/summernote-bs4.css');?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Datatables -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
    
    <!-- ChartJS -->
    <script src="<?=base_url('suite_vendor/plugins/chart.js/Chart.min.js');?>"></script>
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css');?>">
    <!-- jQuery -->
    <script src="<?=base_url('suite_vendor/plugins/jquery/jquery.min.js');?>"></script>
    <!-- SweetAlert2 -->
    <script src="<?=base_url('suite_vendor/plugins/sweetalert2/sweetalert2.min.js');?>"></script>
    <link href="<?=base_url();?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    
  <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css');?>">
    <script src="<?=base_url();?>vendor/select2/select2.min.js"></script>
    <!-- jQueryBlockUI -->
    <script src="<?=base_url('suite_vendor/plugins/jqueryBlockUI/jquery.blockUI.js');?>"></script>
</head>

<body>
    <div id="domMessage" style="display:none;"> 
        <h1>資料讀取中請稍後!!</h1> 
    </div> 
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="<?=base_url()?>images/logo.png" height="40">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto">
                    <?php if(@$_SESSION['uid'] == NULL){?>
                    <li class="nav-item">
                        <a class="btn btn-outline-primary" href="<?=base_url('index/loginpage')?>">登入</a>
                    </li>
                    <?php }else{?>
                    <li class="nav-item dropdown dropdown-lg">
                        <a class="nav-link dropdown-toggle text-dark" href="#nav-user-dropdown" role="button" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-user fa-lg"></i>
                        </a>
                        <div class="dropdown-menu border-lg-1" aria-labelledby="navbarUser" id="nav-user-dropdown">
                            <a href="<?=base_url('suite/admin')?>" class="dropdown-item">
                                <i class="fas fa-users-cog"></i>
                                後臺管理
                            </a>
                        <div class="dropdown-divider"></div>
                            <a href="<?=base_url('index/renovate');?>" class="dropdown-item">
                                <i class="fas fa-wrench"></i>
                                工程履歷
                            </a>
                            <a href="<?=base_url('index/asset');?>" class="dropdown-item">
                                <i class="fas fa-archive"></i>
                                資產列表
                            </a>
                            <a href="<?=base_url('index/money');?>" class="dropdown-item">
                                <i class="fas fa-money-bill-alt"></i>
                                編列預算
                            </a>
                            <a href="<?=base_url('index/checkmoney');?>" class="dropdown-item">
                                <i class="fas fa-money-bill-alt"></i>
                                核定預算
                            </a>
                            <a href="<?=base_url('index/profile');?>" class="dropdown-item">
                                <i class="fas fa-cog"></i>
                                設定
                            </a>
                        <div class="dropdown-divider"></div>
                        
                            <a href="<?=base_url('index/logout');?>" class="dropdown-item">
                                <i class="fas fa-sign-out-alt"></i>
                                登出
                            </a>
                        </div>
                    </li> 
                    <?php }?>
                </ul>  
            </div>
        </div>
    </nav>
