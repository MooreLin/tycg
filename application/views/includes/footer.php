
<!-- Bootstrap 4 -->
<script src="<?=base_url('suite_vendor/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<!-- Sparkline -->
<script src="<?=base_url('suite_vendor/plugins/sparklines/sparkline.js');?>"></script>
<!-- JQVMap -->
<script src="<?=base_url('suite_vendor/plugins/jqvmap/jquery.vmap.min.js');?>"></script>
<script src="<?=base_url('suite_vendor/plugins/jqvmap/maps/jquery.vmap.usa.js');?>"></script>
<!-- jQuery Knob Chart -->
<script src="<?=base_url('suite_vendor/plugins/jquery-knob/jquery.knob.min.js');?>"></script>
<!-- daterangepicker -->
<script src="<?=base_url('suite_vendor/plugins/moment/moment.min.js');?>"></script>
<script src="<?=base_url('suite_vendor/plugins/daterangepicker/daterangepicker.js');?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=base_url('suite_vendor/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js');?>"></script>
<!-- Summernote -->
<script src="<?=base_url('suite_vendor/plugins/summernote/summernote-bs4.min.js');?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('suite_vendor/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('suite_vendor/dist/js/adminlte.js');?>"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?=base_url('suite_vendor/dist/js/pages/dashboard.js');?>"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?=base_url('suite_vendor/dist/js/demo.js');?>"></script> -->

<!-- DataTables -->
<script src="<?=base_url('suite_vendor/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?=base_url('suite_vendor/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?=base_url('suite_vendor/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
<script src="<?=base_url('suite_vendor/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
</body>
</html>
