<?php
defined('BASEPATH') OR exit('No direct script access allowed');header("Content-Security-Policy: upgrade-insecure-requests");
?>
<script>
    var group_list = <?=json_encode($allgrouplist)?>;
    $(function () {
        $('#user_tables').DataTable();
    })
    
    // addbtn
    $(document).on('click','.addbtn',function(){
        $('#modal-primary').modal('show');
        $('#modal-primary .modal-title').text('新增會員');
        $('#modal-primary #group_type').val('add');
        $('#modal-primary #group_id').val();

    });
    // editbtn 
    $(document).on('click','.editbtn',function(){
        var group_id = $(this).data('no');
        $('#modal-primary').modal('show');
        $('#modal-primary .modal-title').text('修改會員');
        $('#modal-primary #group_type').val('edit');
        $('#modal-primary #group_id').val(group_id);
        $('#modal-primary #group_name').val(group_list[group_id]['group_name']);
        console.log(typeof(group_list[group_id]['group_msg']));
        if(group_list[group_id]['group_msg'].search('renovate') != -1){$('#page_1').attr('checked','checked');}
        if(group_list[group_id]['group_msg'].search('asset') != -1){$('#page_2').attr('checked','checked');}
        if(group_list[group_id]['group_msg'].search('money') != -1){$('#page_3').attr('checked','checked');}
        if(group_list[group_id]['group_msg'].search('checkmoney') != -1){$('#page_4').attr('checked','checked');}
    });

    // model submit
    $(document).on('submit','#groupform',function(){
        $.ajax({
            url: "<?=base_url('suite/Admin/group_send')?>",
            data: $('#groupform').serialize(),
            type:"POST",
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(thrownError)
                alert(xhr.responseText);
            },
            success:function(json){
                // show message
                console.log(json);

                if(json == true){
                    // success
                    Swal.fire({title: "成功",icon: "success"}).then(function(value){
                        window.location.reload();
                    });
                }else{
                    // error
                    Swal.fire({title: "發生錯誤",icon: "error"});
                    return false;
                }
            }
        });
        return false;
    });
    
     // Delbtn
     $(document).on('click','.delbtn',function(){
        var group_id = $(this).data('no');
        Swal.fire({
            title: "確定要刪除這筆資料嗎?",
            text: "一經刪除後將不可復原!並會影響已選取的會員!!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '是，我要刪除!!',
            cancelButtonText: '取消'
        })
        .then((result) =>{
            if (result.value) {
                $.post("<?=base_url('suite/admin/group_del')?>",{group_id:group_id},function(json){
                    if(json == true){
                        // success
                        Swal.fire({title: "刪除成功",icon: "success"}).then(function(value){
                            window.location.reload();;
                        });
                    }else{
                        // error
                        Swal.fire({title: "刪除時發生錯誤",icon: "warning"});
                        return false;
                    }
                });
            }
        });
    });
</script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">  
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?=$this->pagename?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active"><?=$this->pagename?></li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="float-right">
                <button type="button" class="btn btn-primary addbtn" data-toggle="modal" title="新增"><i class="fa fa-plus"></i></button>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
        <!-- Model -->
        <form class="form-signin" id="groupform" method="post" role="form">
            <div class="modal fade" id="modal-primary" style="display: none;" data-backdrop="static">
                <input type="hidden" name="group_type" id="group_type">
                <input type="hidden" name="group_id" id="group_id">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="group_name" class="control-label">群組名稱：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="group_name" id="group_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="user_pwd" class="control-label">權限選擇：</label>
                            </div>
                            <div class="col-9">
                                <div class="checkbox">
                                    <input type="checkbox" id="page_1" name="pagecheckbox[]" value="renovate">工程履歷
                                    <input type="checkbox" id="page_2" name="pagecheckbox[]" value="asset">資產列表
                                    <input type="checkbox" id="page_3" name="pagecheckbox[]" value="money">編列預算
                                    <input type="checkbox" id="page_4" name="pagecheckbox[]" value="checkmoney">核定預算
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </div>
                <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </form>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="user_tables" class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>群組名稱</th>
                            <th>權限</th>
                            <th>編輯時間</th>
                            <th>動作</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($group_msg as $key => $value) { ?>
                            <tr>
                                <td><?=$value['group_id'];?></td>
                                <td><?=$value['group_name'];?></td>
                                <td>
                                    <?php
                                        $group_msg = json_decode($value['group_msg'],true);
                                        if(in_array('renovate',$group_msg)){
                                            echo "工程履歷&#10004;	<br>";
                                        }else{
                                            echo "工程履歷&#10006;	<br>";
                                        }
                                        if(in_array('asset',$group_msg)){
                                            echo "資產列表&#10004;	<br>";
                                        }else{
                                            echo "資產列表&#10006;	<br>";
                                        }
                                        if(in_array('money',$group_msg)){
                                            echo "編列預算&#10004;	<br>";
                                        }else{
                                            echo "編列預算&#10006;	<br>";
                                        }
                                        if(in_array('checkmoney',$group_msg)){
                                            echo "核定預算&#10004;	<br>";
                                        }else{
                                            echo "核定預算&#10006;	<br>";
                                        }
                                    ?>
                                </td>
                                <td>
                                    <?php 
                                        if(empty($value['group_update_date'])){
                                            echo $value['group_create_date'];
                                        }else{
                                            echo $value['group_update_date'];
                                        }
                                    ?>
                                </td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success editbtn" data-toggle="modal" title="edit" data-no="<?=$value['group_id']?>"><i class="fa fa-pen"></i></button>                                            
                                        <button class="btn btn-danger delbtn" data-toggle="tooltip" data-placement="top" title="Delete" data-no="<?=$value['group_id']?>"><i class="fa fa-trash"></i>
                                    </div>
                                </td>
                            </tr>
                        <?php }?>
                    </table>
                </div>
                <!-- /.card-body -->
                </div>
            </div>
        </div>
      </div>
    </section>

    </div>
  <!-- /.content-wrapper -->
