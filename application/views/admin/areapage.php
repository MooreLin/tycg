<?php
defined('BASEPATH') OR exit('No direct script access allowed');header("Content-Security-Policy: upgrade-insecure-requests");
?>
<script>
    $(function () {
        $('#user_tables').DataTable();
    })
    
     // Delbtn
     $(document).on('click','.delbtn',function(){
        var code_item_id = $(this).data('no');
        Swal.fire({
            title: "確定要刪除這筆資料嗎?",
            text: "一經刪除後將不可復原!並會影響已選取的會員!!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '是，我要刪除!!',
            cancelButtonText: '取消'
        })
        .then((result) =>{
            if (result.isConfirmed) {
                $.post("<?=base_url('code_p1/del')?>",{code_item_id:code_item_id},function(json){
                    if(json == "true"){
                        // success
                        Swal.fire({title: "刪除成功",icon: "success"}).then(function(value){
                            window.location.reload();;
                        });
                    }else{
                        // error
                        Swal.fire({title: "刪除時發生錯誤",icon: "warning"});
                        return false;
                    }
                });
            }
        });
    });

    // update button
    $(document).on('click','.updatebtn',function(){
        Swal.fire({
            title:"初次更新可能需要一段時間!!",
            icon:"warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '是，我要更新!!',
            cancelButtonText: '取消'

        }).then((result) =>{
            if(result.value){
                $.post("<?=base_url('suite/Admin/update_area_msg')?>", function(json){
                    if(json == true){
                        // success
                        Swal.fire({title: "更新成功",icon: "success"}).then(function(value){
                            window.location.reload();;
                        });
                    }else{
                        // error
                        Swal.fire({title: "更新時發生錯誤",icon: "warning"});
                        return false;
                    }
                },'json');
            }
        });
    });
</script>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">  
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?=$this->pagename?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active"><?=$this->pagename?></li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
    
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="float-right">
                    <button type="button" class="btn btn-primary updatebtn" data-toggle="modal" title="更新區域"><i class="ion ion-ios-refresh-empty"></i></button>
                </div>
            </div>
        </div>
    </div>
    <section class="content">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="card">
                <!-- /.card-header -->
                    <div class="card-body">
                        <table id="user_tables" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>行政區</th>
                                <!-- <th>村里名</th> -->
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($area_msg as $key => $value) {?>
                                <tr>
                                    <td><?=$value['area_id'];?></td>
                                    <td><?=$value['area_name'];?></td>
                                    <!-- <td><?=$value['village_name'];?></td> -->
                                </tr>
                            <?php }?>
                        </table>
                    </div>
                <!-- /.card-body -->
                </div>
            </div>
        </div>
      </div>
    </section>

    </div>
  <!-- /.content-wrapper -->
