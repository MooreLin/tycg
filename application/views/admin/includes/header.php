<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html><html lang="zh-tw" style="height: auto;"><head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="j1bwKDkYuyFha9Z6tBfffSTsjg9RPNvpwJnDxaqI">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="locale" content="zh-TW">
    <meta name="copyright" content="桃園市政府工務局">

    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">


	<title>列表頁 | 會員管理 | 後臺管理</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/fontawesome-free/css/all.min.css');?>">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css');?>">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/icheck-bootstrap/icheck-bootstrap.min.css');?>">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/jqvmap/jqvmap.min.css');?>">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/dist/css/adminlte.min.css');?>">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/overlayScrollbars/css/OverlayScrollbars.min.css');?>">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/daterangepicker/daterangepicker.css');?>">
    <!-- summernote -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/summernote/summernote-bs4.css');?>">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <!-- Datatables -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css');?>">
    
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?=base_url('suite_vendor/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css');?>">
    <!-- jQuery -->
    <script src="<?=base_url('suite_vendor/plugins/jquery/jquery.min.js');?>"></script>
    <!-- SweetAlert2 -->
    <script src="<?=base_url('suite_vendor/plugins/sweetalert2/sweetalert2.min.js');?>"></script>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light"  style="background-color: #367fa9;">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto" >
        <li class="nav-item dropdown dropdown-lg">
            <a class="nav-link dropdown-toggle text-dark" href="#nav-user-dropdown" role="button" id="navbarUser" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-user fa-lg"></i>
            </a>
            <div class="dropdown-menu border-lg-1" aria-labelledby="navbarUser" id="nav-user-dropdown">
                <a href="<?=base_url('suite/admin')?>" class="dropdown-item">
                    <i class="fas fa-users-cog"></i>
                    後臺管理
                </a>
            <div class="dropdown-divider"></div>
                <a href="<?=base_url('index/renovate')?>" class="dropdown-item">
                    <i class="fas fa-wrench"></i>
                    工程履歷
                </a>
                <a href="<?=base_url('index/asset')?>" class="dropdown-item">
                    <i class="fas fa-archive"></i>
                    資產列表
                </a>
                <a href="<?=base_url('index/money')?>" class="dropdown-item">
                    <i class="fas fa-money-bill"></i>
                    編列預算
                </a>
                <a href="<?=base_url('index/checkmoney')?>" class="dropdown-item">
                    <i class="fas fa-money-bill"></i>
                    核定預算
                </a>
                <a href="<?=base_url('index/profile')?>" class="dropdown-item">
                    <i class="fas fa-cog"></i>
                    設定
                </a>
            <div class="dropdown-divider"></div>                        
                <a href="<?=base_url('index/logout');?>" class="dropdown-item">
                    <i class="fas fa-sign-out-alt"></i>
                    登出
                </a>
            </div>
        </li> 
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=base_url('suite/admin')?>" class="brand-link"  style="background-color: #367fa9;">
      <span class="logo-mini">TYCG</span>
      <span class="brand-text font-weight-light "><b>桃園市政府工務局</b></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="header">網站內容區</li>
            <li class="nav-item has-treeview menu-open">
                <a href="#" class="nav-link">
                    <i class="nav-icon fa fa-users"></i>
                    <p>
                        會員管理
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
                <ul class="nav nav-treeview">
                    <li class="nav-item">
                    <a href="<?=base_url('suite/admin');?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>會員列表</p>
                    </a>
                    </li>
                    <li class="nav-item">
                    <a href="<?=base_url('suite/admin/group');?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>群組列表</p>
                    </a>
                    </li>
                    <li class="nav-item">
                    <a href="<?=base_url('suite/admin/area');?>" class="nav-link">
                        <i class="far fa-circle nav-icon"></i>
                        <p>區域列表</p>
                    </a>
                    </li>
                </ul>
            </li>
            <!-- <li class="header">網站後台區</li>
            <li class="nav-item">
                <a href="pages/widgets.html" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Widgets
                    <span class="right badge badge-danger">New</span>
                </p>
                </a>
            </li> -->
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
