
  <footer class="main-footer">
    <strong>Copyright &copy; 2019-2020 TYCG-ENG.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.0.5
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<!-- <script>
  $.widget.bridge('uibutton', $.ui.button)
</script> -->
<!-- Bootstrap 4 -->
<script src="/suite_vendor/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="/suite_vendor/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="/suite_vendor/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="/suite_vendor/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="/suite_vendor/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="/suite_vendor/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="/suite_vendor/plugins/moment/moment.min.js"></script>
<script src="/suite_vendor/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="/suite_vendor/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="/suite_vendor/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="/suite_vendor/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="/suite_vendor/dist/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="<?=base_url('suite_vendor/dist/js/pages/dashboard.js');?>"></script> -->
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?=base_url('suite_vendor/dist/js/demo.js');?>"></script> -->

<!-- DataTables -->
<script src="/suite_vendor/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="/suite_vendor/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="/suite_vendor/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="/suite_vendor/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
</body>
</html>
