<?php
defined('BASEPATH') OR exit('No direct script access allowed');header("Content-Security-Policy: upgrade-insecure-requests");
?>
<script>
    var user_list = <?=json_encode($alluserlist)?>;
    $(function () {
        $('#user_tables').DataTable();
    })
    // addbtn
    $(document).on('click','.addbtn',function(){
        $('#modal-primary').modal('show');
        $('#modal-primary .modal-title').text('新增會員');
        $('#modal-primary #user_type').val('add');
        $('#modal-primary #user_id').val('');
        $('#modal-primary #user_pwd').val('');
        $('#modal-primary #user_pwd').attr('placeholder','');
        $('#modal-primary #user_account').val('');
        $('#modal-primary #user_name').val('');
        $('#modal-primary input[type="checkbox"]').prop('checked',false);
        // $("#modal-primary :input").not(":button, :submit, :reset, :hidden").val("").removeAttr("checked").remove("selected");

    });
    // editbtn 
    $(document).on('click','.editbtn',function(){
        var user_id = $(this).data('no');
        $('#modal-primary').modal('show');
        $('#modal-primary .modal-title').text('修改會員');
        $('#modal-primary #user_type').val('edit');
        $('#modal-primary input[type="checkbox"]').prop('checked',false);
        $('#modal-primary #user_id').val(user_id);
        $('#modal-primary #user_pwd').attr('placeholder','不須修改請留空白');
        $('#modal-primary #user_name').val(user_list[user_id]['user_name']);
        $('#modal-primary #user_account').val(user_list[user_id]['user_account']);
        $('#modal-primary #user_group').val(user_list[user_id]['group_id']);
        
        $.post('<?=base_url('suite/admin/get_user_area');?>',{user_id:user_id},function(json){
            var obj = JSON.parse(json);
            obj.forEach(function(item){
                if($('#area_name_'+item)){$('#area_name_'+item).prop('checked',true);}    
            });
        });
    });

    // model submit
    $(document).on('submit','#userform',function(){
        if($('#user_group').val() == '0'){
            Swal.fire({title: "請選擇群組",icon: "error"});
            return false;
        }
        $.ajax({
            url: "<?=base_url('suite/Admin/user_send')?>",
            data: $('#userform').serialize(),
            type:"POST",
            error:function(xhr, ajaxOptions, thrownError){ 
                console.log(thrownError)
                alert(xhr.responseText);
            },
            success:function(json){
                // show message
                console.log(json);

                if(json == true){
                    // success
                    Swal.fire({title: "成功",icon: "success"}).then(function(value){
                        window.location.reload();
                    });
                }else{
                    // error
                    Swal.fire({title: "發生錯誤",icon: "error"});
                    return false;
                }
            }
        });
        return false;
    });

     // Delbtn
    $(document).on('click','.delbtn',function(){
        var user_id = $(this).data('no');
        Swal.fire({
            title: "確定要刪除這筆資料嗎?",
            text: "一經刪除後將不可復原!",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '是，我要刪除!!',
            cancelButtonText: '取消'
        })
        .then((result) =>{
            if (result.isConfirmed) {
                $.post("<?=base_url('suite/admin/user_del')?>",{user_id:user_id},function(json){
                    if(json == "true"){
                        // success
                        Swal.fire({title: "刪除成功",icon: "success"}).then(function(value){
                            window.location.reload();;
                        });
                    }else{
                        // error
                        Swal.fire({title: "刪除時發生錯誤",icon: "warning"});
                        return false;
                    }
                });
            }
        });
    });
    

    // 停權按鈕
    $(document).on('click','.changebtn',function(){
        var user_id = $(this).data('no');
        var change_type = $(this).data('status');
        $.post('<?=base_url('suite/admin/change_user_status')?>',{user_id:user_id,change_type:change_type},function(json){
            if(json == true){
                // success
                Swal.fire({title: "成功",icon: "success"}).then(function(value){
                    window.location.reload();
                });
            }else{
                // error
                Swal.fire({title: "發生錯誤",icon: "error"});
                return false;
            }
        });
    });

</script>
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">  
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?=$this->pagename?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active"><?=$this->pagename?></li>
                </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="float-right">
                    <button type="button" class="btn btn-primary addbtn" data-toggle="modal" title="新增"><i class="fa fa-plus"></i></button>
                        <!-- <a href="<?=base_url('suite/admin/');?>" data-toggle="tooltip" title="" class="btn btn-primary" data-original-title="新增"></a> -->
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            
            <!-- Model -->
            
        <form class="form-signin" id="userform" method="post" role="form">
            <div class="modal fade" id="modal-primary" style="display: none;" data-backdrop="static">
                <input type="hidden" name="user_type" id="user_type">
                <input type="hidden" name="user_id" id="user_id">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="user_name" class="control-label">會員名稱：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="user_name" id="user_name" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="user_account" class="control-label">會員帳號：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="user_account" id="user_account" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="user_pwd" class="control-label">會員密碼：</label>
                            </div>
                            <div class="col-9">
                                <input type="password" name="user_pwd" id="user_pwd" class="form-control">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="user_group" class="control-label">權限群組：</label>
                            </div>
                            <div class="col-9">
                                <select class="form-control" id="user_group" name="user_group"> 
                                    <option value="0">請選擇群組</option>
                                    <?php foreach ($group_list as $gk => $gval) {?>
                                        <option value="<?=$gval['group_id'];?>"><?=$gval['group_name'];?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="user_area" class="control-label">區域選擇：</label>
                            </div>
                            <div class="col-9">
                                <div class="checkbox">
                                    <?php foreach ($area_list as $ak => $aval) {?>
                                        <input type="checkbox" id="<?='area_name_'.$aval['area_id'];?>" name="area_name[]" value="<?=$aval['area_id'];?>"><?=$aval['area_name'];?>&nbsp;
                                    <?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </div>
                <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </form>
            <!-- /.content-wrapper -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                        <!-- /.card-header -->
                            <div class="card-body">
                                <table id="user_tables" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>名稱</th>
                                        <th>帳號</th>
                                        <th>群組</th>
                                        <th>創立時間</th>
                                        <th>更新時間時間</th>
                                        <th>動作</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($userlist as $uk => $uval) {
                                        if($uval['user_status'] != 'Y'){ $trbg = 'bg-pink color-palette';}else{$trbg = '';}?>
                                        <tr class="<?=$trbg;?>">
                                            <td><?=$uval['user_id'];?></td>
                                            <td><?=$uval['user_name'];?></td>
                                            <td><?=$uval['user_account'];?></td>
                                            <td><?=$uval['group_name'];?></td>
                                            <td><?=$uval['user_create_date'];?></td>
                                            <td><?=$uval['user_update_date'];?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button type="button" class="btn btn-success editbtn" data-toggle="modal" title="edit" data-no="<?=$uval['user_id']?>"><i class="fa fa-pen"></i></button>                                            
                                                    <button id="_dropdown-4" type="button" class="btn btn-sm btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
                                                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="_dropdown-4" style="">
                                                        <li class="dropdown-item" data-ajax="GET" data-query="s=1">
                                                            <?php if($uval['user_status'] == 'Y'){?>
                                                                <button type="button" class="btn changebtn" data-no="<?=$uval['user_id']?>" data-status="N"><i class="fas fa-lock"></i> 停權 該使用者</button>
                                                            <?php }else{?>
                                                                <button type="button" class="btn changebtn" data-no="<?=$uval['user_id']?>" data-status="Y"><i class="fas fa-unlock"></i> 啟用 該使用者</button>
                                                            <?php }?>
                                                        </li>
                                                    </ul>
                                                    
                                                    <button class="btn btn-danger delbtn" data-toggle="tooltip" data-placement="top" title="Delete" data-no="<?=$uval['user_id']?>"><i class="fa fa-trash"></i>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                </table>
                            </div>
                        <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div>

        </section>
        
</div>

