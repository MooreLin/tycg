<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
    
$(function(){
    $(document).on('click', '.searchbtn', function(){
       years = $('#year').val();
       month = $('#month').val();
       postdate = years +'-'+ month;
       $.post("<?=base_url('monthly/getmonthly')?>", {postdate:postdate}, function(result){
        console.log(result);
        if(result == 0){
            $('#doc_status').html('未月結');
            $('.btn_monthly').css("visibility","visible");
            $('.btn_monthly').attr("data-year",years);
            $('.btn_monthly').attr("data-month",month);

            $('.btn_cancel').css("visibility","hidden");
            $('.btn_cancel').attr("data-year",'');
            $('.btn_cancel').attr("data-month",'');

            $('.btn_incr').css("visibility","hidden");
            $('.btn_incr').attr("data-year",'');
            $('.btn_incr').attr("data-month",'');


            $('.btn_sheet').css("visibility","hidden");
            $('.btn_sheet').attr("data-year",'');
            $('.btn_sheet').attr("data-month",'');
        }else{
            $('#doc_status').html('已月結');
            $('.btn_monthly').css("visibility","hidden");
            $('.btn_monthly').attr("data-year",'');
            $('.btn_monthly').attr("data-month",'');

            $('.btn_cancel').css("visibility","visible");
            $('.btn_cancel').attr("data-year",years);
            $('.btn_cancel').attr("data-month",month);

            $('.btn_incr').css("visibility","visible");
            $('.btn_incr').attr("data-year",years);
            $('.btn_incr').attr("data-month",month);

            $('.btn_sheet').css("visibility","visible");
            $('.btn_sheet').attr("data-year",years);
            $('.btn_sheet').attr("data-month",month);
        }
       });
       return false;
    });

    // 月結
    $(document).on('click','.btn_monthly',function(){
        years = $(this).data('year');
        month = $(this).data('month');
        $.ajax({
                url: "<?=base_url('monthly/start_monthly')?>",
                data: $('#searchform').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",icon: "error"});
                        return false;
                    }
                }
            });
            return false;
    });
    // 取消月結
    $(document).on('click','.btn_cancel',function(){
        years = $(this).data('year');
        month = $(this).data('month');
        $.ajax({
                url: "<?=base_url('monthly/cancel_monthly')?>",
                data: $('#searchform').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",icon: "error"});
                        return false;
                    }
                }
            });
            return false;

    });
    // 結存表
    $(document).on('click','.btn_sheet',function(){
        years = $(this).data('year');
        month = $(this).data('month');
        $('#searchform').attr('action','<?=base_url('monthly/sheet')?>');
        $('#searchform').submit();

    });
    // 增減表
    $(document).on('click','.btn_incr',function(){
        years = $(this).data('year');
        month = $(this).data('month');
        $('#searchform').attr('action','<?=base_url('monthly/incr')?>');
        $('#searchform').submit();

    });
});

</script>
<!-- MAIN CONTENT-->
<div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="title-1 m-b-25">月結狀態</h2>
                        <div class="table-responsive table--no-card m-b-40">
                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>
                                        <th>最後月結年</th>
                                        <th>最後月結月</th>
                                        <th>最後月結日期</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php if(!empty($monthly_msg)){?>
                                    <tr>
                                        <td><?=$monthly_msg['last_monthly_year']?>年</td>
                                        <td><?=$monthly_msg['last_monthly_month']?>月</td>
                                        <td><?=$monthly_msg['last_monthly_alldate']?></td>
                                    </tr>
                                    <?php }else{?>
                                    <tr>
                                        <td colspan="3" align="center">無資料....</td>
                                    </tr>
                                    <?php }?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <form id="searchform" name="searchform" method="post" enctype="multipart/form-data" class="form-horizontal">
                        <h2 class="title-1 m-b-25">月結報表列印</h2>
                            <div class="row form-group">
                                    <div class="col col-md-2 ">
                                        <select class="form-control select2" name="year" id="year">
                                            <option value="">請選擇年</option>
                                            <?php
                                                foreach ($doc_numyear as $doc_numyear) {
                                                    echo '<option value="'.$doc_numyear['doc_num_year'].'">'.$doc_numyear['doc_num_year'].'</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    
                                    <div class="col col-md-2 ">
                                        <select class="form-control select2" name="month" id="month">
                                            <option value="">請選擇月</option>
                                            <?php
                                                for ($i=1; $i < 13; $i++) { 
                                                    echo '<option value="'.str_pad($i, 2, '0', STR_PAD_LEFT).'">'.$i.'月</option>';
                                                }
                                            ?>
                                        </select>
                                    </div>
                                    
                                    <div class="col col-md-2 ">
                                        <input type="button" class="btn btn-primary searchbtn" value="查詢">
                                    </div>
                            </div>
                        </form>
                        <h3>月結狀態：<span id="doc_status"></span></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?php if($this->authority_array['monthly_option2'] == 'Y'):?>
                            <button type="button" class="btn btn-success btn_monthly" id="btn_monthly" style="visibility:hidden">月結</button>
                            <button type="button" class="btn btn-danger btn_cancel" id="btn_cancel" style="visibility:hidden">取消月結</button>
                        <?php endif;?>
                        
                        <?php if($this->authority_array['monthly_option3'] == 'Y'):?>
                            <button type="button" class="btn btn-secondary btn_incr" id="btn_incr" style="visibility:hidden">列印增減表</button>
                            <button type="button" class="btn btn-secondary btn_sheet" id="btn_sheet" style="visibility:hidden">列印結存表</button>
                        <?php endif;?>
                    </div>
                </div>