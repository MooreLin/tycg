<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
        $(document).ready(function() {
            $('.tags').select2();
        });
    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
<!-- Add A 任選 Model -->
    <div class="modal fade" id="addAModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">新增任選優惠</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="a_name" class=" form-control-label">優惠組合名稱：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="a_name" id="a_name" class="form-control" placeholder="請輸入組合名稱">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="a_number" class=" form-control-label">數量：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="a_number" id="a_number" class="form-control" placeholder="請輸入數量">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-9 offset-3">
                            <div class="form-check">
                                <div class="radio">
                                    <label for="radio1" class="form-check-label ">
                                        <div class="col-12">
                                            <input type="radio" id="radio1" name="radios" value="option1" class="form-check-input">金額：
                                        </div>
                                        <div class="col-12">
                                            <input type="text" name="a_money" id="a_money" class="form-control" >
                                        </div>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="radio2" class="form-check-label ">
                                        <div class="col-12">
                                            <input type="radio" id="radio2" name="radios" value="option2" class="form-check-input">折扣：
                                        </div>
                                        <div class="col-12">
                                            <input type="text" name="a_discount" id="a_discount" class="form-control" ><span>9折請打90，88折請打88</span>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="sdate" class=" form-control-label">開始日期：</label>
                        </div>
                        <div class="col-9">
                            <input type="date" name="sdate" id="sdate" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="edate" class=" form-control-label">結束日期：</label>
                        </div>
                        <div class="col-9">
                            <input type="date" name="edate" id="edate" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
<!-- end modal large -->

<!-- Edit A 任選 Model -->
    <div class="modal fade" id="editAModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">新增任選優惠</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="a_name" class=" form-control-label">優惠組合名稱：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="a_name" id="a_name" class="form-control" placeholder="請輸入組合名稱">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="a_number" class=" form-control-label">數量：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="a_number" id="a_number" class="form-control" placeholder="請輸入數量">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-9 offset-3">
                            <div class="form-check">
                                <div class="radio">
                                    <label for="radio1" class="form-check-label ">
                                        <div class="col-12">
                                            <input type="radio" id="radio1" name="radios" value="option1" class="form-check-input">金額：
                                        </div>
                                        <div class="col-12">
                                            <input type="text" name="a_money" id="a_money" class="form-control" >
                                        </div>
                                    </label>
                                </div>
                                <div class="radio">
                                    <label for="radio2" class="form-check-label ">
                                        <div class="col-12">
                                            <input type="radio" id="radio2" name="radios" value="option2" class="form-check-input">折扣：
                                        </div>
                                        <div class="col-12">
                                            <input type="text" name="a_discount" id="a_discount" class="form-control" ><span>9折請打90，88折請打88</span>
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="sdate" class=" form-control-label">開始日期：</label>
                        </div>
                        <div class="col-9">
                            <input type="date" name="sdate" id="sdate" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="edate" class=" form-control-label">結束日期：</label>
                        </div>
                        <div class="col-9">
                            <input type="date" name="edate" id="edate" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
<!-- end modal large -->

<!-- Add B 組合 Model -->
    <div class="modal fade" id="addBModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">新增組合優惠</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="b_name" class=" form-control-label">優惠組合名稱：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="b_name" id="b_name" class="form-control" placeholder="請輸入組合名稱">
                        </div>
                    </div>
                    <div class="form-group row">
                            <label for="product" class="col-sm-3 col-form-label">選擇產品</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="product" name="product[]" multiple="multiple">
                                    <option value="1">A</option>
                                    <option value="2">B</option>
                                    <option value="3">C</option>
                                    <option value="4">D</option>
                                <select>
                            </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="sdate" class=" form-control-label">開始日期：</label>
                        </div>
                        <div class="col-9">
                            <input type="date" name="sdate" id="sdate" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="edate" class=" form-control-label">結束日期：</label>
                        </div>
                        <div class="col-9">
                            <input type="date" name="edate" id="edate" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
<!-- end modal large -->

<!-- Edit B 組合 Model -->
    <div class="modal fade" id="editBModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">新增組合優惠</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="b_name" class=" form-control-label">優惠組合名稱：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="b_name" id="b_name" class="form-control" placeholder="請輸入組合名稱">
                        </div>
                    </div>
                    <div class="form-group row">
                            <label for="product" class="col-sm-3 col-form-label">選擇產品</label>
                            <div class="col-sm-9">
                                <select class="form-control" id="product" name="product[]" multiple="multiple">
                                    <option value="1">A</option>
                                    <option value="2">B</option>
                                    <option value="3">C</option>
                                    <option value="4">D</option>
                                <select>
                            </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="sdate" class=" form-control-label">開始日期：</label>
                        </div>
                        <div class="col-9">
                            <input type="date" name="sdate" id="sdate" class="form-control">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-3">
                            <label for="edate" class=" form-control-label">結束日期：</label>
                        </div>
                        <div class="col-9">
                            <input type="date" name="edate" id="edate" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
<!-- end modal large -->
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">優惠組合管理</h3>
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                            </div>
                            <div class="table-data__tool-right">
                                <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#addAModal">
                                    <i class="zmdi zmdi-plus"></i>新增任選優惠</button>
                                <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#addBModal">
                                    <i class="zmdi zmdi-plus"></i>新增組合優惠</button>
                            </div>
                        </div>
                        <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        
                                        <th>優惠方案</th>
                                        <th>優惠名稱</th>
                                        <th>開始時間</th>
                                        <th>結束時間</th>
                                        <th>內容</th> 
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="tr-shadow">
                                        
                                        <td>品牌</td>
                                        <td>品項</td>
                                        <td>產品名稱</td>
                                        <td>產品編號</td>
                                        <td>2020/08/14 15:20</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        
                                    <td>品牌</td>
                                        <td>品項</td>
                                        <td>產品名稱</td>
                                        <td>產品編號</td>
                                        <td>2020/08/14 15:20</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        
                                    <td>品牌</td>
                                        <td>品項</td>
                                        <td>產品名稱</td>
                                        <td>產品編號</td>
                                        <td>2020/08/14 15:20</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>品牌</td>
                                        <td>品項</td>
                                        <td>產品名稱</td>
                                        <td>產品編號</td>
                                        <td>2020/08/14 15:20</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE -->
                    </div>
                    
                </div>


