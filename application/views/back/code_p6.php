<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  
    <script>
        $(document).ready(function() {
            $('.organ_list').select2({
                theme: "bootstrap4"
            });
        });
        $(document).ready( function () {
            $('#table_id').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
            });
        } );

        // Delbtn
        $(document).on('click','.delbtn',function(){
            var code_item_area_id = $(this).data('no');
            Swal.fire({
                title: "確定要刪除這筆資料嗎?",
                text: "一經刪除後將不可復原!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是，我要刪除!!',
                cancelButtonText: '取消'
            })
            .then((result) =>{
                if (result.isConfirmed) {
                    $.post("<?=base_url('code_p5/del')?>",{code_item_area_id:code_item_area_id},function(json){
                        if(json == "true"){
                            // success
                            Swal.fire({title: "刪除成功",icon: "success"}).then(function(value){
                                window.location.reload();;
                            });
                        }else{
                            // error
                            Swal.fire({title: "刪除時發生錯誤",icon: "warning"});
                            return false;
                        }
                    });
                }
            });
        });

        // 清空model
        $(document).on('click','.addbtn',function(){
            
            $('#editnotify #largeModalLabel').text('新增異動');
            $('#editnotify #type').val('add');
            $('#editnotify #code_item_area_id').val('');
            $('#editnotify #code_item_area_code').val('');
            $('#editnotify #code_item_area_name').val('');
        });

        // editbtn show modal
        $(document).on('click','.editbtn',function(){
            code_item_area_id =$(this).data('no');
            $.post('<?=base_url('Code_p6/getid')?>',{code_item_area_id:code_item_area_id},function(json){
                console.log(json);
                $('#editnotify #largeModalLabel').text('修改異動')
                $('#editnotify #type').val('edit');
                $('#editnotify #code_item_area_id').val(json['code_item_area_id']);
                $('#editnotify #code_item_area_code').val(json['code_item_area_code']);
                $('#editnotify #code_item_area_name').val(json['code_item_area_name']);
            },'json');
            // $('#editnotify #notify').val(allnotify[$(this).data('no')]['M01I02NV0040']);
            // $('#editnotify #timestart').val(allnotify[$(this).data('no')]['M01N01DD0003']);
            // $('#editnotify #timestop').val(allnotify[$(this).data('no')]['M01N02DD0003']);
            $('#memberModal').modal('show');
        });

        // Model submit
        $(document).on('submit','#editnotify',function(){
            $.ajax({
                url: "<?=base_url('code_p6/send')?>",
                data: $('#editnotify').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",text:"請確認資料",icon: "error"});
                        return false;
                    }
                }
            });
            return false;
        });
    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
<!-- Model -->
    <form class="form-signin" id="editnotify" method="post" role="form">
        <input type="hidden" name="type" id="type" value="add">
        <input type="hidden" name="code_item_area_id" id="code_item_area_id">
        <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="largeModalLabel">新增物品區分別</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">    
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">                        
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="code_item_area_code" class=" form-control-label">物品區分別代碼：</label>
                            </div>
                            <div class="col-8">
                                <input type="text" name="code_item_area_code" id="code_item_area_code" class="form-control" placeholder="請輸入物品區分別代碼" maxlength="3" onkeyup="value=value.replace(/[\W]/g,'') ">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="code_item_area_name" class=" form-control-label">物品區分別名稱：</label>
                            </div>
                            <div class="col-8">
                                <input type="text" name="code_item_area_name" id="code_item_area_name" class="form-control" placeholder="請輸入物品區分別名稱" maxlength="20">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
<!-- end modal large -->
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>    
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">物品區分別管理</h3>
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                            </div>
                            <div class="table-data__tool-right">
                                <?php if($this->authority_array['code_option2'] == 'Y' || $this->user_group =='S'):?>
                                    <button class="au-btn au-btn-icon au-btn--green au-btn--small addbtn" data-toggle="modal" data-target="#memberModal">
                                        <i class="zmdi zmdi-plus"></i>新增物品區分別代碼</button>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="table-responsive table-responsive-data2">
                            <table id="table_id" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>物品區分別代碼</th>
                                        <th>物品區分別名稱</th>
                                        <th>更新時間</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($code_list as $key => $value) :
                                    ?>
                                    <tr>
                                        <td><?=$value['code_item_area_code']?></td>
                                        <td><?=$value['code_item_area_name']?></td>
                                        <td><?=$value['code_item_area_update_date']?></td>
                                        <td>
                                            <?php if($this->authority_array['code_option2'] == 'Y'){?>
                                                <div class="table-data-feature">
                                                    <button class="item editbtn" data-toggle="tooltip" data-placement="top" title="Edit" data-no="<?=$value['code_item_area_id'];?>">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item delbtn" data-toggle="tooltip" data-placement="top" title="Delete" data-no="<?=$value['code_item_area_id'];?>">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                </div>
                                            <?php }else{
                                                echo "　";
                                             }?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE -->
                    </div>
                </div>


