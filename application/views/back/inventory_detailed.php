<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
var datatable;



function checkIsNumber(input, id){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);

    if(id == 1){
        if($(input).val().length == 1){
            $('#id_2').focus();
        }
    }else if(id == 2){
        if($(input).val().length == 2){
            $('#id_3').focus();
        }
    }else if(id == 3){
        if($(input).val().length == 2){
            $('#id_4').focus();
        }
    }else if(id == 4){
        if($(input).val().length == 2){
            $('#id_5').focus();
        }
    }
}

function checkItem(){
    var id_1 = $('#id_1').val();
    var id_2 = $('#id_2').val();
    var id_3 = $('#id_3').val();
    var id_4 = $('#id_4').val();
    var id_5 = $('#id_5').val();
    if(id_1 != '' && id_2 != '' && id_3 != '' && id_4 != '' && id_5 != ''){
        $.post('<?=base_url('item_p1/checkItemID')?>', {id_1:id_1,id_2:id_2,id_3:id_3,id_4:id_4,id_5:id_5}, function(result){
            $('#item_name').html(result);
        });
    }
}


$(function(){
    // $('.movebtn').hide();
    // $('.revokebtn').hide();
    
    $('.print_all_list').attr("href","#");
    $('.reprint_tag').attr("href","#");
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    // datatable = $('#datatable').DataTable({
    //     'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
    //     'columnDefs': [{
    //         'targets': [0],
    //         'searchable': false,
    //         'orderable': false
    //     }],
    //     "language": {
    //         "url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
    //     }
    // });



    $(document).on('click', '#selectall', function(){
        var rows = datatable.rows({ 'search': 'applied' }).nodes();
        $('input[name="selects[]"]', rows).prop('checked', this.checked);
    });

    $(document).on('click', '.print_tag_btn', function(){
        var form = $('#editform');
        console.log('test');
        if(datatable != null){
            $('#editform input[type="hidden"][name="selects[]"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#editform input[name="selects[]"]:checked').length + $('#editform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }

        form.attr('action','<?=base_url('item_p1/print_tag')?>');
        form.submit();
    });




    // 盤點清冊
    $(document).on('click','.print_list',function(){
        $('#editform #print_type').val('all');
        var form = $('#editform');
        
        if(datatable != null){
            $('#editform input[type="hidden"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#editform input[name="selects[]"]:checked').length + $('#editform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
        $('#editform').attr('action','<?=base_url('inventory/print_list')?>');
        $('#editform').submit();
        return false;
    });

    // 未盤點清冊
    $(document).on('click','.print_list_no',function(){
        $('#editform #print_type').val('N');
        var form = $('#editform');
        
        if(datatable != null){
            $('#editform input[type="hidden"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#editform input[name="selects[]"]:checked').length + $('#editform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
        $('#editform').attr("action","<?=base_url('inventory/print_list_YorN')?>");
        $('#editform').submit();
        return false;
        // inventory_id = $(this).data('id');
        // type = 'N';
        // window.location.href ="<?=base_url('inventory/print_list_YorN')?>?id="+inventory_id+"&type="+type;
    });
    // 已盤點清冊
    $(document).on('click','.print_list_yes',function(){
        $('#editform #print_type').val('Y');
        var form = $('#editform');
        
        if(datatable != null){
            $('#editform input[type="hidden"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#editform input[name="selects[]"]:checked').length + $('#editform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
        $('#editform').attr("action","<?=base_url('inventory/print_list_YorN')?>");
        $('#editform').submit();
        return false;
        // inventory_id = $(this).data('id');
        // type = 'Y';
        // window.location.href ="<?=base_url('inventory/print_list_YorN')?>?id="+inventory_id+"&type="+type;
    });

    // 補0
    function pad(num, n) {
        var len = num.toString().length;
        while(len < n) {
            num = "0" + num;
            len++;
        }
        return num;
    }
    // 查詢
    $(document).on('submit', '#searchform', function(){
        $('#datatable').DataTable().destroy();
        $.post("<?=base_url('inventory/getitem')?>", $("#searchform").serialize(), function(result){
            result = JSON.parse(result);
            console.log(result);
            $('#itemlist').html('');
            var itemhtml = '';
            var count_move = 0;
            var count_revoke = 0;
            var inventory_id = '';
            for(i in result){
                if(result[i]['item_type'] != null){
                    item_type = result[i]['item_type'].substring(20,0);
                }else{
                    item_type = '';
                }
                if(result[i]['item_brand'] != null){
                    item_brand = result[i]['item_brand'].substring(20,0);
                }else{
                    item_brand ='';
                }

                if(result[i]['inventory_status'] == 'Y'){
                    in_status = '已盤點';
                }else{
                    in_status ='　';
                }
                var price = parseInt(result[i]['item_price'])+parseInt(result[i]['item_price_change']);
                itemhtml += '<tr>';
                itemhtml += '<td><input type="checkbox" name="selects[]" id="selects[]" value="'+result[i]['item_id']+'"></td>';
                itemhtml += '<td>'+result[i]['item_buytime']+'<br>'+result[i]['item_gettime']+'</td>';
                
                itemhtml += '<td>'+result[i]['code_item_class']+'-'+result[i]['code_item_project']+'-'+result[i]['code_item_contents']+'-'+result[i]['code_item_section']+'-'+result[i]['code_item_number']+'-'+pad(result[i]['item_number'], 7)+'<br>'+result[i]['code_item_name']+'</td>';
                itemhtml += '<td>'+item_type+'/'+item_brand+'</td>';
                itemhtml += '<td>'+result[i]['code_item_unit']+'</td>';
                itemhtml += '<td>'+price+'</td>';
                itemhtml += '<td>'+result[i]['code_item_deadline']+'</td>';
                itemhtml += '<td>'+result[i]['site_name']+'</td>';
                itemhtml += '<td>'+result[i]['usesite_name']+'<br>'+result[i]['keepsite_name']+'</td>';
                itemhtml += '<td>'+result[i]['usecust_name']+'<br>'+result[i]['keepcust_name']+'</td>';
                itemhtml += '<td>'+in_status+'</td>';
                itemhtml += '</tr>';

                inventory_id = result[0]['inventory_id'];
                
                
            }
            $('.print_all_list').attr("href","<?=base_url('inventory/print_all_item/')?>"+inventory_id);
            $('.reprint_tag').attr("href","<?=base_url('inventory/reprint_tag/')?>"+inventory_id);
            $('#itemlist').html(itemhtml);
            datatable = $('#datatable').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
                'columnDefs': [{
                    'targets': [0],
                    'searchable': false,
                    'orderable': false,
                }],
                "language": {
                    "url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
                }
            });
            $('.item').tooltip('update');
        });

        return false;
    });
    

});
</script>
<!-- MAIN CONTENT-->

<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                <h3 class="title-5 m-b-35">盤點盤存表報表搜尋</h3>
                    <div class="table-data__tool">
                        <div id="accordion" style="width:100%;">
                            <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer;">
                                    <h5 class="mb-0">
                                        <i class="fa"></i> 查詢條件
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <form id="searchform" name="searchform" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="card-body">
                                            <div class="row form-group">
                                                <!-- <div class="col col-md-2">
                                                    <label class="form-control-label">盤點年份</label>
                                                </div>
                                                
                                                <div class="col col-md-2 ">
                                                    <select class="form-control select2" name="year" id="year">
                                                        <option value="">請選擇年</option>
                                                        <?php
                                                            foreach ($doc_numyear as $doc_numyear) {
                                                                echo '<option value="'.$doc_numyear['doc_num_year'].'">'.$doc_numyear['doc_num_year'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div> -->
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">購置日期區間</label>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="date_start" name="date_start" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="date_end" name="date_end" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">

                                                <div class="col col-md-2">
                                                    <label class="form-control-label">物品編號</label>
                                                </div>
                                                <div class="col-12 col-md-5">
                                                    <div class="input-group">
                                                        <input type="text" id="id_1" name="id_1" class="form-control" maxlength="1" style="max-width:40px;min-width:40px;text-align:center;" onkeyup="checkIsNumber(this, 1);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_2" name="id_2" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 2);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_3" name="id_3" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 3);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_4" name="id_4" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 4);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_5" name="id_5" class="form-control" maxlength="4" style="max-width:70px;min-width:70px;text-align:center;" onkeyup="checkIsNumber(this, 5);checkItem();">　　
                                                        <label id="item_name" class="form-control-label"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">物品流水號</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" id="id_6" name="id_6" class="form-control" maxlength="7" style="max-width:100px;min-width:100px;text-align:center;" onkeyup="checkIsNumber(this)">&nbsp~&nbsp
                                                        <input type="text" id="id_7" name="id_7" class="form-control" maxlength="7" style="max-width:100px;min-width:100px;text-align:center;" onkeyup="checkIsNumber(this)">
                                                    </div>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">物品編號查詢</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="item_code" name="item_code">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($itemcode_list as $itemcode){
                                                            echo '<option value="'.$itemcode['code_item_id'].'">'.$itemcode['code_item_class'].'-'.$itemcode['code_item_project'].'-'.$itemcode['code_item_contents'].'-'.$itemcode['code_item_section'].'-'.$itemcode['code_item_number'].' '.$itemcode['code_item_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">存置地點</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="site" name="site">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">保管單位</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="keepsite" name="keepsite">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">使用單位</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="usesite" name="usesite">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">保管人</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="keepcust" name="keepcust">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($custcode_list as $custcode){
                                                            echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">使用人</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="usecust" name="usecust">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($custcode_list as $custcode){
                                                            echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        <div class="card-footer text-right">
                                            <input type="reset" class="btn btn-danger" value="重設">
                                            <input type="submit" class="btn btn-primary" value="查詢">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id="editform" name="editform" method="post" enctype="multipart/form-data" class="form-horizontal" action="">
                         <input type="text" name="print_type" id="print_type" style="display:none">
                         <table id="datatable" class="table table-striped table-bordered" style="width:100%" valign="center">
                            <thead class="thead-light">
                                <tr>
                                    <th><input type="checkbox" id="selectall"></th>
                                    <th>購置日期<br>取得日期</th>
                                    <th>物品編號<br>物品名稱</th>
                                    <th>形式/廠牌</th>
                                    <th>單位</th>
                                    <th>單價</th>
                                    <th>使用年限</th>
                                    <th>存置地點</th>
                                    <th>使用單位<br>保管單位</th>
                                    <th>使用人<br>保管人</th>
                                    <th>盤點狀況</th>
                                </tr>
                            </thead>
                            <tbody id="itemlist">
                                    
                            </tbody>

                            <div id="button_list">
                                <?php if($this->authority_array['inventory_option3'] == 'Y'):?>
                                    <button type="button" class="au-btn au-btn-icon au-btn--blue au-btn--small print_tag_btn"><i class="fa fa-print"></i>列印標籤</button>
                                    <button type="button" class="btn btn-info print_list"><i class="fa fa-print"></i>&nbsp; 列印已選取物品盤點清冊</button>&nbsp;
                                    <button type="button" class="btn btn-danger print_list_no" ><i class="fa fa-print"></i>&nbsp; 列印物品未盤點清冊</button>&nbsp;
                                    <button type="button" class="btn btn-success print_list_yes" ><i class="fa fa-print"></i>&nbsp; 列印物品已盤點清冊</button>&nbsp;
                                    
                                    <a class="au-btn au-btn-icon au-btn--blue au-btn--small print_all_list" href="#"><i class="fa fa-print"></i>列印盤存表</a> 
                                    
                                    <!-- <a class="au-btn au-btn-icon au-btn--blue au-btn--small movebtn" href="#"><i class="fa fa-print"></i>列印盤移動單</a> 
                                    <a class="au-btn au-btn-icon au-btn--blue au-btn--small revokebtn" href="#"><i class="fa fa-print"></i>列印盤報廢單</a>  -->
                                    
                                    <a class="au-btn au-btn-icon au-btn--blue au-btn--small reprint_tag" href="#"><i class="fa fa-print"></i>補印標籤</a>
                                <?php endif;?> 
                            </div>
                        </table>
                    </form>
                </div>
                    
            </div>


