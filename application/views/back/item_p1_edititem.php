<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
function checkIsNumber(input){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);
}

$(function(){
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    $('.dateselector').datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        todayHighlight: true,
        "setDate": '109-01-01',
        "autoclose": true
    }).on("show", function (e) {
        $("div.box").css({minHeight: "480px"});
    }).on("hide", function (e) {
        $("div.box").css({minHeight: "auto"});
    });

    $(document).on('change', '#item_code', function(){
        var no = $(this).val();
        if(no == ''){
            $('#deadline').val('');
            $('#unit').val('');
        }else{
            $('#deadline').val(item_code[no]['code_item_deadline']);
            $('#unit').val(item_code[no]['code_item_unit']);
        }
        $('#unit_price').trigger('change');
    });

    $(document).on('change', '#unit_price', function(){
        var price = $('#unit_price').val();
        var amount = $('#amount').val();
        if(price != '' && amount != ''){
            $('#total_price').val(price * amount);
        }
    });

    $(document).on('change', '#amount', function(){
        var price = $('#unit_price').val();
        var amount = $('#amount').val();
        if(price != '' && amount != ''){
            $('#total_price').val(price * amount);
        }
    });
});
</script>

<style>
[data-toggle="collapse"] .fa:before {   
  content: "\f146";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f0fe";
}
</style>

<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" class="form-horizontal" action="<?=base_url('item_p1/edititem/'.$item['item_id'])?>">
                        <input type="hidden" id="item_id" name="item_id" value="<?=$item['item_id']?>">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">修改物品</strong>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">物品編號</label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <?=$item['code_item_class'].'-'.$item['code_item_project'].'-'.$item['code_item_contents'].'-'.$item['code_item_section'].'-'.$item['code_item_number'].'-'.str_pad($item['item_number'], 7, '0', STR_PAD_LEFT).' '.$item['code_item_name']?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">別名</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="alias" name="alias" value="<?=$item['item_alias']?>">
                                    </div>
                                    <div class="col col-md-1">
                                        <label class="form-control-label">來源</label>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">形式</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="type" name="type" value="<?=$item['item_type']?>">
                                    </div>
                                    <div class="col col-md-1">
                                        <label class="form-control-label">廠牌</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="brand" name="brand" value="<?=$item['item_brand']?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    
                                    <div class="col col-md-1">
                                        <label class="form-control-label">備註</label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <input type="text" class="form-control" id="desc" name="desc" value="<?=$item['item_desc']?>">
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <a role="button" href="<?=base_url('item_p1')?>" class="btn btn-danger">取消</a>
                                <input type="submit" class="btn btn-primary" value="修改">
                            </div>
                        </div>
                    </form>
                </div>
            </div>


