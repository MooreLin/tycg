<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
    
var datatable;
function checkIsNumber(input){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);
}

function pad(num, n) {
    var len = num.toString().length;
    while(len < n) {
        num = "0" + num;
        len++;
    }
    return num;
}

function checkItem(){
    var id_1 = $('#id_1').val();
    var id_2 = $('#id_2').val();
    var id_3 = $('#id_3').val();
    var id_4 = $('#id_4').val();
    var id_5 = $('#id_5').val();
    if(id_1 != '' && id_2 != '' && id_3 != '' && id_4 != '' && id_5 != ''){
        $.post('<?=base_url('item_p1/checkItemID')?>', {id_1:id_1,id_2:id_2,id_3:id_3,id_4:id_4,id_5:id_5}, function(result){
            $('#item_name').html(result);
        });
    }
}

$(function(){
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    //$('#datatable').DataTable();

    $('.dateselector').datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        todayHighlight: true,
    }).on("show", function (e) {
        $("div.box").css({minHeight: "480px"});
    }).on("hide", function (e) {
        $("div.box").css({minHeight: "auto"});
    });

    $(document).on('change', '#selectall', function(){
        $('input[name="selects[]"]').prop('checked', $(this).prop('checked'));
        // $('input[name="selects[]"]').serialize()
    });

    $(document).on('submit', '#searchform', function(){
        $('#datatable').DataTable().destroy();
        $.post("<?=base_url('item_p5/getitem')?>", $("#searchform").serialize(), function(result){
            result = JSON.parse(result);
            console.log(result);
            $('#itemlist').html('');
            var itemhtml = '';
            for(i in result){
                itemhtml += '<tr>';
                // itemhtml += '<td><input type="checkbox" name="selects[]" id="selects[]" value="'+result[i]['item_id']+'"></td>';
                itemhtml += '<td>'+result[i]['doc_write_date']+'</td>';
                itemhtml += '<td>'+result[i]['doc_accounts_date']+'</td>';
                itemhtml += '<td>'+result[i]['doc_num_type']+'</td>';
                itemhtml += '<td>'+result[i]['doc_num_year']+'-'+result[i]['doc_num_type']+'-'+paddingLeft(result[i]['doc_num_number'],7)+'</td>';
                itemhtml += '<td>'+result[i]['doc_type']+'</td>';
                if(result[i]['doc_num_type'] == '基修' ){
                    <?php if($this->authority_array['item_option2'] == 'Y'){?>
                        itemhtml += '<td><button type="button "class="editbtn" title="編輯" data-type="'+result[i]['doc_num_type']+
                        '" data-no="'+result[i]['doc_id']+'"><i class="fa fa-edit"></i></button>&nbsp;&nbsp;</td>';
                    <?php }else{?>
                        itemhtml += '<td></td>';
                    <?php }?>
                }else if(result[i]['doc_status'] == 'Y' || result[i]['doc_num_type'] == '盤移動' || result[i]['doc_num_type'] == '盤報廢' || result[i]['doc_num_type'] == '廢品'){
                    
                    <?php if($this->authority_array['item_option3'] == 'Y'){?>
                        itemhtml += '<td><button type="button "class="printbtn" title="列印單據" data-no="'+result[i]['doc_id']+'"><i class="fa fa-print"></i></button></td>';
                    <?php }else{?>
                        itemhtml += '<td></td>';
                    <?php }?>
                }else{
                    <?php if($this->authority_array['item_option2'] == 'Y'){?>
                        itemhtml += '<td><button type="button "class="editbtn" title="編輯" data-type="'+result[i]['doc_num_type']+'" data-no="'+result[i]['doc_id']+'"><i class="fa fa-edit"></i></button>';
                    <?php }else{?>
                        itemhtml += '<td>';
                    <?php }?>
                    <?php if($this->authority_array['item_option3'] == 'Y'){?>
                        itemhtml += '&nbsp;&nbsp;<button type="button "class="printbtn" title="列印單據" data-no="'+result[i]['doc_id']+'"><i class="fa fa-print"></i></button></td>';
                    <?php }else{?>
                        itemhtml += '</td>';
                    <?php }?>
                }
                
                itemhtml += '</tr>';
            }
            $('#itemlist').html(itemhtml);
            datatable = $('#datatable').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
                'columnDefs': [{
                    'targets': [0],
                    'searchable': false,
                    'orderable': false,
                }],
                "language": {
                    "url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
                }
            });
        });

        return false;
    });
    // 補0
    function paddingLeft(str,lenght){
        if(str.length >= lenght)
        return str;
        else
        return paddingLeft("0" +str,lenght);
    }
    
    // 匯出pdf
    $(document).on('click', '.printbtn', function(){
        no = $(this).data('no');
        var url = "<?=base_url('item_p5/export_pdf/')?>"+no;
        window.location.replace(url);
        return false;
        // location.reload();
    });

    // 編輯
    $(document).on('click', '.editbtn', function(){
        no = $(this).data('no');
        type = $(this).data('type');
        console.log(type);
        if(type == '物增'){
            url = "<?=base_url('item_p2/edit/')?>" + no;
        }
        if(type == '物移'){
            url = "<?=base_url('item_p3/edit/')?>" + no;
        }
        if(type == '增減'){
            url = "<?=base_url('item_p4/edit/')?>" + no;
        }
        if(type == '報廢'){
            url = "<?=base_url('item_p6/edit/')?>" + no;
        }
        if(type == '撥出'){
            url = "<?=base_url('item_p7/edit/')?>" + no;
        }
        if(type == '基修'){
            url = "<?=base_url('item_p8/edit/')?>" + no;
        }
        window.location.replace(url);
        return false;
        // var url = "<?=base_url('item_p5/export_pdf?doc_id=')?>"+no;
        // window.open(url, '匯出', config='height=600,width=800');
        // location.reload();
    });
});

    
</script>

<style>
[data-toggle="collapse"] .fa:before {   
  content: "\f146";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f0fe";
}
</style>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">單據列表</h3>
                    <div class="table-data__tool">
                        <div id="accordion" style="width:100%;">
                            <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer;">
                                    <h5 class="mb-0">
                                        <i class="fa"></i> 查詢條件
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <form id="searchform" name="searchform" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="card-body">
                                            <div class="row form-group">
                                                <div class="col col-md-2 ">
                                                    <label class="form-control-label">傳票編號</label>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <select class="form-control select2" id="doc_type" name="doc_type">
                                                        <option value="">　</option>
                                                        <?php
                                                            foreach ($doc_type as $doc_type) {
                                                                echo '<option value="'.$doc_type['doc_type'].'">'.$doc_type['doc_type'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                
                                                <div class="col col-md-2 ">
                                                    <label class="form-control-label">月結狀態</label>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <select class="form-control select2" id="doc_status" name="doc_status">
                                                        <option value="">　</option>
                                                        <option value="Y">已月結</option>
                                                        <option value="N">未月結</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2 ">
                                                    <label class="form-control-label">單據字號</label>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <select class="form-control select2" id="doc_num_year" name="doc_num_year">
                                                        <option value="">　</option>
                                                        <?php
                                                            foreach ($doc_numyear as $doc_numyear) {
                                                                echo '<option value="'.$doc_numyear['doc_num_year'].'">'.$doc_numyear['doc_num_year'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <select class="form-control select2" id="doc_num_type" name="doc_num_type">
                                                        <option value="">　</option>
                                                        <?php
                                                            foreach ($doc_numtype as $doc_numtype) {
                                                                echo '<option value="'.$doc_numtype['doc_num_type'].'">'.$doc_numtype['doc_num_type'].'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-12 col-md-3">
                                                    <select class="form-control select2" id="doc_num_number" name="doc_num_number">
                                                        <option value="">　</option>
                                                        <?php
                                                            foreach ($doc_numnumber as $doc_numnumber) {
                                                                echo '<option value="'.$doc_numnumber['doc_num_number'].'">'.str_pad($doc_numnumber['doc_num_number'],7,'0',STR_PAD_LEFT).'</option>';
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-2 ">
                                                    <label class="form-control-label">填單日期區間</label>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="write_date_start" name="write_date_start" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="write_date_end" name="write_date_end" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">帳務日期區間</label>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="account_date_start" name="account_date_start" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="account_date_end" name="account_date_end" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <input type="reset" class="btn btn-danger" value="重設">
                                            <input type="submit" class="btn btn-primary" value="查詢">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!-- <div class="row">
                            <div class="col-md-5 offset-md-7 col-sm-6 ml-auto">
                                    <button type="button" class="btn btn-primary excelbtn"><i class="fa fa-upload"></i>&nbsp; 匯出excel</button>
                                    <button type="button" class="btn btn-success printbtn"><i class="fa fa-print"></i>&nbsp; 列印物品清冊</button>
                                    <button type="button" class="btn btn-success printcardbtn"><i class="fa fa-print"></i>&nbsp; 列印物品卡</button>
                                    <button type="button" class="btn btn-secondary printtagbtn"><i class="fa fa-print"></i>&nbsp; 列印物品標籤</button>
                            </div>
                        </div>
                        <br> -->
                    <form id="checkform" name="checkform" method="post">
                        <div class="table-responsive table-responsive-data2">
                            <table id="datatable" class="table table-striped table-bordered" style="width:100%" valign="center">
                                <thead class="thead-light">
                                    <tr>
                                        <th>帳務日期</th>
                                        <th>填單日期</th>
                                        <th>單據種類</th>
                                        <th>單據字號</th>
                                        <th>傳票編號</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                
                                <tbody id="itemlist">
                                    
                                </tbody>
                                
                                <!-- <button type="button" class="btn btn-primary excelbtn"><i class="fa fa-upload"></i>&nbsp; 匯出excel</button>&nbsp;
                                <button type="button" class="btn btn-success printbtn"><i class="fa fa-print"></i>&nbsp; 列印物品清冊</button>&nbsp;
                                <button type="button" class="btn btn-success printcardbtn"><i class="fa fa-print"></i>&nbsp; 列印物品卡</button>&nbsp;
                                <button type="button" class="btn btn-secondary printtagbtn"><i class="fa fa-print"></i>&nbsp; 列印物品標籤</button><br> -->
                            
                            </table>
                        </div>
                    </form>
                    <!-- END DATA TABLE -->
                </div>
                
            </div>


