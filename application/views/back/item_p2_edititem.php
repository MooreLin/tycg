<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
var item_code = <?=json_encode($itemcode_list)?>;

function checkIsNumber(input){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);
}

$(function(){
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    $('.dateselector').datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        todayHighlight: true,
        "setDate": '109-01-01',
        "autoclose": true
    }).on("show", function (e) {
        $("div.box").css({minHeight: "480px"});
    }).on("hide", function (e) {
        $("div.box").css({minHeight: "auto"});
    });

    $(document).on('change', '#item_code', function(){
        var no = $(this).val();
        if(no == ''){
            $('#deadline').val('');
            $('#unit').val('');
        }else{
            $('#deadline').val(item_code[no]['code_item_deadline']);
            $('#unit').val(item_code[no]['code_item_unit']);
        }
        $('#unit_price').trigger('change');
    });

    $(document).on('change', '#unit_price', function(){
        var price = $('#unit_price').val();
        var amount = $('#amount').val();
        if(price != '' && amount != ''){
            $('#total_price').val(price * amount);
        }
    });

    $(document).on('change', '#amount', function(){
        var price = $('#unit_price').val();
        var amount = $('#amount').val();
        if(price != '' && amount != ''){
            $('#total_price').val(price * amount);
        }
    });
});
</script>

<style>
[data-toggle="collapse"] .fa:before {   
  content: "\f146";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f0fe";
}
</style>

<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" class="form-horizontal" action="<?=base_url('item_p2/edititem/'.$doc['doc_id'].'/'.$item['item_id'])?>">
                        <input type="hidden" id="docid" name="docid" value="<?=$doc['doc_id']?>">
                        <input type="hidden" id="itemid" name="itemid" value="<?=$item['item_id']?>">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">修改物品</strong>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">物品編號</label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <?=$item['code_item_class'].'-'.$item['code_item_project'].'-'.$item['code_item_contents'].'-'.$item['code_item_section'].'-'.$item['code_item_number'].'-'.str_pad($item['item_number'], 7, '0', STR_PAD_LEFT).' '.$item['code_item_name']?>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">別名</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="alias" name="alias" value="<?=$item['item_alias']?>">
                                    </div>
                                    <div class="col col-md-1">
                                        <label class="form-control-label">來源</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="change" name="change" required>
                                            <option value="">請選擇來源</option>
                                            <?php
                                            foreach($changecode_list as $changecode){
                                                if($changecode['code_change_id'] == $item['item_change_id']){
                                                    echo '<option value="'.$changecode['code_change_id'].'" selected>'.$changecode['code_change_allcode'].'</option>';
                                                }else{
                                                    echo '<option value="'.$changecode['code_change_id'].'">'.$changecode['code_change_allcode'].'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">形式</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="type" name="type" value="<?=$item['item_type']?>">
                                    </div>
                                    <div class="col col-md-1">
                                        <label class="form-control-label">廠牌</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="brand" name="brand" value="<?=$item['item_brand']?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">取得日期</label>
                                    </div>
                                    <div class="col-12 col-md-2 input-group">
                                        <input type="text" class="form-control dateselector" id="gettime" name="gettime" value="<?=$item['item_gettime']?>" required>
                                        <div class="input-group-addon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col col-md-1">
                                        <label class="form-control-label">年限</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="deadline" name="deadline" value="<?=$item['code_item_deadline']?>" readonly>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">購置日期</label>
                                    </div>
                                    <div class="col-12 col-md-2 input-group">
                                        <input type="text" class="form-control dateselector" id="buytime" name="buytime" value="<?=$item['item_buytime']?>" required>
                                        <div class="input-group-addon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col col-md-1">
                                        <label class="form-control-label">單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="unit" name="unit" value="<?=$item['code_item_unit']?>" readonly>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">會計科目</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="grade" name="grade" value="<?=$item['item_grade']?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">取得文號</label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <input type="text" class="form-control" id="document_number" name="document_number" value="<?=$item['item_document']?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">單價(元)</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="number" class="form-control" id="unit_price" name="unit_price" min="0" value="<?=$item['item_price']?>" required>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">存置地點</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="site" name="site" required>
                                            <option value="">請選擇存置地點</option>
                                            <?php
                                            foreach($sitecode_list as $sitecode){
                                                if($sitecode['code_site_id'] == $item['item_site']){
                                                    echo '<option value="'.$sitecode['code_site_id'].'" selected>'.$sitecode['code_site_number'].'/'.$sitecode['code_site_name'].'</option>';
                                                }else{
                                                    echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_number'].'/'.$sitecode['code_site_name'].'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">使用單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="use_site" name="use_site" required>
                                            <option value="">請選擇使用單位</option>
                                            <?php
                                            foreach($sitecode_list as $sitecode){
                                                if($sitecode['code_site_id'] == $item['item_use_site']){
                                                    echo '<option value="'.$sitecode['code_site_id'].'" selected>'.$sitecode['code_site_number'].'/'.$sitecode['code_site_name'].'</option>';
                                                }else{
                                                    echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_number'].'/'.$sitecode['code_site_name'].'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col col-md-1">
                                        <label class="form-control-label">使用人</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="use_user" name="use_user" required>
                                            <option value="">請選擇使用人</option>
                                            <?php
                                            foreach($custcode_list as $custcode){
                                                if($custcode['code_cust_id'] == $item['item_use_cust']){
                                                    echo '<option value="'.$custcode['code_cust_id'].'" selected>'.$custcode['code_cust_number'].$custcode['code_cust_name'].'</option>';
                                                }else{
                                                    echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_number'].'/'.$custcode['code_cust_name'].'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">保管單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="keep_site" name="keep_site" required>
                                            <option value="">請選擇保管單位</option>
                                            <?php
                                            foreach($sitecode_list as $sitecode){
                                                if($sitecode['code_site_id'] == $item['item_keep_site']){
                                                    echo '<option value="'.$sitecode['code_site_id'].'" selected>'.$sitecode['code_site_number'].'/'.$sitecode['code_site_name'].'</option>';
                                                }else{
                                                    echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_number'].'/'.$sitecode['code_site_name'].'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col col-md-1">
                                        <label class="form-control-label">保管人</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="keep_user" name="keep_user" required>
                                            <option value="">請選擇保管人</option>
                                            <?php
                                            foreach($custcode_list as $custcode){
                                                if($custcode['code_cust_id'] == $item['item_keep_cust']){
                                                    echo '<option value="'.$custcode['code_cust_id'].'" selected>'.$custcode['code_cust_number'].'/'.$custcode['code_cust_name'].'</option>';
                                                }else{
                                                    echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_number'].'/'.$custcode['code_cust_name'].'</option>';
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    
                                    <div class="col col-md-1">
                                        <label class="form-control-label">備註</label>
                                    </div>
                                    <div class="col-12 col-md-5">
                                        <input type="text" class="form-control" id="desc" name="desc" value="<?=$item['item_desc']?>">
                                    </div>

                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <a role="button" href="<?=base_url('item_p2/edit/'.$doc['doc_id'])?>" class="btn btn-danger">取消</a>
                                <input type="submit" class="btn btn-primary" value="修改">
                            </div>
                        </div>
                    </form>
                </div>
            </div>


