<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
        $(document).ready(function() {
            $('.cardnumid').select2();
        });
    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
<!-- Model -->
    <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">新增會員</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="cardnumid" class=" form-control-label">卡號：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="cardnumid" id="cardnumid" class="form-control" placeholder="請輸入卡號">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="membername" class=" form-control-label">姓名：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="membername" id="membername" class="form-control" placeholder="請輸入姓名">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="phone" class=" form-control-label">手機：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="phone" id="phone" class="form-control" placeholder="請輸入手機">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="email" class=" form-control-label">email：</label>
                        </div>
                        <div class="col-9">
                            <input type="email" name="email" id="email" class="form-control" placeholder="請輸入email">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="homephone" class=" form-control-label">室內電話：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="homephone" id="homephone" class="form-control" placeholder="請輸入室內電話03-456789">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="memberacc" class=" form-control-label">地址：</label>
                        </div>
                        <div class="col-9">
                            <input type="text" name="memberacc" id="memberacc" class="form-control" placeholder="請輸入地址">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
<!-- end modal large -->
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">訂單管理</h3>
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                                <div class="rs-select2--light rs-select2--md">
                                    <select class="cardnumid" name="state">
                                        <option value="">訂單編號篩選</option>
                                        <option value="AL">Alabama</option>
                                        <option value="WY">Wyoming</option>
                                    </select>
                                    <div class="dropDownSelect2"></div>
                                </div>
                            </div>
                            <div class="table-data__tool-right">
                            </div>
                        </div>
                        <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th>訂單編號</th>
                                        <th>訂購日期</th>
                                        <th>訂購會員</th>
                                        <th>收件人</th>
                                        <th>總金額</th>
                                        <th>訂單狀態</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="tr-shadow">
                                        <td>訂單編號</td>
                                        <td>訂購日期</td>
                                        <td>訂購會員</td>
                                        <td>收件人</td>
                                        <td>總金額</td>
                                        <td>訂單狀態</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="查看">
                                                    <i class="zmdi zmdi-search"></i>
                                                </button>
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>訂單編號</td>
                                        <td>訂購日期</td>
                                        <td>訂購會員</td>
                                        <td>收件人</td>
                                        <td>總金額</td>
                                        <td>訂單狀態</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="查看">
                                                    <i class="zmdi zmdi-search"></i>
                                                </button>
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>訂單編號</td>
                                        <td>訂購日期</td>
                                        <td>訂購會員</td>
                                        <td>收件人</td>
                                        <td>總金額</td>
                                        <td>訂單狀態</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="查看">
                                                    <i class="zmdi zmdi-search"></i>
                                                </button>
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>訂單編號</td>
                                        <td>訂購日期</td>
                                        <td>訂購會員</td>
                                        <td>收件人</td>
                                        <td>總金額</td>
                                        <td>訂單狀態</td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="查看">
                                                    <i class="zmdi zmdi-search"></i>
                                                </button>
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Delete">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE -->
                    </div>
                    
                </div>


