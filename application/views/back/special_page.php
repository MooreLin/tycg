<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
        $(document).ready(function() {
            $('.tags').select2();
        });
    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
<!-- Model -->
    <div class="modal fade" id="largeModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">新增產品</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group row">
                        <div class="col col-md-2">
                            <label for="bran_list" class=" form-control-label">品牌：</label>
                        </div>
                        <div class="col-12 col-md-4">
                            <select name="bran_list" id="bran_list" class="form-control-sm form-control">
                                <option value="0">請選擇</option>
                                <option value="1">品牌1</option>
                                <option value="2">品牌2</option>
                                <option value="3">品牌3</option>
                            </select>
                        </div>
                        <div class="col col-md-2">
                            <label for="food_list" class=" form-control-label">品項：</label>
                        </div>
                        <div class="col-12 col-md-4">
                            <select name="food_list" id="food_list" class="form-control-sm form-control">
                                <option value="0">請選擇</option>
                                <option value="1">香氛蠟燭</option>
                                <option value="2">蘆竹香精</option>
                                <option value="3">香氛系列</option>
                                <option value="4">香氛加熱器</option>
                                <option value="5">禮盒/配件</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col col-md-2">
                            <label for="p_name" class=" form-control-label">產品名稱：</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" name="p_name" id="p_name" class="form-control" placeholder="請輸入產品名稱">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col col-md-2">
                            <label for="p_number" class=" form-control-label">產品編號：</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" name="p_number" id="p_number" class="form-control" placeholder="請輸入產品編號">
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <div class="col col-md-2">
                            <label for="p_number" class=" form-control-label">產品說明：</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <textarea name="textarea-input" id="textarea-input" rows="9" placeholder="請輸入產品說明" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col col-md-2">
                            <label for="p_package" class=" form-control-label">包裝：</label>
                        </div>
                        <div class="col-12 col-md-4">
                            <input type="text" name="p_package" id="p_package" class="form-control" placeholder="請輸入包裝">
                        </div>
                        <div class="col col-md-2">
                            <label for="p_weight" class=" form-control-label">重量：</label>
                        </div>
                        <div class="col-12 col-md-4">
                            <input type="text" name="p_weight" id="p_weight" class="form-control" placeholder="請輸入重量">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col col-md-2">
                            <label for="p_burning" class=" form-control-label">燃燒時間：</label>
                        </div>
                        <div class="col-12 col-md-4">
                            <input type="text" name="p_burning" id="p_burning" class="form-control" placeholder="請輸入燃燒時間">
                        </div>
                        <div class="col col-md-2">
                            <label for="p_status" class=" form-control-label">狀態：</label>
                        </div>
                        <div class="col-12 col-md-4">
                            <select name="p_status" id="p_status" class="form-control-sm form-control">
                                <option value="0">請選擇</option>
                                <option value="1">上線</option>
                                <option value="2">隱藏</option>
                            </select>
                        </div>
                    </div>

                    
                    <div class="form-group row">
                        <div class="col-2">
                            <label for="p_discount" class=" form-control-label">會員折扣：</label>
                        </div>
                        <div class="col-10">
                            <input type="text" name="p_discount" id="p_discount" class="form-control" placeholder="請輸入折扣"> <span style="color:red">9折請打90，88折請打88</span>
                        </div>
                    </div>

                    
                    <div class="form-group row">
                        <div class="col-5 offset-2">
                            <label for="soloout" class="form-check-label ">
                                <input type="checkbox" id="soloout" name="soloout" value="option1" class="form-check-input">Soloout
                            </label>
                        </div>
                        <div class="col-5">
                            <label for="inline-checkbox1" class="form-check-label ">
                                <input type="checkbox" id="inline-checkbox1" name="inline-checkbox1" value="option1" class="form-check-input">加入優惠組合
                            </label>
                            
                            <select name="p_discount_cp" id="p_discount_cp">
                                <option value="1">選擇1</option>
                                <option value="2">選擇2</option>
                                <option value="3">選擇3</option>
                                <option value="4">選擇4</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-2 offset-2">
                                <label for="inline-checkbox1" class="form-check-label ">
                                    <input type="checkbox" id="inline-checkbox1" name="inline-checkbox1" value="option1" class="pr-1 form-check-input">特價商品
                                </label>
                        </div>
                        <div class="col-2">
                                <label for="p_smoney" class=" form-control-label">特價：NT.</label>
                        </div>
                        <div class="col-6">
                                <input type="text" name="p_smoney" id="p_smoney" class="form-control" placeholder="請輸入特價價格">
                        </div>

                    </div>

                    <div class="form-group row">
                        <div class="col-2">
                            <label for="p_fmoney" class=" form-control-label">價格：</label>
                        </div>
                        <div class="col-4">
                            <input type="text" name="p_fmoney" id="p_fmoney" class="form-control" placeholder="請輸入價格">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-2">
                            <label for="tags" class=" form-control-label">標籤：</label>
                        </div>
                        <div class="col-10">
                            <select class="tags" name="tags[]" id="tags" multiple="multiple">
                                <option value="AL">Alabama</option>
                                <option value="WY">Wyoming</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-2">
                            <label for="images" class=" form-control-label">上傳圖片：</label>
                        </div>
                        <div class="col-10">
                            <input type="file" id="images" name="images[]" multiple="" class="form-control-file">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>
<!-- end modal large -->
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">特價商品管理</h3>
                        <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        
                                        <th>產品名稱</th>
                                        <th style="width: 150px;">產品編號</th>
                                        <th>價格</th>
                                        <th>特價價格</th>
                                        <th style="width: 150px;">圖片</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="tr-shadow">
                                        <td>商品名稱</td>
                                        <td>產品編號</td>
                                        <td>價格</td>
                                        <td>特價價格</td>
                                        <td><img src="<?=base_url().'images/b2.jpg';?>" alt="產品名稱"></td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>商品名稱</td>
                                        <td>產品編號</td>
                                        <td>價格</td>
                                        <td>特價價格</td>
                                        <td><img src="<?=base_url().'images/b2.jpg';?>" alt="產品名稱"></td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>商品名稱</td>
                                        <td>產品編號</td>
                                        <td>價格</td>
                                        <td>特價價格</td>
                                        <td><img src="<?=base_url().'images/b2.jpg';?>" alt="產品名稱"></td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>商品名稱</td>
                                        <td>產品編號</td>
                                        <td>價格</td>
                                        <td>特價價格</td>
                                        <td><img src="<?=base_url().'images/b2.jpg';?>" alt="產品名稱"></td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item" data-toggle="tooltip" data-placement="top" title="Edit">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE -->
                    </div>
                    
                </div>


