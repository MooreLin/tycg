<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
function checkIsNumber(input){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);
}

$(function(){
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    $('.dateselector').datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        todayHighlight: true,
        "setDate": '109-01-01',
        "autoclose": true
    }).on("show", function (e) {
        $("div.box").css({minHeight: "480px"});
    }).on("hide", function (e) {
        $("div.box").css({minHeight: "auto"});
    });
});
</script>

<style>
[data-toggle="collapse"] .fa:before {   
  content: "\f146";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f0fe";
}
</style>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form method="post" class="form-horizontal" action="<?=base_url('item_p2/adddoc')?>">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">新增物品增加單</strong>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">填單日期</label>
                                    </div>
                                    <div class="col-12 col-md-2 input-group">
                                        <input type="text" name="write_date" class="form-control dateselector" value="<?=(date('Y')-1911).date('-m-d')?>" required>
                                        <div class="input-group-addon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">填造單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" value="<?=$this->session->userdata('unit')?>" readonly>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">傳票編號</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" name="type" class="form-control" required>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">帳務日期</label>
                                    </div>
                                    <div class="col-12 col-md-2 input-group">
                                        <input type="text" name="accounts_date" class="form-control dateselector" value="<?=(date('Y')-1911).date('-m-d')?>" required>
                                        <div class="input-group-addon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">單據字號</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" value="<?=date('Y')-1911?>-物增-<?=str_pad($number_now, 7, '0', STR_PAD_LEFT)?>" readonly>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">物品區分別</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control" id="area" name="area" required>
                                            <?php
                                            foreach($itemarea_list as $itemarea){
                                                echo '<option value="'.$itemarea['code_item_area_id'].'">'.$itemarea['code_item_area_code'].' '.$itemarea['code_item_area_name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <input type="reset" class="btn btn-danger" value="重設">
                                <input type="submit" class="btn btn-primary" value="新增">
                            </div>
                        </div>
                    </form>
                </div>
            </div>


