<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- MAIN CONTENT-->
<div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">品牌列表</h3>
                        
                        <div class="table-data__tool-left">
                        </div>
                        <!-- <div class="table-data__tool">
                            <div class="table-data__tool-right">
                                <button class="au-btn au-btn-icon au-btn--green au-btn--small">
                                    <i class="zmdi zmdi-plus"></i>新增品牌</button>
                            </div>
                        </div> -->
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <div class="table-responsive table-responsive-data2">
                            <table class="table table-data2">
                                <thead>
                                    <tr>
                                        <th style="width:200px">品牌名稱</th>
                                        <th>品牌介紹</th>
                                        <th>產品代表圖</th>
                                        <th style="width: 100px;">狀態</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr class="tr-shadow">
                                        <td>Yankeecandle</td>
                                        <td>
                                            <details>
                                                <summary>詳情簡介</summary>
                                                <p>Epcot is a theme park at Walt Disney World Resort featuring exciting attractions, international pavilions, award-winning fireworks and seasonal special events.</p>
                                            </details>
                                        </td>
                                        <td><img src="<?=base_url().'images/bimage.jpg';?>" alt=""></td>
                                        <td><select name="" id="">
                                                <option value="Y" selected="selected">上架</option>
                                                <option value="N">隱藏</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>Aquiesse</td>
                                        <td>
                                            <details>
                                                <summary>詳情簡介</summary>
                                                <p>Epcot is a theme park at Walt Disney World Resort featuring exciting attractions, international pavilions, award-winning fireworks and seasonal special events.</p>
                                            </details>
                                        </td>
                                        <td><img src="<?=base_url().'images/bimage.jpg';?>" alt=""></td>
                                        <td><select name="" id="">
                                                <option value="Y" selected="selected">上架</option>
                                                <option value="N">隱藏</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                    <tr class="tr-shadow">
                                        <td>Warmer</td>
                                        <td>
                                            <details>
                                                <summary>詳情簡介</summary>
                                                <p>Epcot is a theme park at Walt Disney World Resort featuring exciting attractions, international pavilions, award-winning fireworks and seasonal special events.</p>
                                            </details>
                                        </td>
                                        <td><img src="<?=base_url().'images/bimage.jpg';?>" alt=""></td>
                                        <td><select name="" id="">
                                                <option value="Y" selected="selected">上架</option>
                                                <option value="N">隱藏</option>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="spacer"></tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE -->
                    </div>
                </div>