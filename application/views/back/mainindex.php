<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!-- MAIN CONTENT-->
<div class="main-content">
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
                        <h2 class="title-1 m-b-25">待辦單據</h2>
                        <div class="table-responsive table--no-card m-b-40">
                            <table class="table table-borderless table-striped table-earning">
                                <thead>
                                    <tr>
                                        <th>帳務日期</th>
                                        <th>填單日期</th>
                                        <th>單據種類</th>
                                        <th>單據編號</th>
                                        <th>物品區分別</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach($uncheckDoc_list as $doc): ?>
                                        <tr>
                                            <td><?=$doc['doc_accounts_date']?></td>
                                            <td><?=$doc['doc_write_date']?></td>
                                            <td><?=$doc['doc_num_type']?></td>
                                            <td><?=$doc['doc_num_year'].'-'.$doc['doc_num_type'].'-'.str_pad($doc['doc_num_number'], 7, '0', STR_PAD_LEFT)?></td>
                                            <td><?=$doc['code_item_area_code'].' '.$doc['code_item_area_name']?></td>
                                            <td>
                                                <?php if($this->authority_array['item_option2'] == 'Y'):?>
                                                <?php
                                                if($doc['doc_num_type'] == '物增'){
                                                    echo '<a class="btn btn-warning" href="'.base_url('item_p2/edit/'.$doc['doc_id']).'">修改</a>';
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }elseif($doc['doc_num_type'] == '物移'){
                                                    echo '<a class="btn btn-warning" href="'.base_url('item_p3/edit/'.$doc['doc_id']).'">修改</a>';
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }elseif($doc['doc_num_type'] == '增減'){
                                                    echo '<a class="btn btn-warning" href="'.base_url('item_p4/edit/'.$doc['doc_id']).'">修改</a>';
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }elseif($doc['doc_num_type'] == '報廢'){
                                                    echo '<a class="btn btn-warning" href="'.base_url('item_p6/edit/'.$doc['doc_id']).'">修改</a>';
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }elseif($doc['doc_num_type'] == '撥出'){
                                                    echo '<a class="btn btn-warning" href="'.base_url('item_p7/edit/'.$doc['doc_id']).'">修改</a>';
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }elseif($doc['doc_num_type'] == '基修'){
                                                    echo '<a class="btn btn-warning" href="'.base_url('item_p8/edit/'.$doc['doc_id']).'">修改</a>';
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }elseif($doc['doc_num_type'] == '廢品'){
                                                    echo '<a class="btn btn-warning" href="'.base_url('item_p9/edit/'.$doc['doc_id']).'">修改</a>';
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }elseif($doc['doc_num_type'] == '盤報廢'){
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }elseif($doc['doc_num_type'] == '盤移動'){
                                                    echo '<a class="btn btn-info" href="'.base_url('item_p5/export_pdf/'.$doc['doc_id']).'">列印</a>';
                                                }
                                                ?>
                                                <a class="btn btn-danger" href="<?=base_url('Mainview/deldoc/'.$doc['doc_id'])?>">刪除</a>
                                            </td>
                                            <?php endif;?>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>