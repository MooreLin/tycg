<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
    
var datatable;
function checkIsNumber(input){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);
}

function pad(num, n) {
    var len = num.toString().length;
    while(len < n) {
        num = "0" + num;
        len++;
    }
    return num;
}


$(function(){
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    //$('#datatable').DataTable();

    $('.dateselector').datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        todayHighlight: true,
    }).on("show", function (e) {
        $("div.box").css({minHeight: "480px"});
    }).on("hide", function (e) {
        $("div.box").css({minHeight: "auto"});
    });

    $(document).on('change', '#selectall', function(){
        $('input[name="selects[]"]').prop('checked', $(this).prop('checked'));
        // $('input[name="selects[]"]').serialize()
    });

    $(document).on('submit', '#searchform', function(){
        $('#datatable').DataTable().destroy();
        $.post("<?=base_url('inventory/getitem')?>", $("#searchform").serialize(), function(result){
            console.log(result);
            result = JSON.parse(result);
            console.log(result);
            $('#itemlist').html('');
            var itemhtml = '';
            for(i in result){
                itemhtml += '<tr>';
                // itemhtml += '<td><input type="checkbox" name="selects[]" id="selects[]" value="'+result[i]['item_id']+'"></td>';
                itemhtml += '<td>'+result[i]['inventory_date']+'</td>';
                itemhtml += '<td>'+result[i]['count_item']+'</td>';
                itemhtml += '<td>'+result[i]['count_status']+'</td>';
                itemhtml += '<td><button type="button "class="editbtn" title="編輯" data-no="'+result[i]['inventory_id']+'"><i class="fa fa-edit"></i></button></td>';
                
                itemhtml += '</tr>';
            }
            $('#itemlist').html(itemhtml);
            datatable = $('#datatable').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
                'columnDefs': [{
                    'targets': [0],
                    'searchable': false,
                    'orderable': false,
                }],
                "language": {
                    "url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
                }
            });
        });

        return false;
    });
    // 補0
    function paddingLeft(str,lenght){
        if(str.length >= lenght)
        return str;
        else
        return paddingLeft("0" +str,lenght);
    }
    
    // 匯出pdf
    $(document).on('click', '.printbtn', function(){
        no = $(this).data('no');
        var url = "<?=base_url('item_p5/export_pdf/')?>"+no;
        window.location.replace(url);
        return false;
        // location.reload();
    });

    // 編輯
    $(document).on('click', '.editbtn', function(){
        no = $(this).data('no');
        console.log(no);
            url = "<?=base_url('inventory/detailed/')?>" + no;
        window.location.replace(url);
        return false;
        // var url = "<?=base_url('item_p5/export_pdf?doc_id=')?>"+no;
        // window.open(url, '匯出', config='height=600,width=800');
        // location.reload();
    });
});

    
</script>

<style>
[data-toggle="collapse"] .fa:before {   
  content: "\f146";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f0fe";
}
</style>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">盤點紀錄列表</h3>
                    <div class="table-data__tool">
                        <div id="accordion" style="width:100%;">
                            <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer;">
                                    <h5 class="mb-0">
                                        <i class="fa"></i> 查詢條件
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <form id="searchform" name="searchform" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="card-body">
                                            
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">盤點日期</label>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="date_start" name="date_start" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="date_end" name="date_end" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-footer text-right">
                                            <input type="reset" class="btn btn-danger" value="重設">
                                            <input type="submit" class="btn btn-primary" value="查詢">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!-- <div class="row">
                            <div class="col-md-5 offset-md-7 col-sm-6 ml-auto">
                                    <button type="button" class="btn btn-primary excelbtn"><i class="fa fa-upload"></i>&nbsp; 匯出excel</button>
                                    <button type="button" class="btn btn-success printbtn"><i class="fa fa-print"></i>&nbsp; 列印物品清冊</button>
                                    <button type="button" class="btn btn-success printcardbtn"><i class="fa fa-print"></i>&nbsp; 列印物品卡</button>
                                    <button type="button" class="btn btn-secondary printtagbtn"><i class="fa fa-print"></i>&nbsp; 列印物品標籤</button>
                            </div>
                        </div>
                        <br> -->
                    <form id="checkform" name="checkform" method="post">
                        <div class="table-responsive table-responsive-data2">
                            <table id="datatable" class="table table-striped table-bordered" style="width:100%" valign="center">
                                <thead class="thead-light">
                                    <tr>
                                        <th>盤點日期</th>
                                        <th>總帳面數量</th>
                                        <th>總實際數量</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                
                                <tbody id="itemlist">
                                    
                                </tbody>
                                
                                <!-- <button type="button" class="btn btn-primary excelbtn"><i class="fa fa-upload"></i>&nbsp; 匯出excel</button>&nbsp;
                                <button type="button" class="btn btn-success printbtn"><i class="fa fa-print"></i>&nbsp; 列印物品清冊</button>&nbsp;
                                <button type="button" class="btn btn-success printcardbtn"><i class="fa fa-print"></i>&nbsp; 列印物品卡</button>&nbsp;
                                <button type="button" class="btn btn-secondary printtagbtn"><i class="fa fa-print"></i>&nbsp; 列印物品標籤</button><br> -->
                            
                            </table>
                        </div>
                    </form>

                    
                    <!-- END DATA TABLE -->
                </div>
                
            </div>


