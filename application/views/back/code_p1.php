<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  
    <script>
        $(document).ready(function() {
            $('.organ_list').select2({
                theme: "bootstrap4"
            });
        });
        $(document).ready( function () {
            $('#table_id').DataTable({
                "lengthMenu": [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]]
            });
        } );

        // 清空model
        $(document).on('click','.addbtn',function(){
            
            $('#editnotify #largeModalLabel').text('新增物品代碼');
            $('#editnotify #type').val('add');
            $('#editnotify #code_item_id').val('');
            $('#editnotify #organ_list').val('X');
            $('#editnotify #organ_list').trigger('change');
            $('#editnotify #code_item_class').val('');
            $('#editnotify #code_item_project').val('');
            $('#editnotify #code_item_contents').val('');
            $('#editnotify #code_item_section').val('');
            $('#editnotify #code_item_number').val('');
            $('#editnotify #code_item_name').val('');
            $('#editnotify #code_item_deadline').val('');
            $('#editnotify #code_item_unit').val('');
        });

        // Delbtn
        $(document).on('click','.delbtn',function(){
            var code_item_id = $(this).data('no');
            Swal.fire({
                title: "確定要刪除這筆資料嗎?",
                text: "一經刪除後將不可復原!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是，我要刪除!!',
                cancelButtonText: '取消'
            })
            .then((result) =>{
                if (result.isConfirmed) {
                    $.post("<?=base_url('code_p1/del')?>",{code_item_id:code_item_id},function(json){
                        if(json == "true"){
                            // success
                            Swal.fire({title: "刪除成功",icon: "success"}).then(function(value){
                                window.location.reload();;
                            });
                        }else{
                            // error
                            Swal.fire({title: "刪除時發生錯誤",icon: "warning"});
                            return false;
                        }
                    });
                }
            });
        });

        // editbtn show modal
        $(document).on('click','.editbtn',function(){
            code_item_id =$(this).data('no');
            $.post('<?=base_url('Code_p1/getid')?>',{code_item_id:code_item_id},function(json){
                console.log(json);
                $('#editnotify #largeModalLabel').text('修改物品代碼')
                $('#editnotify #type').val('edit');
                $('#editnotify #code_item_id').val(json['code_item_id']);
                $('#editnotify #organ_list').val(json['organ_id']);
                $('#organ_list').trigger('change');
                $('#editnotify #code_item_class').val(json['code_item_class']);
                $('#editnotify #code_item_project').val(json['code_item_project']);
                $('#editnotify #code_item_contents').val(json['code_item_contents']);
                $('#editnotify #code_item_section').val(json['code_item_section']);
                $('#editnotify #code_item_number').val(json['code_item_number']);
                $('#editnotify #code_item_name').val(json['code_item_name']);
                $('#editnotify #code_item_deadline').val(json['code_item_deadline']);
                $('#editnotify #code_item_unit').val(json['code_item_unit']);
            },'json');
            // $('#editnotify #notify').val(allnotify[$(this).data('no')]['M01I02NV0040']);
            // $('#editnotify #timestart').val(allnotify[$(this).data('no')]['M01N01DD0003']);
            // $('#editnotify #timestop').val(allnotify[$(this).data('no')]['M01N02DD0003']);
            $('#memberModal').modal('show');
        });

        // Model submit
        $(document).on('submit','#editnotify',function(){
            $.ajax({
                url: "<?=base_url('code_p1/send')?>",
                data: $('#editnotify').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",icon: "error"});
                        return false;
                    }
                }
            });
            return false;
        });

    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
<!-- Model -->
    <form class="form-signin" id="editnotify" method="post" role="form">
        <input type="hidden" name="type" id="type" value="add">
        <input type="hidden" name="code_item_id" id="code_item_id">
        <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="largeModalLabel">新增物品代碼</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">    
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="organ_num" class=" form-control-label">機關代碼：</label>
                            </div>
                            <div class="col-9">
                                <select class="organ_list" name="organ_list" id="organ_list">
                                    <option value="X">查詢機關名稱</option>
                                    <?php foreach ($organ_list as $key => $organ) :?>
                                        <option value="<?=$organ['organ_id']?>"><?=$organ['organ_code']?> / <?=$organ['organ_name']?></option>
                                    <?php endforeach;?>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="code_item_class" class=" form-control-label">代碼-類：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code_item_class" id="code_item_class" class="form-control" placeholder="請輸入代碼-類" maxlength="1" oninput="value=value.replace(/[^\d]/g,'')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="code_item_project" class=" form-control-label">代碼-項：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code_item_project" id="code_item_project" class="form-control" placeholder="請輸入代碼-項" maxlength="2" oninput="value=value.replace(/[^\d]/g,'')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="code_item_contents" class=" form-control-label">代碼-目：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code_item_contents" id="code_item_contents" class="form-control" placeholder="請輸入代碼-目" maxlength="2" oninput="value=value.replace(/[^\d]/g,'')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="code_item_section" class=" form-control-label">代碼-節：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code_item_section" id="code_item_section" class="form-control" placeholder="請輸入代碼-節" maxlength="2" oninput="value=value.replace(/[^\d]/g,'')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="code_item_number" class=" form-control-label">代碼-號：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code_item_number" id="code_item_number" class="form-control" placeholder="請輸入代碼-號" maxlength="4" oninput="value=value.replace(/[^\d]/g,'')">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="code_item_name" class=" form-control-label">物品名稱：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code_item_name" id="code_item_name" class="form-control" placeholder="請輸入物品名稱" maxlength="30" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="code_item_deadline" class=" form-control-label">物品年限：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code_item_deadline" id="code_item_deadline" class="form-control" placeholder="請輸入物品年限" maxlength="2" >
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="code_item_unit" class=" form-control-label">物品單位：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="code_item_unit" id="code_item_unit" class="form-control" placeholder="請輸入物品單位" maxlength="4" >
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
<!-- end modal large -->
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">物品代碼</h3>
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                            </div>
                            <div class="table-data__tool-right">
                                
                                <?php if($this->authority_array['code_option3'] == 'Y' || $this->user_group =='S'):?>
                                    <a class="au-btn au-btn-icon au-btn--blue au-btn--small" href="<?=base_url('code_p1/export')?>" target="_blank" role="button"><i class="zmdi zmdi-upload"></i>匯出物品代碼</a>
                                <?php endif;?>
                                <?php if($this->authority_array['code_option2'] == 'Y' || $this->user_group =='S'):?>
                                    <button class="au-btn au-btn-icon au-btn--green au-btn--small addbtn" data-toggle="modal" data-target="#memberModal">
                                        <i class="zmdi zmdi-plus"></i>新增物品代碼</button>
                                <?php endif;?>
                            </div>
                        </div>
                        <div class="table-responsive table-responsive-data2">
                            <table id="table_id" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th style="width:15%">機關代碼</th>
                                        <th style="width:15%">物品代碼</th>
                                        <th style="width:5%">類</th>
                                        <th style="width:5%">項</th>
                                        <th style="width:5%">目</th>
                                        <th style="width:5%">節</th>
                                        <th style="width:5%">號</th>
                                        <th style="width:15%">物品名稱</th>
                                        <th style="width:5%">年限</th>
                                        <th style="width:5%">單位</th>
                                        <th style="width:5%"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($code_list as $key => $value) :?>
                                    <tr>
                                        <td><?=$value['organ_code']?></td>
                                        <td><?=$value['code_item_class'].$value['code_item_project'].$value['code_item_contents'].$value['code_item_section'].$value['code_item_number']?></td>
                                        <td><?=$value['code_item_class']?></td>
                                        <td><?=$value['code_item_project']?></td>
                                        <td><?=$value['code_item_contents']?></td>
                                        <td><?=$value['code_item_section']?></td>
                                        <td><?=$value['code_item_number']?></td>
                                        <td><?=$value['code_item_name']?></td>
                                        <td><?=$value['code_item_deadline']?></td>
                                        <td><?=$value['code_item_unit']?></td>
                                        <td>
                                            
                                            <?php if($this->authority_array['code_option2'] == 'Y'){?>
                                            <div class="table-data-feature">
                                                <button class="item editbtn" data-toggle="tooltip" data-placement="top" title="Edit" data-no="<?=$value['code_item_id'];?>">
                                                    <i class="zmdi zmdi-edit"></i>
                                                </button>
                                                <button class="item delbtn" data-toggle="tooltip" data-placement="top" title="Delete" data-no="<?=$value['code_item_id'];?>">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                            <?php }else{
                                                echo "　";
                                             }?>
                                        </td>
                                    </tr>
                                <?php endforeach;?>
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE -->
                    </div>
                </div>


