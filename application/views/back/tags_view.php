<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <style>
        table{
            border-collapse: collapse;
            width: 770px; 	
            /*自動斷行*/
            word-wrap: break-word;
            table-layout: fixed;
        }
    </style> -->
</head>
        <!-- 物品卡
        <tr>
            <th width="56.69pt">　</th>
            <th width="44.50pt">　</th>
            <th width="44.50pt">　</th>
            <th width="44.50pt">　</th>
            <th width="56.69pt">　</th>
            <th width="44.50pt">　</th>
            <th width="44.50pt">　</th>
            <th width="56.69pt">　</th>
            <th width="44.50pt">　</th>
            <th width="44.50pt">　</th>
            <th width="44.50pt">　</th>
        </tr> -->

        <!-- 盤點清冊
        <tr>
                <th width="178.58pt">　</th>
                <th width="61.51pt">　</th>
                <th width="46.48pt">　</th>
                <th width="28.62pt">　</th>
                <th width="32.31pt">　</th>
                <th width="57.82pt">　</th>
                <th width="67.46pt">　</th>
                <th width="70.58pt">　</th>
            <th width="74.26pt">　</th>
            <th width="68.31pt">　</th>
            <th width="42.80pt">　</th>
            <th width="52.44pt">　</th>
        </tr> -->
        <tr>
                <th width="113.38pt"></th>
                    <th width="85.03pt"></th>
                    <th width="42.51pt"></th>
                    <th width="42.51pt"></th>
                <th width="42.51pt"></th>
                <th width="73.70pt"></th>
                <th width="73.70pt"></th>
                <th width="53.85pt"></th>
                <th width="39.68pt"></th>
        </tr>
<body>
        <table style="text-align:left;font-size:10pt" class="table">
            <tr>
                <th width="566.87pt" colspan="9">'.$msg[$now_key]['organ_name'].'</th>
            </tr>
            <tr>
                <th width="566.87pt" colspan="9">物品盤存表</th>
            </tr>
            <tr>
                <th width="113.38pt">印表人:</th>
                <th width="359.96pt" colspan="6">'.$this->user_name.'</th>
                <th width="53.85pt">頁次:</th>
                <th width="39.68pt">'.$i.'</th>
            </tr>
            <tr>
                <th width="113.38pt">印表日期:</th>
                <th width="359.96pt" colspan="6">'.$nowdate.'</th>
                <th width="53.85pt">總頁數:</th>
                <th width="39.68pt">'.$pagenumber.'</th>
            </tr>
        </table>
        <table border="1" style="text-align:left;font-size:10pt" class="table">
            <tr>
                <th width="113.38pt">物品編號</th>
                <th width="170.05pt" colspan="3">物品名稱</th>
                <th width="42.51pt">單位</th>
                <th width="73.70pt">帳面數量</th>
                <th width="73.70pt">實際數量</th>
                <th width="93.53pt" colspan="2">備註</th>
            </tr>
            <tr>
                <th width="113.38pt">6010102-0012</th>
                <th width="170.05pt" colspan="3">印表機</th>
                <th width="42.51pt">台</th>
                <th width="73.70pt">3</th>
                <th width="73.70pt">3</th>
                <th width="93.53pt" colspan="2"></th>
            </tr>
        </table>
        <table style="text-align:left;font-size:10pt" class="table">
            <tr>
                <th width="325.94pt" colspan="5">　</th>
                <th width="73.70pt">帳面總計:</th>
                <th width="73.70pt">實際總計:</th>
                <th width="93.53pt" colspan="2">　</th>
            </tr>
            <tr>
                <th heigh="99pt" width="198.41pt" colspan="2">製表</th>
                <th heigh="99pt" width="127.53pt" colspan="3">監盤人</th>
                <th heigh="99pt" width="147.4pt" colspan="2">主辦會計</th>
                <th heigh="99pt" width="93.53pt" colspan="2">機關首長</th>
            </tr>
        </table>
        <!-- 盤點清冊
        <table style="text-align:left;font-size:10pt" class="table">
            <tr>
                <th width="178.58pt">印表人:</th>
                <th width="61.51pt">'.$this->user_name.'</th>
                <th width="445.84pt" colspan="8" align="center">'.$organ_name['organ_name'].'</th>
                <th width="42.80pt">頁次:</th>
                <th width="52.44pt">'.$i.'</th>
            </tr>
            
            <tr>
                <th width="178.58pt">印表日期:</th>
                <th width="61.51pt">'.$nowdate.'</th>
                <th width="445.84pt" colspan="8" align="center">物品盤點清冊</th>
                <th width="42.80pt">總頁數:</th>
                <th width="52.44pt">'.$pagenumber.'</th>
            </tr>
            <tr>
                <th width="781.17pt" colspan="12" align="center">　</th>
            </tr>
        </table>
        
        <table border="1" style="text-align:left;font-size:10pt" class="table">
            <tr>
                <th width="178.58pt" rowspan="2">物品編號</th>
                <th width="61.51pt">物品名稱</th>
                <th width="46.48pt">型式</th>
                <th width="28.62pt">單位</th>
                <th width="32.31pt" rowspan="2">價值</th>
                <th width="57.82pt">購買日期</th>
                <th width="67.46pt">使用年限</th>
                <th width="70.58pt" rowspan="2">存置地點</th>
                <th width="74.26pt" rowspan="2">使用單位</th>
                <th width="68.31pt" rowspan="2">保管單位</th>
                <th width="42.80pt" rowspan="2">保管人</th>
                <th width="52.44pt" rowspan="2">檢查情形</th>
            </tr>
            <tr>
                <th width="61.51pt">物品別名</th>
                <th width="46.48pt">廠牌</th>
                <th width="28.62pt">數量</th>
                <th width="57.82pt">取得日期</th>
                <th width="67.46pt">已使用年限</th>
            </tr>
            <tr>
                <th width="178.58pt">6010102-0016-0000025</th>
                <th width="61.51pt">印表機</th>
                <th width="46.48pt">BY-1234</th>
                <th width="28.62pt">台</th>
                <th width="32.31pt" rowspan="2">6500</th>
                <th width="57.82pt">097/12/23</th>
                <th width="67.46pt">5年</th>
                <th width="70.58pt" rowspan="2">A123會計室</th>
                <th width="74.26pt" rowspan="2">A123會計室</th>
                <th width="68.31pt" rowspan="2">A123會計室</th>
                <th width="42.80pt" rowspan="2">李大同</th>
                <th width="52.44pt" rowspan="2">已盤點</th>
            </tr>
            <tr>
                <th width="178.58pt">110-60101010016-000000025</th>
                <th width="61.51pt">桌上型印表機</th>
                <th width="46.48pt">EPSON</th>
                <th width="28.62pt">1</th>
                <th width="57.82pt">098/08/23</th>
                <th width="67.46pt">11年4月</th>
            </tr>
        </table>
        <table border="1" style="text-align:left;font-size:12pt" class="table">
            <tr>
                <th width="781.17pt" colspan="12">合計　　　　總筆數:'.($now_key - $c_num).'　　　　合計:'.$total_price.'</th>
            </tr>
        </table>
        <table style="text-align:left;font-size:12pt" class="table">
            <tr>
                <th width="781.17pt" colspan="12" align="center">　</th>
            </tr>
            <tr>
                <th width="543.36pt" colspan="8">主辦物品管理人員/盤點人員</th>
                <th width="237.81pt" colspan="4">主辦會計/監盤人員</th>
            </tr>
            <tr>
                <th height="99pt" colspan="8" rowspan="3">　</th>
                <th height="99pt" colspan="4" rowspan="3">　</th>
            </tr>
        </table> -->
        <!-- <table  style="text-align:left;font-size:10pt" class="table">
            <tr>
                <th width="56.69pt">印表人:</th>
                <th width="44.50pt" align="left">李大同</th>
                <th width="335.88pt" colspan="7" align="center">欣華資訊有限公司</th>
                <th width="44.50pt">頁次</th>
                <th width="44.50pt">1</th>
            </tr>
            <tr>
                <th width="56.69pt">印表日期:</th>
                <th width="44.50pt">109/04/02</th>
                <th width="335.88pt" colspan="7" align="center">物品卡</th>
                <th width="44.50pt">總頁數</th>
                <th width="44.50pt">1</th>
            </tr>
            <tr>
                <th width="56.69pt">管理機關:</th>
                <th width="469.38pt" colspan="10" align="left">A11046600F001</th>
            </tr>
            <tr>
                <th width="56.69pt">物品號:</th>
                <th width="133.5pt" colspan="3">6010102-0016-0000025</th>
                <th width="56.69pt">物品名稱</th>
                <th width="89pt" colspan="2">印表機</th>
                <th width="56.69pt">物品別名:</th>
                <th width="133.5pt" colspan="3">雷射印表機</th>
            </tr>
            <tr>
                <th width="526.07pt" colspan="11">　</th>
            </tr>
            <tr>
                <th width="190.19pt" colspan="4">**************************************</th>
                <th width="145.69pt" colspan="3" align="center">管理資料</th>
                <th width="190.19pt" colspan="4">**************************************</th>
            </tr>
                <tr>
                    <th width="56.69pt">型式:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">最低使用年限:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">廠牌:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">購置日期:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">單位:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">取得日期:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">數量:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">存置地點:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">價值:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">使用單位:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">來源:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">使用人:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">其他事項:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">保管單位:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">附屬設備:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">保管人:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
            <tr>
                <th width="526.07pt" colspan="11">　</th>
            </tr>
            <tr>
                <th width="190.19pt" colspan="4">**************************************</th>
                <th width="145.69pt" colspan="3" align="center">帳務資料</th>
                <th width="190.19pt" colspan="4">**************************************</th>
            </tr>
                <tr>
                    <th width="56.69pt">登記日期:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2">摘要:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">登記憑證:</th>
                    <th width="190.19pt" colspan="4">　</th>
                    <th widht="89pt" colspan="2" rowspan="2">入帳金額:</th>
                    <th widht="190.19pt" colspan="5" rowspan="2">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">登記字號:</th>
                    <th width="190.19pt" colspan="4">　</th>
                </tr>
                <tr>
                    <th width="526.07" colspan="11">次序:</th>
                </tr>
                <tr>
                    <th width="56.69pt">登記憑證:</th>
                    <th width="190.19pt" colspan="4">109-10-29</th>
                    <th widht="89pt" colspan="2">前次帳面金額:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">登記字號:</th>
                    <th width="190.19pt" colspan="4">109-增減-0000001</th>
                    <th widht="89pt" colspan="2">本次增加金額:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt" rowspan="2">摘要:</th>
                    <th width="190.19pt" colspan="4" rowspan="2">05增值</th>
                    <th widht="89pt" colspan="2">本次減少金額:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th widht="89pt" colspan="2">帳面金額:</th>
                    <th widht="190.19pt" colspan="5">7700</th>
                </tr>
                <tr>
                    <th width="526.07pt" colspan="11">　</th>
                </tr>
            <tr>
                <th width="190.19pt" colspan="4">**************************************</th>
                <th width="145.69pt" colspan="3" align="center">報廢資料</th>
                <th width="190.19pt" colspan="4">**************************************</th>
            </tr>
                
                <tr>
                    <th width="56.69pt" rowspan="2">日期:</th>
                    <th width="190.19pt" colspan="4">109/11/12</th>
                    <th widht="89pt" colspan="2" rowspan="4">報廢後處理情形:</th>
                    <th widht="190.19pt" colspan="5">　</th>
                </tr>
                <tr>
                    <th width="56.69pt" rowspan="2">單據字號:</th>
                    <th width="469.38pt" colspan="4">109-報廢-0000001</th>
                </tr>
                <tr>
                    <th width="56.69pt" rowspan="2">原因:</th>
                    <th width="469.38pt" colspan="4">04報廢</th>
                </tr>
                <tr>
                    <th width="56.69pt" rowspan="2">奉准文號:</th>
                    <th width="469.38pt" colspan="4"></th>
                </tr>
                <tr>
                    <th width="526.07pt" colspan="11">　</th>
                </tr>
            <tr>
                <th width="190.19pt" colspan="4">**************************************</th>
                <th width="145.69pt" colspan="3" align="center">異動紀錄</th>
                <th width="190.19pt" colspan="4">**************************************</th>
            </tr>
                <tr>
                    <th width="526.07" colspan="11">次序:</th>
                </tr>
                <tr>
                    <th width="56.69pt">異動日期:</th>
                    <th width="469.38pt" colspan="10">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">異動原因:</th>
                    <th width="469.38pt" colspan="10">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">異動事項:</th>
                    <th width="469.38pt" colspan="10">　</th>
                </tr>
                <tr>
                    <th width="56.69pt">其他事項:</th>
                    <th width="469.38pt" colspan="10">　</th>
                </tr>
        </table> -->
    <!-- 物品清冊 -->
        <!-- <th width="131.24pt"></th>
        <th width="125.29pt"></th>
        <th width="169.51pt"></th>
        <th width="31.46pt"></th>
        <th width="51.02pt"></th>
        <th width="41.95pt"></th>
        <th width="65.19pt"></th>
        <th width="53.85pt"></th>
        <th width="52.44pt"></th>
        <th width="51.02pt"></th>
        <th width="51.02pt"></th> -->

        <!-- <table border="1" style="text-align:left;font-size:12pt" class="table">
            <tr>
                <th width="306.12pt" height="49.32pt" colspan="3">主辦會計人員</th>
                <th width="100.24pt"colspan="2" rowspan="2" align="center" valign="top">撥入機關首長</th>
                <th width="210.19pt"colspan="4" >主辦會計人員</th>
                <th width="125.02pt"colspan="3" rowspan="2" align="center" valign="top">撥出機關首長</th>
            </tr>
            <tr>
                <th width="306.12pt" height="49.32pt" colspan="3" rowspan="6">主辦物品管理人員</th>
                <th width="210.19pt" colspan="4" rowspan="6">主辦物品人員</th>
            </tr>
        </table>
        <table style="text-align:left;font-size:12pt" class="table">
            <tr>
                <th width="741.57pt" colspan="12">說明:本單共分三聯，第一聯存根聯，第二聯登記聯，第三聯通知聯</th>
            </tr>
        </table> -->
</body>
</html>
<!-- 增減
<tr>
                <th width="70.58pt"></th>
                <th width="66.61pt"></th>
                <th width="53.85pt"></th>
                <th width="51.02pt"></th>
                <th width="51.02pt"></th>   
                <th width="40.53pt"></th>
                    <th width="55.55pt"></th>
                    <th width="55.55pt"></th>
                <th width="55.55pt"></th>
                    <th width="55.55pt"></th>
                    <th width="55.55pt"></th>
                    <th width="55.55pt"></th>
                    <th width="55.55pt"></th>
                    <th width="55.55pt"></th>
</tr> -->

<!-- 移動
<tr>
    <th width="70.58pt"></th>
    <th width="66.61pt"></th>
    <th width="53.85pt"></th>
    <th width="51.02pt"></th>
    <th width="51.02pt"></th>
    <th width="94.39pt"></th>
    <th width="66.04pt"></th>
    <th width="45.07pt"></th>
    <th width="64.62pt"></th>
    <th width="51.87pt"></th>
    <th width="68.31pt"></th>
    <th width="43.37pt"></th>
    <th width="65.19pt"></th>
</tr> -->

<!-- 報廢
    <tr>
            <th width="70.58pt"></th>
            <th width="66.61pt"></th>
            <th width="51.02pt"></th>
            <th width="30.61pt"></th>
            <th width="51.02pt"></th>
            <th width="51.02pt"></th>
            <th width="92.97pt"></th>
            <th width="51.02pt"></th>
            <th width="41.38pt"></th>
            <th width="49.60pt"></th>
            <th width="36.85pt"></th>
        <th width="68.31pt"></th>
        <th width="71.14pt"></th>
        <th width="65.19pt"></th>
    </tr>
 -->
 <!-- 撥出單
<tr>
    <th width="51.02pt"></th>
    <th width="85.60pt"></th>
    <th width="169.5pt"></th>
    <th width="44.22pt"></th>
    <th width="51.02pt"></th>
    <th width="51.02pt"></th>
        <th width="51.02pt"></th>
        <th width="40.53pt"></th>
        <th width="64.62pt"></th>
    <th width="45.07pt"></th>
    <th width="45.07pt"></th>
    <th width="34.88pt"></th>
 </tr> -->

 <!-- 撥出單結尾
<tr>
    <th width="51.02pt" height="16.44pt">主辦會計</th>
    <th width="255.1pt" height="49.32pt" colspan="2" rowspan="3"></th>
    <th width="95.24pt" colspan="2">撥入機關</th>
    <th width="51.02pt">主辦會計</th>
    <th width="156.17pt" height="49.32pt" colspan="3" rowspan="3"></th>
    <th width="125.02pt" colspan="3" align="left">撥入機關</th>
 </tr>
 <tr>
     <th height="32.88pt">人員</th>
     <th rowspan="3" colspan="2">首長</th>
     <th height="32.88pt">人員</th>
     <th height="32.88pt" colspan="3" rowspan="3">首長</th>
 </tr>
 <tr>
    <th width="51.02pt" height="16.44pt">主辦物品</th>
    <th width="255.1pt" height="49.32pt" colspan="2" rowspan="3"></th>
    <th width="51.02pt">主辦物品</th>
    <th width="156.17pt" height="49.32pt" colspan="3" rowspan="3"></th>
 </tr>
 <tr>
     <th height="32.88pt">管理人員</th>
     <th height="32.88pt">人員</th>
 </tr> -->

 <!-- <tr>
    <th width="114.80pt"></th>
    <th width="125.29pt"></th>
    <th width="169.51pt"></th>
    <th width="31.46pt"></th>
    <th width="51.02pt"></th>
    <th width="41.95pt"></th>
    <th width="65.19pt"></th>
    <th width="53.85pt"></th>
    <th width="52.44pt"></th>
    <th width="51.02pt"></th>
    <th width="51.02pt"></th>
 
 </tr> -->