<?php
defined('BASEPATH') OR exit('No direct script access allowed');header("Content-Security-Policy: upgrade-insecure-requests");
?>

    <script>
        $(document).ready(function() {
            $('.cardnumid').select2();
        });
        
        $(document).ready( function () {
            $('#list_tables').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
            });
        } );

        // editbtn show modal
        $(document).on('click','.editbtn',function(){
            authority_id =$(this).data('no');
            $.post('<?=base_url('authority/getid')?>',{authority_id:authority_id},function(json){
                console.log(json);
                $('#editnotify #largeModalLabel').text('修改權限功能')
                $('#editnotify #type').val('edit');
                $('#editnotify #authority_name').val(json['authority_name']);
                $('#editnotify #authority_id').val(json['authority_id']);
                
                if(json['item_option1'] == 'Y'){$('#item_check1').attr("checked","checked")};
                if(json['item_option2'] == 'Y'){$('#item_check2').attr("checked","checked")};
                if(json['item_option3'] == 'Y'){$('#item_check3').attr("checked","checked")};
                if(json['code_option1'] == 'Y'){$('#code_check1').attr("checked","checked")};
                if(json['code_option2'] == 'Y'){$('#code_check2').attr("checked","checked")};
                if(json['code_option3'] == 'Y'){$('#code_check3').attr("checked","checked")};
                if(json['monthly_option1'] == 'Y'){$('#monthly_check1').attr("checked","checked")};
                if(json['monthly_option2'] == 'Y'){$('#monthly_check2').attr("checked","checked")};
                if(json['monthly_option3'] == 'Y'){$('#monthly_check3').attr("checked","checked")};
                if(json['inventory_option1'] == 'Y'){$('#inventory_check1').attr("checked","checked")};
                if(json['inventory_option2'] == 'Y'){$('#inventory_check2').attr("checked","checked")};
                if(json['inventory_option3'] == 'Y'){$('#inventory_check3').attr("checked","checked")};
            },'json');
            // $('#editnotify #notify').val(allnotify[$(this).data('no')]['M01I02NV0040']);
            // $('#editnotify #timestart').val(allnotify[$(this).data('no')]['M01N01DD0003']);
            // $('#editnotify #timestop').val(allnotify[$(this).data('no')]['M01N02DD0003']);
            $('#memberModal').modal('show');
        });

        $(document).on('click','.addbtn',function(){
            
            $('#editnotify #largeModalLabel').text('新增機關');
            $('#editnotify #type').val('add');
            $('#editnotify #organ_id').val('');
            $('#editnotify #organ_code').val('');
            $('#editnotify #organ_name').val('');
            $('#editnotify #organ_inventory').val('');
            $('#editnotify #organ_inventory').trigger('change');
        });

        // Model submit
        $(document).on('submit','#editnotify',function(){
            $.ajax({
                url: "<?=base_url('authority/send')?>",
                data: $('#editnotify').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",icon: "error"});
                        return false;
                    }
                }
            });
            return false;
        });
    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
<!-- Model -->
    <form class="form-signin" id="editnotify" method="post" role="form">
        <input type="hidden" name="type" value="add" id="type">
        <input type="hidden" name="authority_id" id="authority_id">
        <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="largeModalLabel">權限群組</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="authority_name" class=" form-control-label">群組名稱：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="authority_name" id="authority_name" class="form-control">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="organ_name" class=" form-control-label">物品管理：</label>
                            </div>
                            <div class="col-9">
                                <div class="form-check-inline form-check">
                                    <label for="item_check1" class="form-check-label ">
                                    <input type="checkbox" id="item_check1" name="item_check1" value="check_true" class="form-check-input">查看
                                    </label>
                                    <label for="item_check2" class="form-check-label ">
                                    <input type="checkbox" id="item_check2" name="item_check2" value="check_true" class="form-check-input">新增/修改/刪除
                                    </label>
                                    <label for="item_check3" class="form-check-label ">
                                    <input type="checkbox" id="item_check3" name="item_check3" value="check_true" class="form-check-input">列印及匯出
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="organ_name" class=" form-control-label">代碼管理：</label>
                            </div>
                            <div class="col-9">
                                <div class="form-check-inline form-check">
                                    <label for="code_check1" class="form-check-label ">
                                    <input type="checkbox" id="code_check1" name="code_check1" value="check_true" class="form-check-input">查看
                                    </label>
                                    <label for="code_check2" class="form-check-label ">
                                    <input type="checkbox" id="code_check2" name="code_check2" value="check_true" class="form-check-input">新增/修改/刪除
                                    </label>
                                    <label for="code_check3" class="form-check-label ">
                                    <input type="checkbox" id="code_check3" name="code_check3" value="check_true" class="form-check-input">列印及匯出
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="organ_name" class=" form-control-label">月結功能：</label>
                            </div>
                            <div class="col-9">
                                <div class="form-check-inline form-check">
                                    <label for="monthly_check1" class="form-check-label ">
                                    <input type="checkbox" id="monthly_check1" name="monthly_check1" value="check_true" class="form-check-input">查看
                                    </label>
                                    <label for="monthly_check2" class="form-check-label ">
                                    <input type="checkbox" id="monthly_check2" name="monthly_check2" value="check_true" class="form-check-input">新增/修改/刪除
                                    </label>
                                    <label for="monthly_check3" class="form-check-label ">
                                    <input type="checkbox" id="monthly_check3" name="monthly_check3" value="check_true" class="form-check-input">列印及匯出
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="organ_name" class=" form-control-label">盤點功能：</label>
                            </div>
                            <div class="col-9">
                                <div class="form-check-inline form-check">
                                    <label for="inventory_check1" class="form-check-label ">
                                    <input type="checkbox" id="inventory_check1" name="inventory_check1" value="check_true" class="form-check-input">查看
                                    </label>
                                    <label for="inventory_check2" class="form-check-label ">
                                    <input type="checkbox" id="inventory_check2" name="inventory_check2" value="check_true" class="form-check-input">新增/修改/刪除
                                    </label>
                                    <label for="inventory_check3" class="form-check-label ">
                                    <input type="checkbox" id="inventory_check3" name="inventory_check3" value="check_true" class="form-check-input">列印及匯出
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
<!-- end modal large -->
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">權限群組</h3>
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                            </div>
                            <div class="table-data__tool-right">
                            </div>
                        </div>
                            <table id="list_tables" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>權限群組名稱</th>
                                        <th>物品管理</th>
                                        <th>代碼管理</th>
                                        <th>月結功能</th>
                                        <th>盤點功能</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($authority_list as $key => $value) {?>
                                        <tr class="tr-shadow">
                                            <td><?=$value['authority_name']?></td>
                                            <td>查看：<?=$value['item_option1']?><br>新/修/刪：<?=$value['item_option2']?><br>匯出及列印：<?=$value['item_option3']?></td>
                                            <td>查看：<?=$value['code_option1']?><br>新/修/刪：<?=$value['code_option2']?><br>匯出及列印：<?=$value['code_option3']?></td>
                                            <td>查看：<?=$value['monthly_option1']?><br>新/修/刪：<?=$value['monthly_option2']?><br>匯出及列印：<?=$value['monthly_option3']?></td>
                                            <td>查看：<?=$value['inventory_option1']?><br>新/修/刪：<?=$value['inventory_option2']?><br>匯出及列印：<?=$value['inventory_option3']?></td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <button class="item editbtn" data-toggle="tooltip" data-placement="top" title="Edit" data-no="<?=$value['authority_id']?>">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php }?>
                                        
                                </tbody>
                            </table>
                        <!-- END DATA TABLE -->
                    </div>
                    
                </div>


