<?php
defined('BASEPATH') OR exit('No direct script access allowed');header("Content-Security-Policy: upgrade-insecure-requests");
?>

    <script>
        $(document).ready(function() {
            $('.cardnumid').select2();
        });
        
        $(document).ready( function () {
            $('#list_tables').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
            });
        } );

        // 清空truncatebtn
        $(document).on('click','.truncatebtn',function(){
            Swal.fire({
                title: "確定要清空所有資料嗎?",
                text: "一經清空後將不可復原!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是，我要清空!!',
                cancelButtonText: '取消'
            })
            .then((result) =>{
                if (result.isConfirmed) {
                    $.post('<?=base_url('Member_p1/truncate')?>',function(json){
                        console.log(json);
                        if(json == true){
                            // success
                            Swal.fire({title: "清空成功",icon: "success"}).then(function(value){
                                window.location.reload();;
                            });
                        }else{
                            // error
                            Swal.fire({title: "清空時發生錯誤",icon: "warning"});
                            return false;
                        }
                    },'json');
                }
            });
            
        });

        // editbtn show modal
        $(document).on('click','.editbtn',function(){
            organ_id =$(this).data('no');
            $.post('<?=base_url('Member_p1/getid')?>',{organ_id:organ_id},function(json){
                console.log(json);
                $('#editnotify #largeModalLabel').text('修改機關')
                $('#editnotify #type').val('edit');
                $('#editnotify #organ_id').val(json['organ_id']);
                $('#editnotify #organ_code').val(json['organ_code']);
                $('#editnotify #organ_name').val(json['organ_name']);
                $('#editnotify #organ_inventory').val(json['organ_inventory']);
                $('#editnotify #organ_inventory').trigger('change');
            },'json');
            // $('#editnotify #notify').val(allnotify[$(this).data('no')]['M01I02NV0040']);
            // $('#editnotify #timestart').val(allnotify[$(this).data('no')]['M01N01DD0003']);
            // $('#editnotify #timestop').val(allnotify[$(this).data('no')]['M01N02DD0003']);
            $('#memberModal').modal('show');
        });

        // Delbtn
        $(document).on('click','.delbtn',function(){
            var organ_id = $(this).data('no');
            // console.log(organ_id);
            Swal.fire({
                title: "確定要刪除這筆資料嗎?",
                text: "一經刪除後將不可復原!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是，我要刪除!!',
                cancelButtonText: '取消'
            })
            .then((result) =>{
                if (result.isConfirmed) {
                    $.post("<?=base_url('member_p1/del')?>",{organ_id:organ_id},function(json){
                        if(json == "true"){
                            // success
                            Swal.fire({title: "刪除成功",icon: "success"}).then(function(value){
                                window.location.reload();;
                            });
                        }else{
                            // error
                            Swal.fire({title: "刪除時發生錯誤",icon: "warning"});
                            return false;
                        }
                    });
                }
            });
        });

        $(document).on('click','.addbtn',function(){
            
            $('#editnotify #largeModalLabel').text('新增機關');
            $('#editnotify #type').val('add');
            $('#editnotify #organ_id').val('');
            $('#editnotify #organ_code').val('');
            $('#editnotify #organ_name').val('');
            $('#editnotify #organ_inventory').val('');
            $('#editnotify #organ_inventory').trigger('change');
        });

        // Model submit
        $(document).on('submit','#editnotify',function(){
            $.ajax({
                url: "<?=base_url('member_p1/send')?>",
                data: $('#editnotify').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",icon: "error"});
                        return false;
                    }
                }
            });
            return false;
        });
    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
<!-- Model -->
    <form class="form-signin" id="editnotify" method="post" role="form">
        <input type="hidden" name="type" value="add" id="type">
        <input type="hidden" name="organ_id" id="organ_id">
        <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="largeModalLabel">新增機關</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="organ_code" class=" form-control-label">機關代碼：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="organ_code" id="organ_code" class="form-control" placeholder="請輸入機關代碼" maxlength="13" onkeyup="value=value.replace(/[\W]/g,'')">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="organ_name" class=" form-control-label">機關名稱：</label>
                            </div>
                            <div class="col-9">
                                <input type="text" name="organ_name" id="organ_name" class="form-control" placeholder="請輸入機關名稱" maxlength="60">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-2">
                                <label for="organ_inventory" class=" form-control-label">購買盤點：</label>
                            </div>
                            <div class="col-9">
                            <select class="organ_inventory form-control" name="organ_inventory" id="organ_inventory">
                                    <option value="">請選狀態</option>
                                    <option value="Y">是</option>
                                    <option value="N">否</option>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
<!-- end modal large -->
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">機關管理</h3>
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                            </div>
                            <div class="table-data__tool-right">
                                <!-- <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#memberModal">
                                    <i class="zmdi zmdi-upload"></i>匯入機關資料</button> -->
                                <button class="btn btn-danger truncatebtn" data-toggle="tooltip" data-placement="top" title="清空資料">
                                        <i class="zmdi zmdi-close"></i>清空資料</button>
                                <button class="au-btn au-btn-icon au-btn--green au-btn--small addbtn" data-toggle="modal" data-target="#memberModal">
                                    <i class="zmdi zmdi-plus"></i>新增機關</button>
                            </div>
                        </div>
                        <!-- <div class="table-responsive table-responsive-data2"> -->
                            <table id="list_tables" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>機關代碼</th>
                                        <th>機關名稱</th>
                                        <th>更新日期</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($organ_list as $key => $value) :?>
                                        <tr class="tr-shadow">
                                            <td><?=$value['organ_code']?></td>
                                            <td><?=$value['organ_name']?></td>
                                            <td><?=$value['organ_update_datetime']?></td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <button class="item editbtn" data-toggle="tooltip" data-placement="top" title="Edit" data-no="<?=$value['organ_id']?>">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item delbtn" data-toggle="tooltip" data-placement="top" title="Delete" data-no="<?=$value['organ_id']?>">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        <!-- </div> -->
                        <!-- END DATA TABLE -->
                    </div>
                    
                </div>


