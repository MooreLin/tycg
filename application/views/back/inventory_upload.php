<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>

    $(document).on('submit','#import',function(){
		
		if($('#import #file').val() == ""){
			Swal.fire({title: "請選擇要上傳的檔案",icon: "warning"});
			return false;
		}
		if(!type.test($("#import #file").val())){ 
			Swal.fire({title: "只允許上傳副檔名為json,txt的檔案",icon: "warning"});	
			return false;
		}
		var first = true;
		var total;
		
	});
</script>

<style>
[data-toggle="collapse"] .fa:before {   
  content: "\f146";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f0fe";
}
</style>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            
			<div class="row">
				<div class="col-md-12">
					<h3 class="title-5 m-b-35">上傳盤點資料</h3>
				</div>
			</div>
            <div class="row">
				<div class="col-md-4 offset-md-4">
					<form id="import" name="import" action="inventory/upload" method="POST" enctype="multipart/form-data">
						<div class="form-group row">
							<label for="file" class="col-sm-3 col-form-label">匯入檔案</label>
							<div class="col-sm-9">
								<input type="file" class="form-control-file" id="file" name="file" accept=".json,.txt">
							</div>
						</div>
						<button type="submit" class="btn btn-primary btn-block">上傳檔案</button>
					</form>
				</div>
			</div>
			<br><br><br><br><br><br>
			<div class="row">
				<div class="col-md-4 offset-md-4">
					<a href="<?=base_url('inventory/delete')?>" class="btn btn-danger btn-block">刪除系統暫存盤點資料</a>
				</div>
            </div>
           

