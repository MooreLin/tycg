<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
var doc_id = '<?=$doc['doc_id']?>';
var datatable;

function checkIsNumber(input){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);
}

$(function(){
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    datatable = $('#datatable').DataTable({
        'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
        "language": {
            "url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
        }
    });

    $('.dateselector').datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        todayHighlight: true,
        "setDate": '109-01-01',
        "autoclose": true
    }).on("show", function (e) {
        $("div.box").css({minHeight: "480px"});
    }).on("hide", function (e) {
        $("div.box").css({minHeight: "auto"});
    });

    $(document).on('click', '#selectall', function(){
        var rows = datatable.rows({ 'search': 'applied' }).nodes();
        $('input[name="selects[]"]', rows).prop('checked', this.checked);
    });

    $(document).on('click', '.print_tag_btn', function(){
        var form = $('#editform');
        if(datatable != null){
            $('#editform input[type="hidden"][name="selects[]"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#editform input[name="selects[]"]:checked').length + $('#editform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }

        $('#editform').attr('action','<?=base_url('item_p1/print_tag')?>');
        $('#editform').submit();
    });

    $(document).on('click', '.editbtn', function(){
        var form = $('#editform');
        if(datatable != null){
            $('#editform input[type="hidden"][name="selects[]"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#editform input[name="selects[]"]:checked').length + $('#editform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }

        $('#editform').attr('action','<?=base_url('item_p8/editItem')?>');
        $('#editform').submit();
    });

    $(document).on('click', '.delbtn', function(){
        var item_id = $(this).data('no');
        Swal.fire({
            title: '確定要刪除?',
            text: "刪除後物品將回復移動前狀態",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            confirmButtonText: '確定',
            cancelButtonText: '取消'
        }).then((result) => {
            if (result.isConfirmed) {
                $.post('<?=base_url('item_p8/deleteItem')?>', {doc_id:doc_id,item_id:item_id}, function(res){
                    if(res == 'true'){
                        Swal.fire('刪除成功','','success').then(() => {location.reload()});
                    }else{
                        Swal.fire('刪除時發生錯誤','','error');
                    }
                });
            }
        });
        return false;
    });

    $(document).on('submit', '#header_from', function(){
        if($('#header_from #write_date').val() == ''){
            Swal.fire('請輸入填單日期','','warning');
            return false;
        }
        if($('#header_from #accounts_date').val() == ''){
            Swal.fire('請輸入帳務日期','','warning');
            return false;
        }
        if($('#header_from #type').val() == ''){
            Swal.fire('請輸入傳票編號','','warning');
            return false;
        }
        $.post('<?=base_url('item_p8/editDocHeader')?>', $(this).serialize(), function(result){
            if(result == 'true'){
                Swal.fire('修改成功','','success').then(() => {location.reload()});
            }else{
                Swal.fire('修改時發生錯誤','','error');
            }
        });
        return false;
    });
});
</script>
<!-- MAIN CONTENT-->

<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <form id="header_from" name="header_from" method="post" class="form-horizontal">
                        <input type="hidden" name="doc_id" value="<?=$doc['doc_id']?>">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title mb-3">物品基本資料修改單</strong>
                            </div>
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">填單日期</label>
                                    </div>
                                    <div class="col-12 col-md-2 input-group">
                                        <input type="text" id="write_date" name="write_date" class="form-control dateselector" value="<?=$doc['doc_write_date']?>">
                                        <div class="input-group-addon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">填造單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" value="<?=$doc['code_write_allcode']?>" readonly>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">傳票編號</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" id="type" name="type" class="form-control" value="<?=$doc['doc_type']?>">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">帳務日期</label>
                                    </div>
                                    <div class="col-12 col-md-2 input-group">
                                        <input type="text" id="accounts_date" name="accounts_date" class="form-control dateselector" value="<?=$doc['doc_accounts_date']?>">
                                        <div class="input-group-addon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">單據字號</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" value="<?=$doc['doc_num_year'].'-'.$doc['doc_num_type'].'-'.str_pad($doc['doc_num_number'], 7, '0', STR_PAD_LEFT)?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <input type="submit" class="btn btn-primary" value="修改表頭">
                            </div>
                        </div>
                    </form>
                    <form id="editform" name="editform" method="post" enctype="multipart/form-data" class="form-horizontal" action="">
                        <?php if($this->authority_array['item_option3'] == 'Y'):?>
                            <button type="button" class="au-btn au-btn-icon au-btn--blue au-btn--small print_tag_btn"><i class="fa fa-print"></i>列印標籤</button>
                        <?php endif;?>
                        <?php if($this->authority_array['item_option2'] == 'Y'):?>
                            <a class="au-btn au-btn-icon au-btn--blue au-btn--small addbtn" href="<?=base_url('item_p8/?year='.$doc['doc_num_year'].'&number='.$doc['doc_num_number'])?>"><i class="fa fa-plus"></i>新增物品至單</a>
                        <?php endif;?>
                        <!--<a class="au-btn au-btn-icon au-btn--blue au-btn--small addbtn" href="<?=base_url('item_p5/export_pdf/'.$doc['doc_id'])?>"><i class="fa fa-print"></i>列印單據</a>-->
                        <input type="hidden" id="doc_id" name="doc_id" value="<?=$doc['doc_id']?>">
                        <table id="datatable" class="table table-striped table-bordered" style="width:100%" valign="center">
                            <thead class="thead-light">
                                <tr>
                                    <th><input type="checkbox" id="selectall"></th>
                                    <th>購置日期<br>取得日期</th>
                                    <th>物品編號<br>物品名稱</th>
                                    <th>形式/廠牌</th>
                                    <th>單位</th>
                                    <th>使用年限</th>

                                    <th>新別名<br>新形式<br>新廠牌</th>
                                    <th>新會計科目<br>新取得文號</th>
                                    <th>新來源<br>新存置地點</th>
                                    <th>新保管人<br>新保管單位</th>
                                    <th>新使用人<br>新使用單位</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($item_list as $item): ?>
                                    <tr>
                                        <td><input type="checkbox" name="selects[]" value="<?=$item['item_id']?>"></td>
                                        <td><?=$item['item_buytime']?><br><?=$item['item_gettime']?></td>
                                        <td><?=$item['code_item_class']?>-<?=$item['code_item_project']?>-<?=$item['code_item_contents']?>-<?=$item['code_item_section']?>-<?=$item['code_item_number']?>-<?=str_pad($item['item_number'], 7, '0', STR_PAD_LEFT)?><br><?=$item['code_item_name']?></td>
                                        <td><?=mb_substr($item['item_type'],0,20,'utf8');?><br><?=mb_substr($item['item_brand'],0,20,'utf8');?></td>
                                        <td><?=$item['code_item_unit']?></td>
                                        <td><?=$item['code_item_deadline']?></td>
                                        
                                        <td><?=$item['to']['alias']!=''?$item['to']['alias']:'(空白)'?><br><?=$item['to']['type']!=''?$item['to']['type']:'(空白)'?><br><?=$item['to']['brand']!=''?$item['to']['brand']:'(空白)'?></td>
                                        <td><?=$item['to']['grade']!=''?$item['to']['grade']:'(空白)'?><br><?=$item['to']['document']!=''?$item['to']['document']:'(空白)'?><br><?=$item['to']['desc']!=''?$item['to']['desc']:'(空白)'?></td>
                                        <td><?=$changecode_list[$item['to']['change']]['code_change_allcode']?><br><?=$sitecode_list[$item['to']['site']]['code_site_name']?></td>
                                        <td><?=$custcode_list[$item['to']['keep_cust']]['code_cust_name']?><br><?=$sitecode_list[$item['to']['keep_site']]['code_site_name']?></td>
                                        <td><?=$custcode_list[$item['to']['use_cust']]['code_cust_name']?><br><?=$sitecode_list[$item['to']['use_site']]['code_site_name']?></td>
                                        <td>
                                            <div class="table-data-feature">
                                                <button class="item delbtn" data-toggle="tooltip" data-placement="top" title="刪除" data-no="<?=$item['item_id'];?>">
                                                    <i class="zmdi zmdi-delete"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <?php if($this->authority_array['item_option2'] == 'Y'):?>
                            <div class="card" style="margin-top: 50px;">
                                <div class="card-header">
                                    <h5 class="mb-0">
                                        批次修改
                                    </h5>
                                </div>

                                
                                <div class="card-body">
                                <div class="row form-group">
                                        <div class="col col-md-1">
                                            <label class="form-control-label">別名</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="text" class="form-control" id="alias" name="alias" placeholder="不修改請留空">
                                        </div>
                                        <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">形式</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="text" class="form-control" id="type" name="type" placeholder="不修改請留空">
                                        </div>
                                        <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">廠牌</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="text" class="form-control" id="brand" name="brand" placeholder="不修改請留空">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-1">
                                            <label class="form-control-label">會計科目</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="text" class="form-control" id="grade" name="grade" placeholder="不修改請留空">
                                        </div>
                                        <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">取得文號</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="text" class="form-control" id="document_number" name="document_number" placeholder="不修改請留空">
                                        </div>
                                        <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">物品備註</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="text" class="form-control" id="desc" name="desc" placeholder="不修改請留空">
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-1">
                                            <label class="form-control-label">來源</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <select class="form-control select2" id="change" name="change">
                                                <option value="">不修改</option>
                                                <?php
                                                foreach($changecode_list as $changecode){
                                                    echo '<option value="'.$changecode['code_change_id'].'">'.$changecode['code_change_allcode'].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">存置地點</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <select class="form-control select2" id="site" name="site">
                                                <option value="">不修改</option>
                                                <?php
                                                foreach($sitecode_list as $sitecode){
                                                    echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <!-- <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">年限</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="text" class="form-control" id="deadline" name="deadline" oninput="value=value.replace(/[^\d]/g,'')">
                                        </div> -->
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-1">
                                            <label class="form-control-label">保管單位</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <select class="form-control select2" id="keep_site" name="keep_site">
                                                <option value="">不修改</option>
                                                <?php
                                                foreach($sitecode_list as $sitecode){
                                                    echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">使用單位</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <select class="form-control select2" id="use_site" name="use_site">
                                                <option value="">不修改</option>
                                                <?php
                                                foreach($sitecode_list as $sitecode){
                                                    echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <!-- <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">單位</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <input type="text" class="form-control" id="unit" name="unit">
                                        </div> -->
                                    </div>
                                    <div class="row form-group">
                                        <div class="col col-md-1">
                                            <label class="form-control-label">保管人</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <select class="form-control select2" id="keep_cust" name="keep_cust">
                                                <option value="">不修改</option>
                                                <?php
                                                foreach($custcode_list as $custcode){
                                                    echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col col-md-1 offset-md-1">
                                            <label class="form-control-label">使用人</label>
                                        </div>
                                        <div class="col-12 col-md-2">
                                            <select class="form-control select2" id="use_cust" name="use_cust">
                                                <option value="">不修改</option>
                                                <?php
                                                foreach($custcode_list as $custcode){
                                                    echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer text-right">
                                    <button type="button" class="btn btn-primary editbtn">確定修改<button>
                                </div>
                            
                            </div>
                        <?php endif;?>
                    </form>
                </div>
            </div>


