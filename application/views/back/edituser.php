<?php
defined('BASEPATH') OR exit('No direct script access allowed');header("Content-Security-Policy: upgrade-insecure-requests");
?>

<script>
        $(document).ready(function() {
            $('.organ_list').select2({
                theme: "bootstrap4",
                reload: true
            });
            $('.member_group').select2({
                theme: "bootstrap4"
            });
            $('.member_unit').select2({
                theme:"bootstrap4"
            });
        });
        
        $(document).ready( function () {
            $('#list_tables').DataTable();
        } );

        $(document).ready(function(){
            var member_id = '<?=$this->user_id;?>';
            $.post("<?=base_url('edituser/getid')?>",{member_id:member_id},function(json){
                $('#editnotify #largeModalLabel').text('修改機關帳號')
                $('#editnotify #type').val('edit');
                $('#editnotify #member_id').val(json['member_id']);
                $('#editnotify #organ_list').val(json['organ_id']);
                $('#editnotify #organ_code').val(json['organ_code']);
                $('#editnotify #organ_name').val(json['organ_name']);
                $('#editnotify #member_unit').val(json['member_unit']);
                $('#editnotify #code_write_allcode').val(json['code_write_allcode']);
                $('#editnotify #member_name').val(json['member_name']);
                $('#editnotify #member_acc').val(json['member_acc']);
                // $('#editnotify #member_pwd').val(json['member_pwd']);
                $('#editnotify #member_group').val(json['member_group']);
                if(json['member_group'] == "A"){
                    $('#editnotify #group').val('機關管理人');
                }else{
                    $('#editnotify #group').val('機關使用人');
                }
            },'json');
        });
        
        // Model submit
        $(document).on('submit','#editnotify',function(){
            $.ajax({
                url: "<?=base_url('edituser/send')?>",
                data: $('#editnotify').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",icon: "error"});
                        return false;
                    }
                }
            });
            return false;
        });

        $(document).on('submit','#edittag',function(){
            $.ajax({
                url: "<?=base_url('edituser/edittag')?>",
                data: $('#edittag').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",icon: "error"});
                        return false;
                    }
                }
            });
            return false;
        });
    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
    <form class="form-signin" id="editnotify" method="post" role="form">
        <div class="section__content section__content--p30" id="">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">使用者管理</h3>
                        <!-- <div class="table-responsive table-responsive-data2"> -->
                            <input type="hidden" name="organ_list" id="organ_list">
                            <input type="hidden" name="member_id" id="member_id">
                            <input type="hidden" name="member_unit" id="member_unit">
                            <input type="hidden" name="member_group" id="member_group">
                            <input type="hidden" name="type" id="type">
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="organ_code" class=" form-control-label">機關代碼：</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" name="organ_code" id="organ_code" class="form-control" placeholder="請輸入機關名稱" maxlength="60" readonly="readonly">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="organ_name" class=" form-control-label">機關名稱：</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" name="organ_name" id="organ_name" class="form-control" placeholder="請輸入機關名稱" maxlength="60" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="code_write_allcode" class=" form-control-label">填造單位：</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" name="code_write_allcode" id="code_write_allcode" class="form-control" placeholder="請輸入機關名稱" maxlength="60" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="member_name" class=" form-control-label">帳號使用人名稱：</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" name="member_name" id="member_name" class="form-control" placeholder="請輸入帳號使用人名稱" maxlength="8" readonly="readonly">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="member_acc" class=" form-control-label">帳號：</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" name="member_acc" id="member_acc" class="form-control" placeholder="請輸入帳號" maxlength="20" onkeyup="value=value.replace(/[\W]/g,'')" readonly="readonly">
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="member_pwd" class=" form-control-label">密碼：</label>
                                </div>
                                <div class="col-8">
                                    <input type="password" name="member_pwd" id="member_pwd" class="form-control" placeholder="請輸入密碼" maxlength="20" onkeyup="value=value.replace(/[\W]/g,'')">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-3">
                                    <label for="group" class=" form-control-label">權限群組：</label>
                                </div>
                                <div class="col-8">
                                    <input type="text" name="group" id="group" class="form-control" placeholder="請輸入帳號使用人名稱" maxlength="8" readonly="readonly">
                                </div>
                            </div>
                            
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                        <!-- </div> -->
                        <!-- END DATA TABLE -->
                    </div>
                    
                </div>
            </div>
        </div>
    </form>

    <form class="form-signin" id="edittag" method="post" role="form">
                                    <input type="hidden" name="check_id" value="<?=$tag_list['check_id'];?>">
        <div class="section__content section__content--p30" id="">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            
                            <style type="text/css">
                                table {
                                        table-layout:fixed;word-break:break-all;
                                }
                            </style>
                            <!-- DATA TABLE -->
                            <h3 class="title-5 m-b-35">標籤管理</h3>
                            <table class="table">
                                <tr>
                                    <th>名稱</th>
                                    <th>機關名稱</th>
                                    <th>機關編號</th>
                                    <th>物品名稱</th>
                                    <th>物品別名</th>
                                    <th>取得及購置日期</th>
                                    <th>物品年限</th>
                                    <th>保管單位</th>
                                    <th>保管人</th>
                                    <th>存置地點</th>
                                    <th>廠牌及型式</th>
                                </tr>
                                <tr>
                                    <th>勾選印出項目</th>
                                    <th><input type="checkbox" id="organ_name" name="organ_name" value="Y" <?php if($tag_list['check_organ_name'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="organ_code" name="organ_code" value="Y"<?php if($tag_list['check_code'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="item_name" name="item_name" value="Y"<?php if($tag_list['check_item_name'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="item_alias" name="item_alias" value="Y"<?php if($tag_list['check_item_alias'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="getandbuy" name="getandbuy" value="Y"<?php if($tag_list['check_getandbuy'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="item_deadline" name="item_deadline" value="Y"<?php if($tag_list['check_item_deadline'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="keep_site" name="keep_site" value="Y"<?php if($tag_list['check_keep_site'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="keep_cust" name="keep_cust" value="Y"<?php if($tag_list['check_keep_cust'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="item_site" name="item_site" value="Y"<?php if($tag_list['check_site'] =='Y'){echo 'checked';}?>></th>
                                    <th><input type="checkbox" id="typeandbrand" name="typeandbrand" value="Y"<?php if($tag_list['check_typeandbrand'] =='Y'){echo 'checked';}?>></th>
                                </tr>
                            </table>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">儲存</button>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </form>
</div>


