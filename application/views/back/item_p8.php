<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
var datatable;
var itemcode_list = <?=json_encode($itemcode_list)?>;

function checkIsNumber(input, id){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);

    if(id == 1){
        if($(input).val().length == 1){
            $('#id_2').focus();
        }
    }else if(id == 2){
        if($(input).val().length == 2){
            $('#id_3').focus();
        }
    }else if(id == 3){
        if($(input).val().length == 2){
            $('#id_4').focus();
        }
    }else if(id == 4){
        if($(input).val().length == 2){
            $('#id_5').focus();
        }
    }
}

function pad(num, n) {
    var len = num.toString().length;
    while(len < n) {
        num = "0" + num;
        len++;
    }
    return num;
}

function checkItem(){
    var id_1 = $('#id_1').val();
    var id_2 = $('#id_2').val();
    var id_3 = $('#id_3').val();
    var id_4 = $('#id_4').val();
    var id_5 = $('#id_5').val();
    if(id_1 != '' && id_2 != '' && id_3 != '' && id_4 != '' && id_5 != ''){
        $.post('<?=base_url('item_p8/checkItemID')?>', {id_1:id_1,id_2:id_2,id_3:id_3,id_4:id_4,id_5:id_5}, function(result){
            $('#item_name').html(result);
        });
    }
}

$(function(){
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    $('.dateselector').datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        todayHighlight: true,
    }).on("show", function (e) {
        $("div.box").css({minHeight: "480px"});
    }).on("hide", function (e) {
        $("div.box").css({minHeight: "auto"});
    });

    $(document).on('click', '#selectall', function(){
        var rows = datatable.rows({ 'search': 'applied' }).nodes();
        $('input[name="selects[]"]', rows).prop('checked', this.checked);
    });

    $(document).on('change', '#item_code', function(){
        var no = $(this).val();
        if(no != ''){
            $('#id_1').val(itemcode_list[no]['code_item_class']);
            $('#id_2').val(itemcode_list[no]['code_item_project']);
            $('#id_3').val(itemcode_list[no]['code_item_contents']);
            $('#id_4').val(itemcode_list[no]['code_item_section']);
            $('#id_5').val(itemcode_list[no]['code_item_number']);
            $('#item_name').html(itemcode_list[no]['code_item_name']);
        }
    });

    $(document).on('submit', '#moveform', function(){
        var form = this;
        
        if(datatable != null){
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#moveform input[name="selects[]"]:checked').length + $('#moveform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
    });
    
    $(document).on('submit', '#searchform', function(){
        $('#datatable').DataTable().destroy();
        $.post("<?=base_url('item_p8/getitem')?>", $("#searchform").serialize(), function(result){
            result = JSON.parse(result);
            console.log(result);
            $('#itemlist').html('');
            var itemhtml = '';
            for(i in result){
                if(result[i]['item_type'] != null){
                    item_type = result[i]['item_type'].substring(20,0);
                }else{
                    item_type = '';
                }
                if(result[i]['item_brand'] != null){
                    item_brand = result[i]['item_brand'].substring(20,0);
                }else{
                    item_brand ='';
                }
                var price = parseInt(result[i]['item_price'])+parseInt(result[i]['item_price_change']);
                itemhtml += '<tr>';
                itemhtml += '<td><input type="checkbox" name="selects[]" value="'+result[i]['item_id']+'"></td>';
                itemhtml += '<td>'+result[i]['code_item_area_code']+'<br>'+result[i]['code_item_area_name']+'</td>';
                itemhtml += '<td>'+result[i]['item_buytime']+'<br>'+result[i]['item_gettime']+'</td>';
                itemhtml += '<td>'+result[i]['code_item_class']+'-'+result[i]['code_item_project']+'-'+result[i]['code_item_contents']+'-'+result[i]['code_item_section']+'-'+result[i]['code_item_number']+'-'+pad(result[i]['item_number'], 7)+'<br>'+result[i]['code_item_name'];
                itemhtml += '</td>';
                itemhtml += '<td>'+result[i]['item_desc']+'</td>';
                itemhtml += '<td>'+item_type+'/'+item_brand+'</td>';
                itemhtml += '<td>'+price+'</td>';
                itemhtml += '<td>'+result[i]['code_change_allcode']+'</td>';
                itemhtml += '<td>'+result[i]['item_grade']+'</td>';
                itemhtml += '<td>'+result[i]['item_document']+'</td>';
                itemhtml += '<td>'+result[i]['site_name']+'</td>';
                itemhtml += '<td>'+result[i]['usesite_name']+'<br>'+result[i]['keepsite_name']+'</td>';
                itemhtml += '<td>'+result[i]['usecust_name']+'<br>'+result[i]['keepcust_name']+'</td>';
                itemhtml += '</tr>';
            }
            $('#itemlist').html(itemhtml);
            datatable = $('#datatable').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
                'columnDefs': [{
                    'targets': [0],
                    'searchable': false,
                    'orderable': false
                }],
                "language": {
                    "url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
                }
            });
        });

        return false;
    });
});
</script>

<style>
[data-toggle="collapse"] .fa:before {   
  content: "\f146";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f0fe";
}
</style>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">物品基本資料修改<?=(isset($doc) ? ' 修改至('.$doc['doc_num_year'].'-基修-'.str_pad($number_now, 7, '0', STR_PAD_LEFT).')' : '')?></h3>
                    <div class="table-data__tool">
                        <div id="accordion" style="width:100%;">
                            <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer;">
                                    <h5 class="mb-0">
                                        <i class="fa"></i> 查詢條件
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <form id="searchform" name="searchform" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="card-body">
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">物品區分別</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="area" name="area">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($itemarea_list as $itemarea){
                                                            echo '<option value="'.$itemarea['code_item_area_id'].'">'.$itemarea['code_item_area_code'].' '.$itemarea['code_item_area_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">購置日期區間</label>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="date_start" name="date_start" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="date_end" name="date_end" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">物品編號</label>
                                                </div>
                                                <div class="col-12 col-md-5">
                                                    <div class="input-group">
                                                        <input type="text" id="id_1" name="id_1" class="form-control" maxlength="1" style="max-width:40px;min-width:40px;text-align:center;" onkeyup="checkIsNumber(this, 1);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_2" name="id_2" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 2);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_3" name="id_3" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 3);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_4" name="id_4" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 4);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_5" name="id_5" class="form-control" maxlength="4" style="max-width:70px;min-width:70px;text-align:center;" onkeyup="checkIsNumber(this, 5);checkItem();">　　
                                                        <label id="item_name" class="form-control-label"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">物品流水號</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" id="id_6" name="id_6" class="form-control" maxlength="7" style="max-width:100px;min-width:100px;text-align:center;" onkeyup="checkIsNumber(this)">&nbsp~&nbsp
                                                        <input type="text" id="id_7" name="id_7" class="form-control" maxlength="7" style="max-width:100px;min-width:100px;text-align:center;" onkeyup="checkIsNumber(this)">
                                                    </div>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">物品編號查詢</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="item_code" name="item_code">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($itemcode_list as $itemcode){
                                                            echo '<option value="'.$itemcode['code_item_id'].'">'.$itemcode['code_item_class'].'-'.$itemcode['code_item_project'].'-'.$itemcode['code_item_contents'].'-'.$itemcode['code_item_section'].'-'.$itemcode['code_item_number'].' '.$itemcode['code_item_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">存置地點</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="site" name="site">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">保管單位</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="keepsite" name="keepsite">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">使用單位</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="usesite" name="usesite">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">保管人</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="keepcust" name="keepcust">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($custcode_list as $custcode){
                                                            echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">使用人</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="usecust" name="usecust">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($custcode_list as $custcode){
                                                            echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        <div class="card-footer text-right">
                                            <input type="reset" class="btn btn-danger" value="重設">
                                            <input type="submit" class="btn btn-primary" value="查詢">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form id="moveform" name="moveform" method="post" enctype="multipart/form-data" class="form-horizontal" action="<?=base_url('item_p8/adddoc')?>">
                        <input type="hidden" name="doc_id" value="<?=isset($doc) ? $doc['doc_id'] : '0'?>">
                        <div class="table-responsive table-responsive-data2">
                            <table id="datatable" class="table table-striped table-bordered" style="width:100%" valign="center">
                                <thead class="thead-light">
                                    <tr>
                                        <th><input type="checkbox" id="selectall"></th>
                                        <th>物品區分別</th>
                                        <th>購置日期<br>取得日期</th>
                                        <th>物品編號<br>物品名稱</th>
                                        <th>物品備註</th>
                                        <th>形式/廠牌</th>
                                        <th>單價</th>
                                        <th>來源</th>
                                        <th>會計科目</th>
                                        <th>取得文號</th>
                                        <th>存置地點</th>
                                        <th>使用單位<br>保管單位</th>
                                        <th>使用人<br>保管人</th>
                                    </tr>
                                </thead>
                                <tbody id="itemlist">
                                    
                                </tbody>
                            </table>
                        </div>
                        <!-- END DATA TABLE -->
                        <div class="card" style="margin-top: 50px;">
                            <div class="card-header">
                                <h5 class="mb-0">
                                    批次更新
                                </h5>
                            </div>

                            
                            <div class="card-body">
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">別名</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="alias" name="alias" placeholder="不修改請留空">
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">形式</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="type" name="type" placeholder="不修改請留空">
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">廠牌</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="brand" name="brand" placeholder="不修改請留空">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">會計科目</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="grade" name="grade" placeholder="不修改請留空">
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">取得文號</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="document_number" name="document_number" placeholder="不修改請留空">
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">物品備註</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="desc" name="desc" placeholder="不修改請留空">
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">來源</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="change" name="change">
                                            <option value="">不修改</option>
                                            <?php
                                            foreach($changecode_list as $changecode){
                                                echo '<option value="'.$changecode['code_change_id'].'">'.$changecode['code_change_allcode'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">存置地點</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="site" name="site">
                                            <option value="">不修改</option>
                                            <?php
                                            foreach($sitecode_list as $sitecode){
                                                echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <!-- <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">年限</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="deadline" name="deadline" oninput="value=value.replace(/[^\d]/g,'')">
                                    </div> -->
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">保管單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="keep_site" name="keep_site">
                                            <option value="">不修改</option>
                                            <?php
                                            foreach($sitecode_list as $sitecode){
                                                echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">使用單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="use_site" name="use_site">
                                            <option value="">不修改</option>
                                            <?php
                                            foreach($sitecode_list as $sitecode){
                                                echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <!-- <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" id="unit" name="unit">
                                    </div> -->
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">保管人</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="keep_cust" name="keep_cust">
                                            <option value="">不修改</option>
                                            <?php
                                            foreach($custcode_list as $custcode){
                                                echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">使用人</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control select2" id="use_cust" name="use_cust">
                                            <option value="">不修改</option>
                                            <?php
                                            foreach($custcode_list as $custcode){
                                                echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <hr>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">填單日期</label>
                                    </div>
                                    <div class="col-12 col-md-2 input-group">
                                        <input type="text" name="write_date" class="form-control dateselector" value="<?=isset($doc) ? $doc['doc_write_date'] : (date('Y')-1911).date('-m-d')?>" required>
                                        <div class="input-group-addon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">填造單位</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" value="<?=$this->session->userdata('unit')?>" readonly>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">傳票編號</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" name="document_type" class="form-control" value="<?=isset($doc) ? $doc['doc_type'] : ''?>" required>
                                    </div>
                                </div>
                                <div class="row form-group">
                                    <div class="col col-md-1">
                                        <label class="form-control-label">帳務日期</label>
                                    </div>
                                    <div class="col-12 col-md-2 input-group">
                                        <input type="text" name="accounts_date" class="form-control dateselector" value="<?=isset($doc) ? $doc['doc_accounts_date'] : (date('Y')-1911).date('-m-d')?>" required>
                                        <div class="input-group-addon">
                                            <i class="far fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">單據字號</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <input type="text" class="form-control" value="<?=date('Y')-1911?>-基修-<?=str_pad($number_now, 7, '0', STR_PAD_LEFT)?>" readonly>
                                    </div>
                                    <div class="col col-md-1 offset-md-1">
                                        <label class="form-control-label">物品區分別</label>
                                    </div>
                                    <div class="col-12 col-md-2">
                                        <select class="form-control" id="area" name="area" required>
                                            <?php
                                            foreach($itemarea_list as $itemarea){
                                                echo '<option value="'.$itemarea['code_item_area_id'].'">'.$itemarea['code_item_area_code'].' '.$itemarea['code_item_area_name'].'</option>';
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-right">
                                <input type="submit" class="btn btn-primary" value="確定修改">
                            </div>
                        
                        </div>
                    </form>
                </div>
                
            </div>


