<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<script>
var datatable;
var itemcode_list = <?=json_encode($itemcode_list)?>;

function checkIsNumber(input, id){
    var str = $(input).val();
    var result = '';
    for(var i = 0 ; i < str.length ; i++){
        var code = str.charCodeAt(i);
        if(code >= 65296 && code <= 65305){
            result += String.fromCharCode(code - 65248);
        }
        if(code >= 48 && code <= 57){
            result += String.fromCharCode(code);
        }
    }
    $(input).val(result);

    if(id == 1){
        if($(input).val().length == 1){
            $('#id_2').focus();
        }
    }else if(id == 2){
        if($(input).val().length == 2){
            $('#id_3').focus();
        }
    }else if(id == 3){
        if($(input).val().length == 2){
            $('#id_4').focus();
        }
    }else if(id == 4){
        if($(input).val().length == 2){
            $('#id_5').focus();
        }
    }
}

function pad(num, n) {
    var len = num.toString().length;
    while(len < n) {
        num = "0" + num;
        len++;
    }
    return num;
}

function checkItem(){
    var id_1 = $('#id_1').val();
    var id_2 = $('#id_2').val();
    var id_3 = $('#id_3').val();
    var id_4 = $('#id_4').val();
    var id_5 = $('#id_5').val();
    if(id_1 != '' && id_2 != '' && id_3 != '' && id_4 != '' && id_5 != ''){
        $.post('<?=base_url('item_p1/checkItemID')?>', {id_1:id_1,id_2:id_2,id_3:id_3,id_4:id_4,id_5:id_5}, function(result){
            $('#item_name').html(result);
        });
    }
}

$(function(){
    $('.select2').select2({
        theme: 'bootstrap4',
    });

    //$('#datatable').DataTable();

    $('.dateselector').datepicker({
        format: "twy-mm-dd",
        language: "zh-TW",
        todayHighlight: true,
    }).on("show", function (e) {
        $("div.box").css({minHeight: "480px"});
    }).on("hide", function (e) {
        $("div.box").css({minHeight: "auto"});
    });

    $(document).on('click', '#selectall', function(){
        var rows = datatable.rows({ 'search': 'applied' }).nodes();
        $('input[name="selects[]"]', rows).prop('checked', this.checked);
    });

    $(document).on('change', '#item_code', function(){
        var no = $(this).val();
        if(no != ''){
            $('#id_1').val(itemcode_list[no]['code_item_class']);
            $('#id_2').val(itemcode_list[no]['code_item_project']);
            $('#id_3').val(itemcode_list[no]['code_item_contents']);
            $('#id_4').val(itemcode_list[no]['code_item_section']);
            $('#id_5').val(itemcode_list[no]['code_item_number']);
            $('#item_name').html(itemcode_list[no]['code_item_name']);
        }
    });

    $(document).on('submit', '#searchform', function(){
        $('#datatable').DataTable().destroy();
        $.post("<?=base_url('item_p1/getitem')?>", $("#searchform").serialize(), function(result){
            result = JSON.parse(result);
            console.log(result);
            $('#itemlist').html('');
            var itemhtml = '';
            for(i in result){
                if(result[i]['item_type'] != null){
                    item_type = result[i]['item_type'].substring(20,0);
                }else{
                    item_type = '';
                }
                if(result[i]['item_brand'] != null){
                    item_brand = result[i]['item_brand'].substring(20,0);
                }else{
                    item_brand ='';
                }
                var price = parseInt(result[i]['item_price'])+parseInt(result[i]['item_price_change']);
                itemhtml += '<tr>';
                itemhtml += '<td><input type="checkbox" name="selects[]" id="selects[]" value="'+result[i]['item_id']+'"></td>';
                itemhtml += '<td>'+result[i]['code_item_area_code']+' '+result[i]['code_item_area_name']+'</td>';
                itemhtml += '<td>'+result[i]['item_buytime']+'<br>'+result[i]['item_gettime']+'</td>';
                itemhtml += '<td>'+result[i]['code_item_class']+'-'+result[i]['code_item_project']+'-'+result[i]['code_item_contents']+'-'+result[i]['code_item_section']+'-'+result[i]['code_item_number']+'-'+pad(result[i]['item_number'], 7)+'<br>'+result[i]['code_item_name']+'</td>';
                itemhtml += '<td>'+item_type+'/'+item_brand+'</td>';
                itemhtml += '<td>'+price+'</td>';
                itemhtml += '<td>'+result[i]['code_item_deadline']+'</td>';
                itemhtml += '<td>'+result[i]['site_name']+'</td>';
                itemhtml += '<td>'+result[i]['usesite_name']+'<br>'+result[i]['keepsite_name']+'</td>';
                itemhtml += '<td>'+result[i]['usecust_name']+'<br>'+result[i]['keepcust_name']+'</td>';
                itemhtml += '</tr>';
            }
            $('#itemlist').html(itemhtml);
            datatable = $('#datatable').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
                'columnDefs': [{
                    'targets': [0],
                    'searchable': false,
                    'orderable': false,
                }],
                "language": {
                    "url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
                }
            });
            $('.item').tooltip('update');
        });

        return false;
    });
    // 匯出excel
    $(document).on('click', '.excelbtn', function(){
        var form = $('#checkform');
        
        if(datatable != null){
            $('#checkform input[type="hidden"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#checkform input[name="selects[]"]:checked').length + $('#checkform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
        $('#checkform').attr('action','<?=base_url('item_p1/export')?>');
        $('#checkform').submit();
    });
    // 匯出pdf
    $(document).on('click', '.printbtn', function(){
        var form = $('#checkform');
        
        if(datatable != null){
            $('#checkform input[type="hidden"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#checkform input[name="selects[]"]:checked').length + $('#checkform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
        // else if($('#checkform input[name="selects[]"]:checked').length + $('#checkform input[name="selects[]"][type="hidden"]').length > 500){
        //     alert('無法匯出，單次匯出上限為500筆');
        //     return false;
        // }
        
        $('#checkform').attr('action','<?=base_url('item_p1/export_pdf')?>');
        $('#checkform').submit();
        // var url = "<?=base_url('item_p1/export_pdf?')?>"+form.serialize();
        // window.open(url, '匯出', config='height=600,width=800');
        // location.reload();
    });
    // 列印標籤
    
    $(document).on('click', '.printtagbtn', function(){
        var form = $('#checkform');
        
        if(datatable != null){
            $('#checkform input[type="hidden"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#checkform input[name="selects[]"]:checked').length + $('#checkform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
        
        $('#checkform').attr('action','<?=base_url('item_p1/print_tag')?>');
        $('#checkform').submit();
        // var url = "<?=base_url('item_p1/print_tag?')?>"+form.serialize();
        // window.open(url, '匯出', config='height=600,width=800');
        // location.reload();
    });


    // 列印物品卡printcardbtn
    $(document).on('click', '.printcardbtn', function(){
        var form = $('#checkform');
        
        if(datatable != null){
            $('#checkform input[type="hidden"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#checkform input[name="selects[]"]:checked').length + $('#checkform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
        
        $('#checkform').attr('action','<?=base_url('item_p1/print_item_card')?>');
        $('#checkform').submit();
        return false;
    });

    
    // json
    $(document).on('click','.export_json',function(){
        var form = $('#checkform');
        
        if(datatable != null){
            $('#checkform input[type="hidden"]').each(function(){
                this.remove();
            });
            datatable.$('input[type="checkbox"]').each(function(){
                if(!$.contains(document, this)){
                    if(this.checked){
                        $(form).append(
                            $('<input>')
                            .attr('type', 'hidden')
                            .attr('name', this.name)
                            .val(this.value)
                        );
                    }
                }
            });
        }
        if($('#checkform input[name="selects[]"]:checked').length + $('#checkform input[name="selects[]"][type="hidden"]').length <= 0){
            alert('請至少選擇一項');
            return false;
        }
        
        $('#checkform').attr('action','<?=base_url('inventory/export_json')?>');
        $('#checkform').submit();
        return false;
    });

});

    
</script>

<style>
[data-toggle="collapse"] .fa:before {   
  content: "\f146";
}

[data-toggle="collapse"].collapsed .fa:before {
  content: "\f0fe";
}
</style>
<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!-- DATA TABLE -->
                    <h3 class="title-5 m-b-35">物品列表(含物品基本資料修改、匯出主檔excel、印物品清冊、印物品卡、印物品標籤)</h3>
                    <div class="table-data__tool">
                        <div id="accordion" style="width:100%;">
                            <div class="card">
                                <div class="card-header" id="headingOne" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="cursor: pointer;">
                                    <h5 class="mb-0">
                                        <i class="fa"></i> 查詢條件
                                    </h5>
                                </div>

                                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <form id="searchform" name="searchform" method="post" enctype="multipart/form-data" class="form-horizontal">
                                        <div class="card-body">
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">物品區分別</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="area" name="area">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($itemarea_list as $itemarea){
                                                            echo '<option value="'.$itemarea['code_item_area_id'].'">'.$itemarea['code_item_area_code'].' '.$itemarea['code_item_area_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">購置日期區間</label>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="date_start" name="date_start" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-2 input-group">
                                                    <input type="text" id="date_end" name="date_end" class="form-control dateselector">
                                                    <div class="input-group-addon">
                                                        <i class="far fa-calendar"></i>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">物品編號</label>
                                                </div>
                                                <div class="col-12 col-md-5">
                                                    <div class="input-group">
                                                        <input type="text" id="id_1" name="id_1" class="form-control" maxlength="1" style="max-width:40px;min-width:40px;text-align:center;" onkeyup="checkIsNumber(this, 1);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_2" name="id_2" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 2);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_3" name="id_3" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 3);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_4" name="id_4" class="form-control" maxlength="2" style="max-width:50px;min-width:50px;text-align:center;" onkeyup="checkIsNumber(this, 4);checkItem();">&nbsp-&nbsp
                                                        <input type="text" id="id_5" name="id_5" class="form-control" maxlength="4" style="max-width:70px;min-width:70px;text-align:center;" onkeyup="checkIsNumber(this, 5);checkItem();">　　
                                                        <label id="item_name" class="form-control-label"></label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">物品流水號</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <div class="input-group">
                                                        <input type="text" id="id_6" name="id_6" class="form-control" maxlength="7" style="max-width:100px;min-width:100px;text-align:center;" onkeyup="checkIsNumber(this)">&nbsp~&nbsp
                                                        <input type="text" id="id_7" name="id_7" class="form-control" maxlength="7" style="max-width:100px;min-width:100px;text-align:center;" onkeyup="checkIsNumber(this)">
                                                    </div>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">物品編號查詢</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="item_code" name="item_code">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($itemcode_list as $itemcode){
                                                            echo '<option value="'.$itemcode['code_item_id'].'">'.$itemcode['code_item_class'].'-'.$itemcode['code_item_project'].'-'.$itemcode['code_item_contents'].'-'.$itemcode['code_item_section'].'-'.$itemcode['code_item_number'].' '.$itemcode['code_item_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">存置地點</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="site" name="site">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">保管單位</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="keepsite" name="keepsite">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">使用單位</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="usesite" name="usesite">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($sitecode_list as $sitecode){
                                                            echo '<option value="'.$sitecode['code_site_id'].'">'.$sitecode['code_site_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row form-group">
                                                <div class="col col-md-2">
                                                    <label class="form-control-label">保管人</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="keepcust" name="keepcust">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($custcode_list as $custcode){
                                                            echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col col-md-2 offset-md-2">
                                                    <label class="form-control-label">使用人</label>
                                                </div>
                                                <div class="col-12 col-md-2">
                                                    <select class="form-control select2" id="usecust" name="usecust">
                                                        <option value="">　</option>
                                                        <?php
                                                        foreach($custcode_list as $custcode){
                                                            echo '<option value="'.$custcode['code_cust_id'].'">'.$custcode['code_cust_name'].'</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        
                                        </div>
                                        <div class="card-footer text-right">
                                            <input type="reset" class="btn btn-danger" value="重設">
                                            <input type="submit" class="btn btn-primary" value="查詢">
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!-- <div class="row">
                            <div class="col-md-5 offset-md-7 col-sm-6 ml-auto">
                                    <button type="button" class="btn btn-primary excelbtn"><i class="fa fa-upload"></i>&nbsp; 匯出excel</button>
                                    <button type="button" class="btn btn-success printbtn"><i class="fa fa-print"></i>&nbsp; 列印物品清冊</button>
                                    <button type="button" class="btn btn-success printcardbtn"><i class="fa fa-print"></i>&nbsp; 列印物品卡</button>
                                    <button type="button" class="btn btn-secondary printtagbtn"><i class="fa fa-print"></i>&nbsp; 列印物品標籤</button>
                            </div>
                        </div>
                        <br> -->
                    <form id="checkform" name="checkform" method="post">
                        <div class="table-responsive table-responsive-data2">
                            <table id="datatable" class="table table-striped table-bordered" style="width:100%" valign="center">
                                <thead class="thead-light">
                                    <tr>
                                        <th><input type="checkbox" id="selectall"></th>
                                        <th>物品區分別</th>
                                        <th>購置日期<br>取得日期</th>
                                        <th>物品編號<br>物品名稱</th>
                                        <th>形式/廠牌</th>
                                        <th>帳面金額</th>
                                        <th>使用年限</th>
                                        <th>存置地點</th>
                                        <th>使用單位<br>保管單位</th>
                                        <th>使用人<br>保管人</th>
                                    </tr>
                                </thead>
                                
                                <tbody id="itemlist">
                                    
                                </tbody>
                                    <?php if($this->session->userdata('group') == 'S' || ($this->session->userdata('organ_inventory') == 'Y' && $this->authority_array['item_option3'] == 'Y')):?>
                                        <button type="button" class="btn btn-primary export_json"><i class="fa fa-upload"></i>&nbsp; 匯出盤點資料</button>&nbsp;
                                    <?php endif;?>
                                    <?php if($this->authority_array['item_option3'] == 'Y'):?>
                                        <button type="button" class="btn btn-primary excelbtn"><i class="fa fa-upload"></i>&nbsp; 匯出excel</button>&nbsp;
                                        <button type="button" class="btn btn-success printbtn"><i class="fa fa-print"></i>&nbsp; 列印物品清冊</button>&nbsp;
                                        <button type="button" class="btn btn-success printcardbtn"><i class="fa fa-print"></i>&nbsp; 列印物品卡</button>&nbsp;
                                        <button type="button" class="btn btn-secondary printtagbtn"><i class="fa fa-print"></i>&nbsp; 列印物品標籤</button><br>
                                    <?php endif;?>
                            </table>
                        </div>
                    </form>
                    <!-- END DATA TABLE -->
                </div>
                
            </div>


