<?php
defined('BASEPATH') OR exit('No direct script access allowed');header("Content-Security-Policy: upgrade-insecure-requests");
?>

<script>
        $(document).ready(function() {
            $('.organ_list').select2({
                theme: "bootstrap4"
            });
            $('.member_group').select2({
                theme: "bootstrap4"
            });
            $('.member_unit').select2({
                theme:"bootstrap4"
            });
        });
        
        $(document).ready( function () {
            $('#list_tables').DataTable({
                'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
                });
        } );

        $(document).change('.organ_list',function(){
            organ_id = $('.organ_list').val();
            $.post("<?=base_url('member_p1/getid')?>",{organ_id:organ_id},function(json){
                console.log(json);
                if(json != null){
                    $('#memberModal #organ_name').val(json['organ_name']);
                }else{
                    $('#memberModal #organ_name').val('');
                }
                
            },'json');
        });

        
        // Delbtn
        $(document).on('click','.delbtn',function(){
            var member_id = $(this).data('no');
            Swal.fire({
                title: "確定要刪除這筆資料嗎?",
                text: "一經刪除後將不可復原!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: '是，我要刪除!!',
                cancelButtonText: '取消'
            })
            .then((result) =>{
                if (result.isConfirmed) {
                    $.post("<?=base_url('member_p2/del')?>",{member_id:member_id},function(json){
                        if(json == "true"){
                            // success
                            Swal.fire({title: "刪除成功",icon: "success"}).then(function(value){
                                window.location.reload();;
                            });
                        }else{
                            // error
                            Swal.fire({title: "刪除時發生錯誤",icon: "warning"});
                            return false;
                        }
                    });
                }
            });
        });

        $(document).on('click','.addbtn',function(){
            
            $('#editnotify #largeModalLabel').text('新增機關帳號');
            $('#editnotify #type').val('add');
            $('#editnotify #member_id').val('');
            $('#editnotify #organ_list').val('');
            $('#editnotify #organ_list').trigger('change');
            $('#editnotify #organ_name').val('');
            $('#editnotify #member_unit').val('');
            $('#editnotify #member_unit').trigger('change');
            $('#editnotify #member_name').val('');
            $('#editnotify #member_acc').val('');
            $('#editnotify #member_pwd').val('');
            $('#editnotify #member_group').val('');
            $('#editnotify #member_group').trigger('change');
        });


        // editbtn show modal
        $(document).on('click','.editbtn',function(){
            member_id =$(this).data('no');
            $.post('<?=base_url('Member_p2/getid')?>',{member_id:member_id},function(json){
                console.log(json);
                $('#editnotify #largeModalLabel').text('修改機關帳號')
                $('#editnotify #type').val('edit');
                $('#editnotify #member_id').val(json['member_id']);
                $('#editnotify #organ_list').val(json['organ_id']);
                $('#organ_list').trigger('change');
                $('#editnotify #organ_name').val(json['organ_name']);
                $('#editnotify #member_unit').val(json['member_unit']);
                $('#editnotify #member_unit').trigger('change');
                $('#editnotify #member_name').val(json['member_name']);
                $('#editnotify #member_acc').val(json['member_acc']);
                // $('#editnotify #member_pwd').val(json['member_pwd']);
                $('#editnotify #member_group').val(json['member_group']);
                $('#editnotify #member_group').trigger('change');
            },'json');
            // $('#editnotify #notify').val(allnotify[$(this).data('no')]['M01I02NV0040']);
            // $('#editnotify #timestart').val(allnotify[$(this).data('no')]['M01N01DD0003']);
            // $('#editnotify #timestop').val(allnotify[$(this).data('no')]['M01N02DD0003']);
            $('#memberModal').modal('show');
        });


        // Model submit
        $(document).on('submit','#editnotify',function(){
            $.ajax({
                url: "<?=base_url('member_p2/send')?>",
                data: $('#editnotify').serialize(),
                type:"POST",
                error:function(xhr, ajaxOptions, thrownError){ 
                    console.log(thrownError)
                    alert(xhr.responseText);
                },
                success:function(json){
                    // show message
                    console.log(json);

                    if(json == "true"){
                        // success
                        Swal.fire({title: "成功",icon: "success"}).then(function(value){
                            window.location.reload();
                        });
                    }else{
                        // error
                        Swal.fire({title: "發生錯誤",icon: "error"});
                        return false;
                    }
                }
            });
            return false;
        });
    </script>
<!-- MAIN CONTENT-->
<div class="main-content">
<!-- Model -->
    <form class="form-signin" id="editnotify" method="post" role="form">
        <input type="hidden" name="type" value="add" id="type">
        <input type="hidden" name="member_id" id="member_id">
        <div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="largeModalLabel" aria-hidden="true"  data-backdrop="static">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="largeModalLabel">新增機關帳號</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="organ_list" class=" form-control-label">機關代碼：</label>
                            </div>
                            <div class="col-8">
                                <select class="organ_list" name="organ_list" id="organ_list">
                                    <option value="">選擇機關名稱</option>
                                    <?php foreach ($organ_list as $key => $organ) :?>
                                        <option value="<?=$organ['organ_id']?>"><?=$organ['organ_code']?> / <?=$organ['organ_name']?></option>
                                    <?php endforeach;?>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="organ_name" class=" form-control-label">機關名稱：</label>
                            </div>
                            <div class="col-8">
                                <input type="text" name="organ_name" id="organ_name" class="form-control" placeholder="請輸入機關名稱" maxlength="60" readonly="readonly">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="member_unit" class=" form-control-label">填造單位：</label>
                            </div>
                            <div class="col-8">
                                <select class="member_unit" name="member_unit" id="member_unit">
                                    <option value="">選擇填造單位</option>
                                    <?php foreach ($code4_list as $key => $code4) :?>
                                        <option value="<?=$code4['code_write_id']?>"><?=$code4['organ_name'].' - '.$code4['code_write_allcode']?></option>
                                    <?php endforeach;?>
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="member_name" class=" form-control-label">帳號使用人名稱：</label>
                            </div>
                            <div class="col-8">
                                <input type="text" name="member_name" id="member_name" class="form-control" placeholder="請輸入帳號使用人名稱" maxlength="8">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="member_acc" class=" form-control-label">帳號：</label>
                            </div>
                            <div class="col-8">
                                <input type="text" name="member_acc" id="member_acc" class="form-control" placeholder="請輸入帳號" maxlength="20" onkeyup="value=value.replace(/[\W]/g,'')">
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="member_pwd" class=" form-control-label">密碼：</label>
                            </div>
                            <div class="col-8">
                                <input type="password" name="member_pwd" id="member_pwd" class="form-control" placeholder="請輸入密碼" maxlength="20" onkeyup="value=value.replace(/[\W]/g,'')">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-3">
                                <label for="member_group" class=" form-control-label">權限群組：</label>
                            </div>
                            <div class="col-8">
                                <select class="member_group form-control" name="member_group" id="member_group">
                                    <option value="">請選擇群組名稱</option>
                                    <?php foreach ($authority_list as $Akey => $Avalue) {
                                        echo "<option value='".$Avalue['authority_code']."'>".$Avalue['authority_name']."</option>";
                                    }?>
                                    <!-- <option value="A">機關管理人</option>
                                    <option value="C">機關使用者</option> -->
                                </select>
                                <div class="dropDownSelect2"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">關閉</button>
                        <button type="submit" class="btn btn-primary">儲存</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
<!-- end modal large -->
        <div class="section__content section__content--p30">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        
                        <style type="text/css">
                            table {
                                    table-layout:fixed;word-break:break-all;
                            }
                        </style>
                        <!-- DATA TABLE -->
                        <h3 class="title-5 m-b-35">機關帳號管理</h3>
                        <div class="table-data__tool">
                            <div class="table-data__tool-left">
                            </div>
                            <div class="table-data__tool-right">
                                <!-- <button class="au-btn au-btn-icon au-btn--green au-btn--small" data-toggle="modal" data-target="#memberModal">
                                    <i class="zmdi zmdi-upload"></i>匯入機關資料</button> -->
                                <button class="au-btn au-btn-icon au-btn--green au-btn--small addbtn" data-toggle="modal" data-target="#memberModal">
                                    <i class="zmdi zmdi-plus"></i>新增機關帳號</button>
                            </div>
                        </div>
                        <!-- <div class="table-responsive table-responsive-data2"> -->
                            <table id="list_tables" class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th>機關代碼</th>
                                        <th>機關名稱</th>
                                        <th>填造單位</th>
                                        <th>帳號使用人名稱</th>
                                        <th>帳號</th>
                                        <th>權限群組</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($member_list as $key => $value) :?>
                                        <tr class="tr-shadow">
                                            <td><?=$value['organ_code']?></td>
                                            <td><?=$value['organ_name']?></td>
                                            <td><?=$value['code_write_allcode']?></td>
                                            <td><?=$value['member_name']?></td>
                                            <td><?=$value['member_acc']?></td>
                                            <td><?=$value['member_group'] =='A' ? '機關管理人':'機關使用人'?></td>
                                            <td>
                                                <div class="table-data-feature">
                                                    <button class="item editbtn" data-toggle="tooltip" data-placement="top" title="Edit" data-no="<?=$value['member_id']?>">
                                                        <i class="zmdi zmdi-edit"></i>
                                                    </button>
                                                    <button class="item delbtn" data-toggle="tooltip" data-placement="top" title="Delete" data-no="<?=$value['member_id']?>">
                                                        <i class="zmdi zmdi-delete"></i>
                                                    </button>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach;?>
                                </tbody>
                            </table>
                        <!-- </div> -->
                        <!-- END DATA TABLE -->
                    </div>
                    
                </div>


