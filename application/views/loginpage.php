<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>登入</title>
    <!-- Fontfaces CSS-->
    <link href="<?=base_url();?>css/font-face.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstr<?=base_url();?>ap CSS-->
    <link href="<?=base_url();?>vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor <?=base_url();?>CSS-->
    <link href="<?=base_url();?>vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/select2/select2-bootstrap4.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" media="all">
    <link href="<?=base_url();?>vendor/datatables/datatables.min.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="<?=base_url();?>css/theme.css" rel="stylesheet" media="all">
    
    
    <!-- Jquery JS-->
    <script src="<?=base_url();?>vendor/jquery-3.2.1.min.js"></script>

    
</head>

<body class="animsition">
    <nav class="navbar navbar-expand-lg navbar-light bg-light shadow">
        <div class="container">
            <a class="navbar-brand" href="/">
                <img src="<?=base_url()?>images/logo.png" height="40">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="true" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </nav>
    <div class="page-wrapper">
        <div class="page-content--bge5">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-form">
                            <form action="<?=base_url().'index/login'?>" method="post">
                                <div class="form-group">
                                    <label>帳號</label>
                                    <input class="au-input au-input--full" type="text" name="user_account" placeholder="Account">
                                </div>
                                <div class="form-group">
                                    <label>密碼</label>
                                    <input class="au-input au-input--full" type="password" name="user_pwd" placeholder="Password">
                                </div>
                                <div class="login-checkbox">
                                    <label>
                                        <input type="checkbox" name="remember">記住我
                                    </label>
                                    <!-- <label>
                                        <a href="#">Forgotten Password?</a>
                                    </label> -->
                                </div>
                                <button class="au-btn au-btn--block au-btn--blue2 m-b-20" type="submit">登入</button>
                                
                            </form>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Bootstrap JS-->
    <script src="<?=base_url();?>vendor/bootstrap-4.1/popper.min.js"></script>
    <script src="<?=base_url();?>vendor/bootstrap-4.1/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?=base_url();?>vendor/slick/slick.min.js">
    </script>
    <script src="<?=base_url();?>vendor/wow/wow.min.js"></script>
    <script src="<?=base_url();?>vendor/animsition/animsition.min.js"></script>
    <script src="<?=base_url();?>vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?=base_url();?>vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?=base_url();?>vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?=base_url();?>vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?=base_url();?>vendor/perfect-scrollbar/perfect-scrollbar.js"></script>
    <script src="<?=base_url();?>vendor/chartjs/Chart.bundle.min.js"></script>
    <script src="<?=base_url();?>vendor/select2/select2.min.js"></script>
    <script src="<?=base_url();?>vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="<?=base_url();?>vendor/bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js"></script>
    <script src="<?=base_url();?>vendor/datatables/datatables.min.js"></script>

    <script src="<?=base_url();?>vendor/sweetalert/sweetalert2.all.min.js"></script>
    <!-- Main JS-->
    <script src="<?=base_url();?>js/main.js"></script>
    
</body>
</html>