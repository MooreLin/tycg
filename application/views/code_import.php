<script>

var type = /(.csv|.xls|.xlsx)$/i;

$(function(){
	$('#prgressdiv').hide();

	$(document).on('submit','#import',function(){
		
		if($('#import #file').val() == ""){
			Swal.fire({title: "請選擇要上傳的檔案",icon: "warning"});
			return false;
		}
		if($('#import #filetype').val() == ""){
			Swal.fire({title: "請選擇要上傳的檔案類型",icon: "warning"});
			return false;
		}
		if(!type.test($("#import #file").val())){ 
			Swal.fire({title: "只允許上傳副檔名為csv,xls,xlsx的檔案",icon: "warning"});	
			return false;
		}
		// $("#progressbar").html('0%');
		// $("#progressbar").width('0%');
		// $("#progressbar").attr('aria-valuenow', 0);
		$('#prgressdiv').show();
		var first = true;
		var total;
		$.ajax({
			url: "<?=base_url('index/import')?>",
			data: new FormData(this),
			type:"POST",
			cache:false,
			processData: false,
			contentType: false,
			error:function(xhr, ajaxOptions, thrownError){ 
				console.log(thrownError)
				alert(xhr.responseText);
			},
			success:function(json){
				// show message
				console.log(json);
				$('#importlog').val(json);
			},
			xhr:function(){
				var xhr = $.ajaxSettings.xhr(); // 建立xhr(XMLHttpRequest)物件
				/*xhr.upload.addEventListener("progress", function(progressEvent){ // 監聽ProgressEvent
					if (progressEvent.lengthComputable) {
						var percentComplete = progressEvent.loaded / progressEvent.total;
						var percentVal = Math.round(percentComplete*100) + "%";
						$("#progressbar").html(percentVal); // 進度條百分比文字
						$("#progressbar").width(percentVal);    // 進度條顏色
						$("#progressbar").attr('aria-valuenow', Math.round(percentComplete*100));
					}
				}, false);*/
				xhr.onprogress = function(e){
					//console.log(e.currentTarget.responseText.substr(responseLen));
					if(first){
						total = e.currentTarget.responseText.replace('資料總筆數', '').replace(/\r\n|\n|\r/g, '');
						total = parseInt(total)+1;
						console.log(total);
						first = false;
					}
					var psconsole = $('#importlog');
					psconsole.val(e.currentTarget.responseText);
					if(psconsole.length){
						psconsole.scrollTop(psconsole[0].scrollHeight - psconsole.height());
					}
					// var strlist = e.currentTarget.responseText.match(/第\d+行/g);
					// var count = parseInt(strlist[strlist.length-1].replace('第', '').replace('行', ''));
					// var percentVal = Math.round((count/total)*100) + "%";
					//console.log(count);
					// $("#progressbar").html(percentVal);
					// $("#progressbar").width(percentVal);
					// $("#progressbar").attr('aria-valuenow', Math.round((count/total)*100));
				};
				return xhr; // 注意必須將xhr(XMLHttpRequest)物件回傳
			}
		});
		return false;
	});

});

</script>


<div class="main-content">
<!-- end modal large -->
	<div class="section__content section__content--p30">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<h3 class="title-5 m-b-35">資料匯入</h3>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 offset-md-4">
					<form id="import" name="import">
						<div class="form-group row">
							<label for="file" class="col-sm-3 col-form-label">匯入檔案</label>
							<div class="col-sm-9">
								<input type="file" class="form-control-file" id="file" name="file" accept=".csv,.xls,.xlsx">
							</div>
						</div>
						<div class="form-group row">
							<label for="filetype" class="col-sm-3 col-form-label">檔案類型</label>
							<div class="col-sm-9">
								<select class="form-control" id="filetype" name="filetype">
									<option value="">請選擇檔案類型</option>
									<option value="p1">道路</option>
									<option value="p2">橋梁</option>
									<option value="p3">路燈</option>
									<option value="p4">路樹</option>
									<option value="p5">人行道</option>
									<option value="p6">邊溝</option>
									<option value="p7">天空纜線</option>
								</select>
							</div>
						</div>
						<button type="submit" class="btn btn-primary btn-block">上傳檔案</button>
					</form>
				</div>
			</div>
			<div id="prgressdiv">
				<hr>
				<div class="row">
					<div class="col-md-6 offset-md-3">
						<div class="progress">
							<div class="progress-bar progress-bar-striped progress-bar-animated" id="progressbar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">0%</div>
						</div>
						<textarea class="form-control" rows="10" id="importlog" name="importlog" readonly></textarea>
					</div>
				</div>
			</div>