	<script>

		var budget_list = <?=json_encode($detailed_msg)?>;
		var alljsonlist = new Array();
		var se_type ='';
		$(function () {
			// $('#user_tables').DataTable({});
			
		});
		
		$(document).ready(function(){
			$('#renovate_class').select2({
				theme: "bootstrap4",
			});
		});

		$(document).on('click','.searchform_submit',function(){
       		$('#user_tables').DataTable().destroy();
			if($('#money_type').val() == '0'){
				Swal.fire('請選擇項目種類','','error');
				return false;
			}
			
			se_type = $('#searchform #money_type').find('option:selected').text();
			var limit_num = $('#searchform #limit_num').val();
			$.ajax({
				url: "<?=base_url('index/getsearch')?>",
				data: $('#searchform').serialize(),
				type:"POST",
				error:function(xhr, ajaxOptions, thrownError){ 
					console.log(thrownError)
					alert(xhr.responseText);
				},
				success:function(json){
					// show message
					
					json = JSON.parse(json);
					
					if($('#searchform #money_type').val() == '1'){
						// var htmlitem = '';
						// for(i in json){
						// 	alljsonlist[json[i]['road_id']] = json[i];
						// 	htmlitem += "<tr>";
						// 	if((limit_num-1) < i){
						// 		htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['road_id']+"'></td>";
						// 	}else{
						// 		htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['road_id']+"' checked></td>";
						// 	}
							
						// 	htmlitem += "<td>"+se_type+"</td>";
						// 	htmlitem += "<td>"+json[i]['name']+"</td>";
						// 	htmlitem += "<td>"+json[i]['bridge_town']+"</td>";
						// 	htmlitem += "<td>"+json[i]['bridge_score']+"</td>";
						// 	htmlitem += "<td>"+json[i]['bridge_repair_cost']+"</td>";
						// 	htmlitem += "</tr>";
						// }
						// $('.databody').html(htmlitem);
						// datatable = $('#user_tables').DataTable({
						// 	'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
						// 	'columnDefs': [{
						// 		'targets': [0],
						// 		'searchable': false,
						// 		'orderable': false
						// 	}],
						// 	"language": {
						// 		"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
						// 	}
						// });
						// checknum_money('bridge');
						// $('#selectall').val('bridge');
					}
					// 橋梁
					if($('#searchform #money_type').val() == '2'){
						var htmlitem = '';
						for(i in json){
							alljsonlist[json[i]['bridge_id']] = json[i];
							htmlitem += "<tr>";
							if((limit_num-1) < i){
								if(budget_list['bridge'][json[i]['bridge_id']] == json[i]['bridge_id']){
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['bridge_id']+"' checked></td>";	
								}else{
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['bridge_id']+"'></td>";
								}
								
							}else{
								htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['bridge_id']+"' checked></td>";
							}
							
							htmlitem += "<td>"+se_type+"</td>";
							htmlitem += "<td>"+json[i]['name']+"</td>";
							htmlitem += "<td>"+json[i]['bridge_town']+"</td>";
							htmlitem += "<td>"+json[i]['bridge_score']+"</td>";
							htmlitem += "<td>"+json[i]['bridge_repair_cost']+"</td>";
							htmlitem += "</tr>";
						}
						$('.databody').html(htmlitem);
						datatable = $('#user_tables').DataTable({
							'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
							'columnDefs': [{
								'targets': [0],
								'searchable': false,
								'orderable': false
							}],
							"language": {
								"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
							}
						});
						checknum_money('bridge');
						$('#selectall').data('myval','bridge');
						$('#send_type').val('bridge');
					}
					// 路燈
					if($('#searchform #money_type').val() == '3'){
						var htmlitem = '';
						for(i in json){
							alljsonlist[json[i]['light_id']] = json[i];
							htmlitem += "<tr>";
							if((limit_num-1) < i){
								if(budget_list['bridge'][json[i]['light_id']] == json[i]['light_id']){
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['light_id']+"' checked></td>";	
								}else{
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['light_id']+"'></td>";
								}
							}else{
								htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['light_id']+"' checked></td>";
							}
							
							htmlitem += "<td>"+se_type+"</td>";
							htmlitem += "<td>"+json[i]['light_numid']+"</td>";
							htmlitem += "<td>"+json[i]['light_town']+"</td>";
							htmlitem += "<td>"+json[i]['light_score']+"</td>";
							htmlitem += "<td>"+json[i]['light_repair_cost']+"</td>";
							htmlitem += "</tr>";
						}
						$('.databody').html(htmlitem);
						datatable = $('#user_tables').DataTable({
							'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
							'columnDefs': [{
								'targets': [0],
								'searchable': false,
								'orderable': false
							}],
							"language": {
								"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
							}
						});
						checknum_money('light');
						$('#selectall').data('myval','light');
						$('#send_type').val('light');
					}
					// 路樹
					if($('#searchform #money_type').val() == '4'){
						var htmlitem = '';
						for(i in json){
							alljsonlist[json[i]['tree_id']] = json[i];
							htmlitem += "<tr>";
							if((limit_num-1) < i){
								if(budget_list['bridge'][json[i]['tree_id']] == json[i]['tree_id']){
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['tree_id']+"' checked></td>";	
								}else{
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['tree_id']+"'></td>";
								}
							}else{
								htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['tree_id']+"' checked></td>";
							}
							
							htmlitem += "<td>"+se_type+"</td>";
							htmlitem += "<td>"+json[i]['tree_no']+"</td>";
							htmlitem += "<td>"+json[i]['tree_town']+"</td>";
							htmlitem += "<td>"+json[i]['tree_score']+"</td>";
							htmlitem += "<td>"+json[i]['tree_repair_cost']+"</td>";
							htmlitem += "</tr>";
						}
						$('.databody').html(htmlitem);
						datatable = $('#user_tables').DataTable({
							'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
							'columnDefs': [{
								'targets': [0],
								'searchable': false,
								'orderable': false
							}],
							"language": {
								"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
							}
						});
						checknum_money('tree');
						$('#selectall').data('myval','tree');
						$('#send_type').val('tree');
					}
					// 人行道
					if($('#searchform #money_type').val() == '5'){
						var htmlitem = '';
						for(i in json){
							alljsonlist[json[i]['sidewalk_id']] = json[i];
							htmlitem += "<tr>";
							if((limit_num-1) < i){
								if(budget_list['bridge'][json[i]['sidewalk_id']] == json[i]['sidewalk_id']){
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['sidewalk_id']+"' checked></td>";	
								}else{
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['sidewalk_id']+"'></td>";
								}
							}else{
								htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['sidewalk_id']+"' checked></td>";
							}
							
							htmlitem += "<td>"+se_type+"</td>";
							htmlitem += "<td>"+json[i]['sw_serial_num']+"</td>";
							htmlitem += "<td>"+json[i]['sw_town']+"</td>";
							htmlitem += "<td>"+json[i]['sw_score']+"</td>";
							htmlitem += "<td>"+json[i]['sw_repair_cost']+"</td>";
							htmlitem += "</tr>";
						}
						$('.databody').html(htmlitem);
						datatable = $('#user_tables').DataTable({
							'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
							'columnDefs': [{
								'targets': [0],
								'searchable': false,
								'orderable': false
							}],
							"language": {
								"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
							}
						});
						checknum_money('sidewalk');
						$('#selectall').data('myval','sidewalk');
						$('#send_type').val('sidewalk');
					}
					// 側溝
					if($('#searchform #money_type').val() == '6'){
						var htmlitem = '';
						for(i in json){
							alljsonlist[json[i]['groove_id']] = json[i];
							htmlitem += "<tr>";
							if((limit_num-1) < i){
								if(budget_list['bridge'][json[i]['groove_id']] == json[i]['groove_id']){
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['groove_id']+"' checked></td>";	
								}else{
									htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['groove_id']+"'></td>";
								}
							}else{
								htmlitem += "<td><input type='checkbox' name='selects[]' class='selectbox' value='"+json[i]['groove_id']+"' checked></td>";
							}
							
							htmlitem += "<td>"+se_type+"</td>";
							htmlitem += "<td>"+json[i]['groove_numid']+"</td>";
							htmlitem += "<td>"+json[i]['groove_town']+"</td>";
							htmlitem += "<td>"+json[i]['groove_score']+"</td>";
							htmlitem += "<td>"+json[i]['groove_repair_cost']+"</td>";
							htmlitem += "</tr>";
						}
						$('.databody').html(htmlitem);
						datatable = $('#user_tables').DataTable({
							'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
							'columnDefs': [{
								'targets': [2],
								'searchable': false,
								'orderable': false
							}],
							"language": {
								"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
							}
						});
						checknum_money('groove');
						$('#selectall').data('myval','groove');
						$('#send_type').val('groove');
					}
					// 天空纜線
					if($('#searchform #money_type').val() == '7'){

					}
				}
			});
			
			return false;
		});
		$(function(){
			jQuery(document).ajaxStart(function(){
				console.log('start');
				$.blockUI({ message: '<h4><img src="<?=base_url("suite_vendor/plugins/jqueryBlockUI/loading.gif");?>" />資料讀取中....請稍後....</h4>' });
			});
			jQuery(document).ajaxStop(function(){
				console.log('stop');
				$.unblockUI();
			});
		});
		
		$(document).on('click', '#selectall', function(){
			var msg = $(this).data('myval');
			var rows = datatable.rows({ 'search': 'applied' }).nodes();
			$('input[name="selects[]"]', rows).prop('checked', this.checked);
			// var allnum = $('input[name="selects[]"]:checked', rows).length;
			checknum_money(msg);
		});

		$(document).on('click','.check_submit',function(){
			$.post('<?=base_url('index/send_money');?>',$('#send_money').serialize(),function(json){
				if(json == true)
				{
					Swal.fire({
                            title: '建立成功',
                            icon: 'success',
                        }).then(function(){
                            location.reload();
                        });
					
				}else{
					Swal.fire('建立時發生錯誤','','error');
					return false;
				}
			},'json');
			return false;
		});
	</script>
	
	<br>
	
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>編列預算</h3>
					
					<hr>
				</div>
				<div class="col-md-3">
					<div class="card card-primary">
						<div class="card-body" style="display:flex; align-items:center;min-height: 290px;height:290px;max-height:290px;">
						
							<h2><?php if($budget_msg->num_rows() > 0){?><a href="<?=base_url('index/viewmoneylist')?>">已建立</a><?php }else{echo '已建立';}?><?=$budget_msg->num_rows();?>/7</h2>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card card-primary">
						<div class="card-body" style="display:flex; align-items:center;min-height: 290px;height:290px;max-height:290px;">
							<h2 >預算總金額<br><?=$allmoney;?>元</h2>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card card-primary chartimg">
						<div class="card-body">
						<div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand">
							<div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
							<canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>		
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
	<div class="content">
		<div class="container">
			<hr>
			<div class="row">
				<div class="col-md-10 offset-1">
					<div class="card card-primary">
						<div class="card-heade">
							<h4>項目篩選</h4>
						</div>
						<div class="card-body">
							<form class="form-signin" id="searchform" method="post" role="form">
								<div class="form-group row">
									<div class="col-3">
										<label for="money_type" class="control-label">項目：</label>
									</div>
									<div class="col-9">
										<select class="form-control" id="money_type" name="money_type"> 
											<option value="0">請選擇項目</option>
											<option value="1">道路</option>
											<option value="2">橋梁</option>
											<option value="3">路燈</option>
											<option value="4">路樹</option>
											<option value="5">人行道</option>
											<option value="6">側溝</option>
											<option value="7">天空纜線</option>
										</select>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-3 d-inline">
										<label for="select_orderby" class="control-label">金額排序方式：</label>
									</div>
										<div class="icheck-primary d-inline">
											<input type="radio" id="money_no" name="r1" checked="" value="money_no">
											<label for="money_no">不依照金額排序
											</label>
										</div>
										<div class="icheck-primary d-inline">
											<input type="radio" id="money_ase" name="r1" value="money_ase">
											<label for="money_ase">金額由大至小
											</label>
										</div>
										<div class="icheck-primary d-inline">
											<input type="radio" id="money_desc" name="r1" value="money_desc">
											<label for="money_desc">金額由小至大
											</label>
										</div>
								</div>
								<div class="form-group row">
										<div class="col-3 d-inline">
											<label for="select_orderby" class="control-label">　</label>
										</div>		
										<div class="icheck-primary d-inline">
											<input type="radio" id="money_interval" name="r1" value="money_interval">
											<label for="money_interval">區間</label>
											<input type="number" name="money_interval_s" id="money_interval_s" disabled>
											至
											<input type="number" name="money_interval_e" id="money_interval_e" disabled>
										</div>
								</div>
								<div class="form-group row">
									<div class="col-3 d-inline">
										<label for="select_orderby" class="control-label">分數排序方式：</label>
									</div>		
										<div class="icheck-success d-inline">
											<input type="radio" id="score_no" name="r2" checked="" value="score_no">
											<label for="score_no">不依照分數排序
											</label>
										</div>
										<div class="icheck-success d-inline">
											<input type="radio" id="score_ase" name="r2" value="score_ase">
											<label for="score_ase">分數由大至小
											</label>
										</div>
										<div class="icheck-success d-inline">
											<input type="radio" id="score_desc" name="r2" value="score_desc">
											<label for="score_desc">分數由小至大
											</label>
										</div>
								</div>
								<div class="form-group row">
										<div class="col-3 d-inline">
											<label for="select_orderby" class="control-label">　</label>
										</div>		
										<div class="icheck-success d-inline">
											<input type="radio" id="score_interval" name="r2" value="score_interval">
											<label for="score_interval">區間</label>
											<input type="number" name="score_interval_s" id="score_interval_s" disabled>
											至
											<input type="number" name="score_interval_e" id="score_interval_e" disabled>
										</div>
								</div>
								<div class="form-group row">
									<div class="col-3">
										<label for="limit_num" class="control-label">篩選數量：</label>
									</div>
									<div class="col-9">
										<input type="number" name="limit_num" id="limit_num" min="0" value="0">
										<span style="color:red">全部列出請輸入0</span>
									</div>
								</div>
								<button type="submit" class="btn btn-primary searchform_submit" style="float:right;">篩選</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-3 offset-3">
					<div class="card">
						<div class="card-body">
							<p>已選取<span style="color:red" id="checknum" name="checknum"></span>項</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card">
						<div class="card-body">
							<p>已選取金額<span style="color:red" id="checkmoney" name="checkmoney"></span>萬</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<hr>
			<div class="col-md-12">
				<form class="form-signin" id="send_money" method="post" role="form">
					<button type="submit" class="btn btn-primary check_submit" style="float:right;">選擇已勾選</button>
					<table id="user_tables" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th><input type="checkbox" id="selectall" data-myval='a'></th>
								<th>資產種類</th>
								<th>名稱/編號</th>
								<th>行政區</th>
								<th>分數</th>
								<th>維修金額</th>
							</tr>
						</thead>
						<tbody class="databody">
							
						</tbody>
					</table>
					<input type="hidden" name="send_type" id="send_type">
					<button type="submit" class="btn btn-primary check_submit" style="float:right;">選擇已勾選</button>
				</form>
			</div>
		</div>
	<script>
		function checknum_money(type_name){
			var all_money = 0;
			var num = '';
			var id = new Array();
			var rows = datatable.rows({ 'search': 'applied' }).nodes();
			
				num = $('input[name="selects[]"]:checked',rows).length;
				
				$('input[name="selects[]"]:checked', rows).each(function(i){
					id[this.value] = this.value;
				});
			if(type_name == 'bridge'){
				for(k in id){
					all_money += parseInt(alljsonlist[k]['bridge_repair_cost']);
				}
				
				$('#checknum').text(num);
				$('#checkmoney').text(all_money);
			}
			if(type_name == 'light'){
				for(k in id){
					all_money += parseInt(alljsonlist[k]['light_repair_cost']);
				}
				$('#checknum').text(num);
				$('#checkmoney').text(all_money);
			}
			if(type_name == 'road'){
				for(k in id){
					all_money += parseInt(alljsonlist[k]['road_repair_cost']);
				}
				$('#checknum').text(num);
				$('#checkmoney').text(all_money);
			}
			if(type_name == 'tree'){
				for(k in id){
					all_money += parseInt(alljsonlist[k]['tree_repair_cost']);
				}
				$('#checknum').text(num);
				$('#checkmoney').text(all_money);
			}
			if(type_name == 'sidewalk'){
				for(k in id){
					all_money += parseInt(alljsonlist[k]['sidewalk_repair_cost']);
				}
				$('#checknum').text(num);
				$('#checkmoney').text(all_money);
			}
			if(type_name == 'groove'){
				for(k in id){
					all_money += parseInt(alljsonlist[k]['groove_repair_cost']);
				}
				$('#checknum').text(num);
				$('#checkmoney').text(all_money);
			}
			
		}
		$(document).on('click','.selectbox',function(){
			
			// var rows = datatable.rows({ 'search': 'applied' }).nodes();
			if(se_type == '道路'){
				checknum_money('road');
			}
			if(se_type == '橋梁'){
				checknum_money('bridge');
			}
			if(se_type == '路燈'){
				checknum_money('light');
			}
			if(se_type == '路樹'){
				checknum_money('tree');
			}
			if(se_type == '人行道'){
				checknum_money('sidewalk');
			}
			if(se_type == '側溝'){
				checknum_money('groove');
			}
			if(se_type == '天空纜線'){
				checknum_money('cable');
			}
		});
		$(function(){
			$('#money_interval').click(function(){
			// $('input[type="radio"]').click(function(){
				var r_val = $(this).attr('value');
				console.log(r_val);
				if(r_val == 'money_interval'){
					$('#money_interval_s').attr('disabled',false);
					$('#money_interval_e').attr('disabled',false);
					$('#money_interval_s').attr('required',true);
					$('#money_interval_e').attr('required',true);
				}else{
					$('#money_interval_s').attr('disabled',true);
					$('#money_interval_e').attr('disabled',true);
					$('#money_interval_s').attr('required',false);
					$('#money_interval_e').attr('required',false);
				}
			});
			$('#score_interval').click(function(){
				var r_val = $(this).attr('value');
				console.log(r_val);
				if(r_val == 'score_interval'){
					$('#score_interval_s').attr('disabled',false);
					$('#score_interval_e').attr('disabled',false);
					$('#score_interval_s').attr('required',true);
					$('#score_interval_e').attr('required',true);
				}else{
					$('#score_interval_s').attr('disabled',true);
					$('#score_interval_e').attr('disabled',true);
					$('#score_interval_s').attr('required',false);
					$('#score_interval_e').attr('required',false);
				}
			});
		});
		//-------------
		//- DONUT CHART -
		//-------------
		// Get context with jQuery - using jQuery's .get() method.

		var labels = [];
		var data = [];
		var tjsonfile = <?=json_encode($budget_msg->result_array());?>;
		var jsonfile = JSON.stringify(tjsonfile);
		labels = $.map(JSON.parse(jsonfile),function(item,index){
			return item.budget_type_cn;
		});
		if(labels == '')
		{
			$('.chartimg').hide();
		}
		data = $.map(JSON.parse(jsonfile),function(item,index){
			return item.budget_money;
		});
		// labels = jsonfile.jsonarray.map(function(e){
		// 	return e.budget_type;
		// });
		// data = jsonfile.jsonarray.map(function(e){
		// 	return e.budget_money;
		// });
		var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
		var donutData        = {
				labels:labels,
				datasets:[{
					data:data,
					backgroundColor:['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de','#BE77FF']
				}]
		}
		var donutOptions     = {
			maintainAspectRatio : false,
			responsive : true,
		}
		//Create pie or douhnut chart
		// You can switch between pie and douhnut using the method below.
		var donutChart = new Chart(donutChartCanvas, {
			type: 'doughnut',
			data: donutData,
			options: donutOptions      
		})

	</script>