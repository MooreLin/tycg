<script>
	var item_msg = <?=$item_msg;?>;
	$(function(){
		console.log(item_msg);
		if(item_msg != '')
		{
			
			$('#select_type').val('edit');
			type_change('<?=$item_type?>');
			$('#asset_type').val('<?=$item_type?>');
			$('#submit_btn').css('display','block');
			switch('<?=$item_type?>'){
				case 'road':
					break;
				case 'bridge':
					$('#edit_id').val(item_msg['bridge_id']);
					$('#asset_number').val(item_msg['numero']);
					$('#asset_bridge_name').val(item_msg['name']);
					$('#asset_town').val(item_msg['bridge_town']);
					$('#asset_bridge_mile').val(item_msg['mile']);
					$('#asset_bridge_lenght').val(item_msg['length']);
					$('#asset_bridge_width').val(item_msg['width']);
					$('#asset_bridge_area').val(item_msg['area']);
					$('#asset_bridge_holes').val(item_msg['holes']);
					$('#asset_bridge_span').val(item_msg['span']);
					$('#asset_bridge_load').val(item_msg['load']);
					$('#asset_bridge_structure_t').val(item_msg['structure_t']);
					$('#asset_bridge_structure_b').val(item_msg['structure_b']);
					$('#asset_bridge_item_top').val(item_msg['item_top']);
					$('#asset_bridge_item_bottom').val(item_msg['item_bottom']);
					$('#asset_bridge_river_name').val(item_msg['river']);
					$('#asset_bridge_build_cost').val(item_msg['bridge_build_cost']);
					$('#asset_bridge_repair_cost').val(item_msg['bridge_repair_cost']);
					$('#asset_bridge_center_x').val(item_msg['center_x']);
					$('#asset_bridge_center_y').val(item_msg['center_y']);
					$('#asset_bridge_rings').val(item_msg['rings']);
					break;
				case 'light':
					$('#edit_id').val(item_msg['light_id']);
					$('#asset_number').val(item_msg['light_numid']);
					$('#asset_light_name').val(item_msg['light_road_name']);
					$('#asset_town').val(item_msg['light_town']);
					$('#asset_light_center_x').val(item_msg['light_xcode']);
					$('#asset_light_center_y').val(item_msg['light_ycode']);
					$('#asset_light_build_cost').val(item_msg['light_build_cost']);
					$('#asset_light_repair_cost').val(item_msg['light_repair_cost']);
					break;
				case 'tree':
					$('#edit_id').val(item_msg['tree_id']);
					$('#asset_number').val(item_msg['tree_no']);
					$('#asset_town').val(item_msg['tree_town']);
					$('#asset_tree_type').val(item_msg['tree_type_name']);
					$('#asset_tree_diameter').val(item_msg['tree_diameter']);
					$('#asset_tree_high').val(item_msg['tree_high']);
					$('#asset_tree_crown').val(item_msg['tree_crown']);
					$('#asset_tree_name').val(item_msg['tree_road']);
					$('#asset_tree_growing').val(item_msg['tree_growing_type']);
					$('#asset_tree_url').val(item_msg['tree_url']);
					$('#asset_tree_center_x').val(item_msg['tree_xcode']);
					$('#asset_tree_center_y').val(item_msg['tree_ycode']);
					$('#asset_tree_build_cost').val(item_msg['tree_build_cost']);
					$('#asset_tree_repair_cost').val(item_msg['tree_repair_cost']);
					break;
				case 'sidewalk':
					$('#edit_id').val(item_msg['sidewalk_id']);
					$('#asset_number').val(item_msg['sw_serial_num_no']);
					$('#asset_town').val(item_msg['sw_town']);
					$('#asset_sw_name').val(item_msg['sw_road_name']);
					$('#asset_sw_road_s').val(item_msg['sw_road_start']);
					$('#asset_sw_road_e').val(item_msg['sw_road_end']);
					$('#asset_sw_direction').val(item_msg['sw_road_direction']);
					$('#asset_sw_pave').val(item_msg['sw_pave']);
					$('#asset_sw_length').val(item_msg['sw_length']);
					$('#asset_sw_width_u').val(item_msg['sw_width_u']);
					$('#asset_sw_width_c').val(item_msg['sw_width_c']);
					$('#asset_sw_leng').val(item_msg['sw_leng']);
					$('#asset_sw_wth').val(item_msg['sw_wth']);
					$('#asset_sw_center_x').val(item_msg['sw_xcode']);
					$('#asset_sw_center_y').val(item_msg['sw_ycode']);
					$('#asset_sw_rings').val(item_msg['sw_rings']);
					$('#asset_tree_build_cost').val(item_msg['sidewalk_build_cost']);
					$('#asset_tree_repair_cost').val(item_msg['sidewalk_repair_cost']);
					break;
				case 'groove':
					$('#edit_id').val(item_msg['groove_id']);
					$('#asset_number').val(item_msg['groove_numid']);
					$('#asset_town').val(item_msg['groove_town']);
					$('#asset_groove_road').val(item_msg['groove_road']);
					$('#asset_groove_length').val(item_msg['groove_length']);
					$('#asset_groove_width').val(item_msg['groove_width']);
					$('#asset_groove_type').val(item_msg['groove_type']);
					$('#asset_groove_center_x').val(item_msg['groove_xcode']);
					$('#asset_groove_center_y').val(item_msg['groove_ycode']);
					$('#asset_groove_rings').val(item_msg['groove_rings']);
					$('#asset_tree_build_cost').val(item_msg['groove_build_cost']);
					$('#asset_tree_repair_cost').val(item_msg['groove_repair_cost']);
					break;
				case 'cable':
					break;
				default:
					break;
			}
		}else{
			$('#select_type').val('add');
		}
	});
	$(document).on('change','#asset_type',function(){
		var type = $(this).val();
		$('#submit_btn').css('display','block');
		type_change(type);
	});

	function type_change(type){
		switch (type) {
			case 'road':
				$('#road').css('display','block');
				$('#bridge').css('display','none');
				$('#light').css('display','none');
				$('#tree').css('display','none');
				$('#sidewalk').css('display','none');
				$('#groove').css('display','none');
				$('#cable').css('display','none');
				break;
			case 'bridge':
				$('#bridge').css('display','block');
				$('#road').css('display','none');
				$('#light').css('display','none');
				$('#tree').css('display','none');
				$('#sidewalk').css('display','none');
				$('#groove').css('display','none');
				$('#cable').css('display','none');
				$('#asset_bridge_structure_t').select2({
					theme: 'bootstrap4'
				})
				$('#asset_bridge_structure_b').select2({
					theme: 'bootstrap4'
				})
				$('#asset_bridge_item_top').select2({
					theme: 'bootstrap4'
				})
				$('#asset_bridge_item_bottom').select2({
					theme: 'bootstrap4'
				})
				break;
			case 'light':
				$('#light').css('display','block');
				$('#road').css('display','none');
				$('#bridge').css('display','none');
				$('#tree').css('display','none');
				$('#sidewalk').css('display','none');
				$('#groove').css('display','none');
				$('#cable').css('display','none');
				break;
			case 'tree':
				$('#tree').css('display','block');
				$('#road').css('display','none');
				$('#bridge').css('display','none');
				$('#light').css('display','none');
				$('#sidewalk').css('display','none');
				$('#groove').css('display','none');
				$('#cable').css('display','none');
				$('#asset_tree_type').select2({
					theme: 'bootstrap4'
				})
				$('#asset_tree_growing').select2({
					theme: 'bootstrap4'
				})
				break;
			case 'sidewalk':
				$('#sidewalk').css('display','block');
				$('#road').css('display','none');
				$('#bridge').css('display','none');
				$('#light').css('display','none');
				$('#tree').css('display','none');
				$('#groove').css('display','none');
				$('#cable').css('display','none');
				break;
			case 'groove':
				$('#groove').css('display','block');
				$('#road').css('display','none');
				$('#bridge').css('display','none');
				$('#light').css('display','none');
				$('#tree').css('display','none');
				$('#sidewalk').css('display','none');
				$('#cable').css('display','none');
				break;
			case 'cable':
				$('#cable').css('display','block');
				$('#road').css('display','none');
				$('#bridge').css('display','none');
				$('#light').css('display','none');
				$('#tree').css('display','none');
				$('#sidewalk').css('display','none');
				$('#groove').css('display','none');
				break;
		
			default:
				$('#road').css('display','none');
				$('#bridge').css('display','none');
				$('#light').css('display','none');
				$('#tree').css('display','none');
				$('#sidewalk').css('display','none');
				$('#groove').css('display','none');
				$('#cable').css('display','none');
				$('#submit_btn').css('display','none');
				break;
		}
	}

	$(document).on('click','.searchform_submit',function(){
		$.ajax({
			url: "<?=base_url('index/send_add_asset')?>",
			data: $('#send_addmsg').serialize(),
			type:"POST",
			error:function(xhr, ajaxOptions, thrownError){ 
				console.log(thrownError)
				alert(xhr.responseText);
			},
			success:function(json){
				if(json == true)
				{
					Swal.fire({
							title: '儲存成功',
							icon: 'success',
						}).then(function(){
							location.reload();
						});
					
				}else{
					Swal.fire('儲存時發生錯誤','','error');
					return false;
				}
			}
		});

		return false;
	});
</script>
<br>
<div class="content-fluid">
	<div class="container">
		<form class="form-signin" id="send_addmsg" method="post" role="form">
			<input type="hidden" name="edit_id" id="edit_id">
			<input type="hidden" name="select_type" id="select_type">
			<div class="form-group row">
				<label for="asset_type" class="control-label">項目：</label>
				<select class="form-control" id="asset_type" name="asset_type"> 
					<option value="0">請選擇項目</option>
					<option value="road">道路</option>
					<option value="bridge">橋梁</option>
					<option value="light">路燈</option>
					<option value="tree">路樹</option>
					<option value="sidewalk">人行道</option>
					<option value="groove">側溝</option>
					<option value="cable">天空纜線</option>
				</select>
			</div>
			<div class="form-group row">
				<label for="asset_number" class="control-label">設施編號：</label>
				<input type="text" class="form-control" name="asset_number" id="asset_number">
			</div>
			<div class="form-group row">
				<label for="asset_town" class="control-label">區域名稱：</label>
				<select class="form-control" id="asset_town" name="asset_town"> 
					<option value="0">請選擇項目</option>
					<?php foreach ($area_msg as $value) {?>
						<option value="<?=$value['area_name']?>"><?=$value['area_name']?></option>
					<?php }?>
				</select>
			</div>
			<!-- 道路 -->
				<div id="road">
				</div>
			<!-- 橋梁 -->
				<div id="bridge" style="display: none;">
					<div class="form-group row">
						<label for="asset_bridge_name" class="control-label">設施名稱：</label>
						<input type="text" class="form-control" name="asset_bridge_name" id="asset_bridge_name">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_mile" class="control-label">里程：</label>
						<input type="text" class="form-control" name="asset_bridge_mile" id="asset_bridge_mile">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_lenght" class="control-label">長度：</label>
						<input type="text" class="form-control" name="asset_bridge_lenght" id="asset_bridge_lenght">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_width" class="control-label">寬度：</label>
						<input type="text" class="form-control" name="asset_bridge_width" id="asset_bridge_width">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_area" class="control-label">面積：</label>
						<input type="text" class="form-control" name="asset_bridge_area" id="asset_bridge_area">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_holes" class="control-label">孔洞數：</label>
						<input type="text" class="form-control" name="asset_bridge_holes" id="asset_bridge_holes">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_span" class="control-label">跨距：</label>
						<input type="text" class="form-control" name="asset_bridge_span" id="asset_bridge_span">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_load" class="control-label">載重：</label>
						<input type="text" class="form-control" name="asset_bridge_load" id="asset_bridge_load">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_structure_t" class="control-label">上部結構名稱：</label>
						<select class="form-control " name="asset_bridge_structure_t" id="asset_bridge_structure_t">
							<option value="0">　</option>
							<option value="混凝土橋類">混凝土橋類</option>
							<option value="磚石橋類">磚石橋類</option>
						</select>
					</div>
					<div class="form-group row">
						<label for="asset_bridge_structure_b" class="control-label">下部結構名稱：</label>
						<select class="form-control " name="asset_bridge_structure_b" id="asset_bridge_structure_b">
							<option value="0">　</option>
							<option value="懸臂">懸臂</option>
							<option value="重力">重力</option>
							<option value="牆式">牆式</option>
						</select>
					</div>
					<div class="form-group row">
						<label for="asset_bridge_item_top" class="control-label">上部結構分類：</label>
						<select class="form-control " name="asset_bridge_item_top" id="asset_bridge_item_top">
							<option value="0">　</option>
							<option value="I型梁">I型梁</option>
							<option value="T型梁">T型梁</option>
							<option value="U型梁">U型梁</option>
							<option value="PC梁">PC梁</option>
							<option value="版梁">版梁</option>
						</select>
					</div>
					<div class="form-group row">
						<label for="asset_bridge_item_bottom" class="control-label">下部結構分類：</label>
						<select class="form-control " name="asset_bridge_item_bottom" id="asset_bridge_item_bottom">
							<option value="0">　</option>
							<option value="多柱式">多柱式</option>
							<option value="牆式">牆式</option>
							<option value="懸臂墩">懸臂墩</option>
							<option value="斜柱式">斜柱式</option>
							<option value="樁架墩">樁架墩</option>
						</select>
					</div>
					<div class="form-group row">
						<label for="asset_bridge_center_xy" class="control-label">地圖位置：</label>
						<div class="input-group">
							<input type="text" class="form-control" name="asset_bridge_center_x" id="asset_bridge_center_x" readonly>
							<input type="text" class="form-control" name="asset_bridge_center_y" id="asset_bridge_center_y" readonly>
							<button type="button" class="btn btn-primary" data-toggle="map.position">尋找</button>
						</div>
					</div>
					<div class="form-group row">
						<label for="asset_bridge_rings" class="control-label">區域：</label>
						<div class="input-group">
							<input type="text" class="form-control" name="asset_bridge_rings" id="asset_bridge_rings" readonly>
							<button type="button" class="btn btn-primary" data-toggle="map.position">尋找</button>
						</div>
							
					</div>
					<div class="form-group row">
						<label for="asset_bridge_river_name" class="control-label">河流名稱：</label>
						<input type="text" class="form-control" name="asset_bridge_river_name" id="asset_bridge_river_name">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_build_cost" class="control-label">建置成本：</label>
						<input type="text" class="form-control" name="asset_bridge_build_cost" id="asset_bridge_build_cost">
					</div>
					<div class="form-group row">
						<label for="asset_bridge_repair_cost" class="control-label">維護成本：</label>
						<input type="text" class="form-control" name="asset_bridge_repair_cost" id="asset_bridge_repair_cost">
					</div>
				</div>
			<!-- 路燈 -->
				<div id="light" style="display: none;">
					<div class="form-group row">
						<label for="asset_light_name" class="control-label">路名：</label>
						<input type="text" class="form-control" name="asset_light_name" id="asset_light_name">
					</div>
					<div class="form-group row">
						<label for="asset_light_center_xy" class="control-label">地圖位置：</label>
						<div class="input-group">
							<input type="text" class="form-control" name="asset_light_center_x" id="asset_light_center_x" readonly>
							<input type="text" class="form-control" name="asset_light_center_y" id="asset_light_center_y" readonly>
							<button type="button" class="btn btn-primary" data-toggle="map.position">尋找</button>
						</div>
					</div>
					<div class="form-group row">
						<label for="asset_light_build_cost" class="control-label">建置成本：</label>
						<input type="text" class="form-control" name="asset_light_build_cost" id="asset_light_build_cost">
					</div>
					<div class="form-group row">
						<label for="asset_light_repair_cost" class="control-label">維護成本：</label>
						<input type="text" class="form-control" name="asset_light_repair_cost" id="asset_light_repair_cost">
					</div>
				</div>
			<!-- 路樹 -->
				<div id="tree" style="display: none;">
					<div class="form-group row">
						<label for="asset_tree_name" class="control-label">路名：</label>
						<input type="text" class="form-control" name="asset_tree_name" id="asset_tree_name">
					</div>
					<div class="form-group row">
						<label for="asset_tree_diameter" class="control-label">樹胸徑(cm)：</label>
						<input type="text" class="form-control" name="asset_tree_diameter" id="asset_tree_diameter">
					</div>
					<div class="form-group row">
						<label for="asset_tree_high" class="control-label">樹高(m)：</label>
						<input type="text" class="form-control" name="asset_tree_high" id="asset_tree_high">
					</div>
					<div class="form-group row">
						<label for="asset_tree_crown" class="control-label">樹冠投影面積(平方公尺)：</label>
						<input type="text" class="form-control" name="asset_tree_crown" id="asset_tree_crown">
					</div>
					<div class="form-group row">
						<label for="asset_tree_type" class="control-label">路樹品種：</label>
						<select class="form-control " name="asset_tree_type" id="asset_tree_type">
							<option value="0">　</option>
							<?php foreach ($tree_type as $trval) {?>
								<option value="<?=$trval['tree_type_name']?>"><?=$trval['tree_type_name'];?>(<?=$trval['tree_type_type'];?>)</option>
							<?php }?>
						</select>
					</div>
					<div class="form-group row">
						<label for="asset_tree_growing" class="control-label">生長狀況：</label>
						<select class="form-control " name="asset_tree_growing" id="asset_tree_growing">
							<option value="0">　</option>
							<option value="良好">良好</option>
							<option value="枯死">枯死</option>
							<option value="斷頭">斷頭</option>
							<option value="危木">危木</option>
							<option value="不良">不良</option>

						</select>
					</div>
					<div class="form-group row">
						<label for="asset_tree_center_xy" class="control-label">地圖位置：</label>
						<div class="input-group">
							<input type="text" class="form-control" name="asset_tree_center_x" id="asset_tree_center_x" readonly>
							<input type="text" class="form-control" name="asset_tree_center_y" id="asset_tree_center_y" readonly>
							<button type="button" class="btn btn-primary" data-toggle="map.position">尋找</button>
						</div>
					</div>
					<div class="form-group row">
						<label for="asset_tree_url" class="control-label">介紹連結：</label>
						<input type="text" class="form-control" name="asset_tree_url" id="asset_tree_url">
					</div>
					<div class="form-group row">
						<label for="asset_tree_build_cost" class="control-label">建置成本：</label>
						<input type="text" class="form-control" name="asset_tree_build_cost" id="asset_tree_build_cost">
					</div>
					<div class="form-group row">
						<label for="asset_tree_repair_cost" class="control-label">維護成本：</label>
						<input type="text" class="form-control" name="asset_tree_repair_cost" id="asset_tree_repair_cost">
					</div>
				</div>
			<!-- 人行道 -->
				<div id="sidewalk" style="display: none;">
					<div class="form-group row">
						<label for="asset_sw_name" class="control-label">道路名稱：</label>
						<input type="text" class="form-control" name="asset_sw_name" id="asset_sw_name">
					</div>
					<div class="form-group row">
						<label for="asset_sw_road_s" class="control-label">道路起點：</label>
						<input type="text" class="form-control" name="asset_sw_road_s" id="asset_sw_road_s">
					</div>
					<div class="form-group row">
						<label for="asset_sw_road_e" class="control-label">道路迄點：</label>
						<input type="text" class="form-control" name="asset_sw_road_e" id="asset_sw_road_e">
					</div>
					
					<div class="form-group row">
						<label for="asset_sw_direction" class="control-label">道路方向：</label>
						<select class="form-control " name="asset_sw_direction" id="asset_sw_direction">
							<option value="0">　</option>
							<option value="1">東/南</option>
							<option value="2">西/北</option>
							<option value="3">徒步區</option>
						</select>
					</div>
					
					<div class="form-group row">
						<label for="asset_sw_pave" class="control-label">鋪設類型：</label>
						<input type="text" class="form-control" name="asset_sw_pave" id="asset_sw_pave">
					</div>
					<div class="form-group row">
						<label for="asset_sw_length" class="control-label">道路長度(中心線長度)：</label>
						<input type="text" class="form-control" name="asset_sw_length" id="asset_sw_length">
					</div>
					<div class="form-group row">
						<label for="asset_sw_width_u" class="control-label">道路寬度(包含雙向人行道)：</label>
						<input type="text" class="form-control" name="asset_sw_width_u" id="asset_sw_width_u">
					</div>
					<div class="form-group row">
						<label for="asset_sw_width_c" class="control-label">道路寬度(不含人行道)：</label>
						<input type="text" class="form-control" name="asset_sw_width_c" id="asset_sw_width_c">
					</div>
					<div class="form-group row">
						<label for="asset_sw_leng" class="control-label">人行道長度：</label>
						<input type="text" class="form-control" name="asset_sw_leng" id="asset_sw_leng">
					</div>
					<div class="form-group row">
						<label for="asset_sw_wth" class="control-label">人行道總寬度：</label>
						<input type="text" class="form-control" name="asset_sw_wth" id="asset_sw_wth">
					</div>
					
					<div class="form-group row">
						<label for="asset_sw_center_xy" class="control-label">地圖位置：</label>
						<div class="input-group">
							<input type="text" class="form-control" name="asset_sw_center_x" id="asset_sw_center_x" readonly>
							<input type="text" class="form-control" name="asset_sw_center_y" id="asset_sw_center_y" readonly>
							<button type="button" class="btn btn-primary" data-toggle="map.position">尋找</button>
						</div>
					</div>
					<div class="form-group row">
						<label for="asset_sw_rings" class="control-label">區域：</label>
						<div class="input-group">
							<input type="text" class="form-control" name="asset_sw_rings" id="asset_sw_rings" readonly>
							<button type="button" class="btn btn-primary" data-toggle="map.position">尋找</button>
						</div>
							
					</div>
					<div class="form-group row">
						<label for="asset_sw_build_cost" class="control-label">建置成本：</label>
						<input type="text" class="form-control" name="asset_sw_build_cost" id="asset_sw_build_cost">
					</div>
					<div class="form-group row">
						<label for="asset_sw_repair_cost" class="control-label">維護成本：</label>
						<input type="text" class="form-control" name="asset_sw_repair_cost" id="asset_sw_repair_cost">
					</div>
				</div>
			<!-- 側溝 -->
				<div id="groove" style="display: none;">

					<div class="form-group row">
						<label for="asset_groove_road" class="control-label">道路名稱：</label>
						<input type="text" class="form-control" name="asset_groove_road" id="asset_groove_road">
					</div>
					<div class="form-group row">
						<label for="asset_groove_length" class="control-label">側溝長度：</label>
						<input type="text" class="form-control" name="asset_groove_length" id="asset_groove_length">
					</div>
					<div class="form-group row">
						<label for="asset_groove_width" class="control-label">側溝寬度：</label>
						<input type="text" class="form-control" name="asset_groove_width" id="asset_groove_width">
					</div>
					<div class="form-group row">
						<label for="asset_groove_type" class="control-label">側溝材料：</label>
						<input type="text" class="form-control" name="asset_groove_type" id="asset_groove_type">
					</div>
					<div class="form-group row">
						<label for="asset_groove_center_xy" class="control-label">地圖位置：</label>
						<div class="input-group">
							<input type="text" class="form-control" name="asset_groove_center_x" id="asset_groove_center_x" readonly>
							<input type="text" class="form-control" name="asset_groove_center_y" id="asset_groove_center_y" readonly>
							<button type="button" class="btn btn-primary" data-toggle="map.position">尋找</button>
						</div>
					</div>
					<div class="form-group row">
						<label for="asset_groove_rings" class="control-label">區域：</label>
						<div class="input-group">
							<input type="text" class="form-control" name="asset_groove_rings" id="asset_groove_rings" readonly>
							<button type="button" class="btn btn-primary" data-toggle="map.position">尋找</button>
						</div>
							
					</div>
					<div class="form-group row">
						<label for="asset_groove_build_cost" class="control-label">建置成本：</label>
						<input type="text" class="form-control" name="asset_groove_build_cost" id="asset_groove_build_cost">
					</div>
					<div class="form-group row">
						<label for="asset_groove_repair_cost" class="control-label">維護成本：</label>
						<input type="text" class="form-control" name="asset_groove_repair_cost" id="asset_groove_repair_cost">
					</div>
				</div>
			<!-- 天空纜線 -->
				<div id="cable">

				</div>
				<div>
					<button type="submit" class="btn btn-primary searchform_submit"  id="submit_btn" style="float:right;display: none;">儲存</button>
					<a href="<?=base_url('index/asset')?>" class="btn btn-outline-secondary" style="float:right;">取消</a>
				</div>
			
		</form>
	</div>
</div>
<script>
	$(function(){
		//Initialize Select2 Elements
		$('#asset_bridge_structure_t').select2({
			theme: 'bootstrap4'
		})
		$('#asset_bridge_structure_b').select2({
			theme: 'bootstrap4'
		})
		$('#asset_bridge_item_top').select2({
			theme: 'bootstrap4'
		})
		$('#asset_bridge_item_bottom').select2({
			theme: 'bootstrap4'
		})
		$('#asset_tree_type').select2({
			theme: 'bootstrap4'
		})
		$('#asset_tree_growing').select2({
			theme: 'bootstrap4'
		})
	})
</script>
