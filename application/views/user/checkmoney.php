	<script>
		var alljsonlist = new Array();
		var se_type ='';
		$(function () {
			// $('#user_tables').DataTable({});
			
			$('#renovate_class').select2({
				theme: "bootstrap4",
			});
		});

		$(document).on('click','.lookbtn',function(){
			$('#lookmodal').modal('show');
			// var id = $(this).data('no');
			var budtype = $(this).data('budgettype');
			var itemhtml = '';
			$.post('<?=base_url('index/get_year_list_msg')?>',{budtype:budtype},function(json){
				// console.log(json);
				for(i in json){
					itemhtml +='<tr>';
					itemhtml +='<td>'+json[i]['budget_type_cn']+'</td>';
					itemhtml +='<td>'+json[i]['budget_year']+'</td>';
					itemhtml +='<td>'+json[i]['budget_quantity']+'</td>';
					itemhtml +='<td>'+json[i]['budget_money']+'</td>';
					itemhtml +='<td>'+json[i]['budget_check_money']+'</td>';
					itemhtml +='</tr>';
					$('.modal-title').text(json[i]['budget_type_cn']+'歷年資料');
				}
				$('#look_tbody').html(itemhtml);
			},'json');
		});

		$(document).on('click','.editbtn',function(){
			var no = $(this).data('no');
			console.log(no);
		});
	</script>
	
	<br>
	
	<div class="content">
		<div class="container">
			<div class="row">
				
				<!-- Model -->
					<div class="modal fade" id="lookmodal" style="display: none;" data-backdrop="static">
						<div class="modal-dialog modal-lg">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title"></h4>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">×</span></button>
							</div>
							<div class="modal-body">
							<table id="user_tables" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>資產種類</th>
										<th>年度</th>
										<th>數量</th>
										<th>預算金額</th>
										<th>核定金額</th>
									</tr>
								</thead>
								<tbody id="look_tbody">
									
								</tbody>
							</table>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-default pull-left" data-dismiss="modal">關閉</button>
							</div>
						</div>
						<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
				<div class="col-md-12">
					<h3>核定預算</h3>
					
					<hr>
				</div>
				<div class="col-md-3">
					<div class="card card-primary">
						<div class="card-body" style="display:flex; align-items:center;min-height: 290px;height:290px;max-height:290px;">
						
							<h2>已建立</a><?=$budget_msg->num_rows();?>/7</h2>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card card-primary">
						<div class="card-body" style="display:flex; align-items:center;min-height: 290px;height:290px;max-height:290px;">
							<h2>預算總金額<br><?=$allmoney;?>元</h2>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card card-primary">
						<div class="card-body">
						<div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand">
							<div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
							<canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>		
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
	<div class="content">
		<div class="container">
			<hr>
			<div class="col-md-12">
				<form class="form-signin" id="send_money" method="post" role="form">
					<table id="user_tables" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>　</th>
								<th>資產種類</th>
								<th>年度</th>
								<th>數量</th>
								<th>預算金額</th>
								<th>核定金額</th>
								<th>　</th>
							</tr>
						</thead>
						<tbody>
							
							<?php foreach($budget_msg->result_array() as $key => $value) {
								if(date('Y')-1911+1 != $value['budget_year']){continue;}?>
								<tr>
									<td><?=$key+1?></td>
									<td><?=$value['budget_type_cn']?></td>
									<td><?=$value['budget_year']?></td>
									<td><?=$value['budget_quantity']?></td>
									<td><?=$value['budget_money']?></td>
									<td><?=$value['budget_check_money']?></td>
									<td>
										<button type="button" class="btn btn-info lookbtn" data-toggle="modal" title="" data-no="<?=$value['budget_id'];?>" data-budgettype="<?=$value['budget_type'];?>">
										<span data-toggle="tooltip" title="" data-original-title="歷年資料"><i class="fa fa-search" ></i></button>
										<?php if($value['budget_check_money'] == null):?>
											<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-no="<?=$value['budget_id'];?>" data-budgettype="<?=$value['budget_type'];?>">
											<span data-toggle="tooltip" title="" data-original-title="核定"><i class="fa fa-check" ></i></button>
										<?php endif;?>
									</td>
								</tr>
							<?php }?>
						</tbody>
					</table>
				</form>
			</div>
		</div>
	<script>
		//-------------
		//- DONUT CHART -
		//-------------
		// Get context with jQuery - using jQuery's .get() method.

		var labels = [];
		var data = [];
		var tjsonfile = <?=json_encode($budget_msg->result_array());?>;
		var jsonfile = JSON.stringify(tjsonfile);
		labels = $.map(JSON.parse(jsonfile),function(item,index){
			return item.budget_type_cn;
		});
		data = $.map(JSON.parse(jsonfile),function(item,index){
			return item.budget_money;
		});
		// labels = jsonfile.jsonarray.map(function(e){
		// 	return e.budget_type;
		// });
		// data = jsonfile.jsonarray.map(function(e){
		// 	return e.budget_money;
		// });
		var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
		var donutData        = {
				labels:labels,
				datasets:[{
					data:data,
					backgroundColor:['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de','#BE77FF']
				}]
			// labels: [
			// 	'道路', 
			// 	'橋梁',
			// 	'路燈', 
			// 	'路樹', 
			// 	'人行道', 
			// 	'邊溝', 
			// 	'天空纜線', 
			// ],
			// datasets: [
			// 	{
			// 	data: [700,500,400,600,300,100,200],
			// 	backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de','#BE77FF'],
			// 	}
			// ]
		}
		var donutOptions     = {
			maintainAspectRatio : false,
			responsive : true,
		}
		//Create pie or douhnut chart
		// You can switch between pie and douhnut using the method below.
		var donutChart = new Chart(donutChartCanvas, {
			type: 'doughnut',
			data: donutData,
			options: donutOptions      
		})

	</script>