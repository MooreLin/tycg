<script>
     var lang = {
            "sProcessing": "處理中...",
            "sLengthMenu": "每頁 _MENU_ 項",
            "sZeroRecords": "沒有匹配結果",
            "sInfo": "當前顯示第 _START_ 至 _END_ 項，共 _TOTAL_ 項。",
            "sInfoEmpty": "當前顯示第 0 至 0 項，共 0 項",
            "sInfoFiltered": "(由 _MAX_ 項結果過濾)",
            "sInfoPostFix": "",
            "sSearch": "搜尋:",
            "sUrl": "",
            "sEmptyTable": "無資料",
            "sLoadingRecords": "載入中...",
            "sInfoThousands": ",",
            "oPaginate": {
            "sFirst": "首頁",
            "sPrevious": "上頁",
            "sNext": "下頁",
            "sLast": "末頁",
            "sJump": "跳轉"
          },
          "oAria": {
            "sSortAscending": ": 以升序排列此列",
            "sSortDescending": ": 以降序排列此列"
          }
        };
		$(function () {
			$('#road_tables').DataTable({
          language:lang, //提示資訊
          responsive: true,
        });
			$('#bridge_tables').DataTable({
          language:lang, //提示資訊
      });
      $('#cable_tables').DataTable({
          language:lang, //提示資訊
      });
     
    });

    // 橋梁btn
    $(document).on('click','.bridge_tabbtn',function(){
      var btype = $(this).data('type');
      $('.addbtn').data('id','bridge');
      $('#bridge_tables').DataTable(
        {
          language:lang, //提示資訊
          responsive: true,
          bDestroy: true,
          processing: true, //隱藏載入提示,自行處理
          serverSide: true, //啟用伺服器端分頁
          ajax: function(data,callback,setting){
            var param = {};
            param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
            param.start = data.start;//開始的記錄序號
            param.page = (data.start / data.length);//當前頁碼
            param.search = data.search.value;
            param.type = btype;
            $.post('<?=base_url('index/get_msg')?>',{param:param},function(result){
              setTimeout(function(){
                var returnData = {};
                returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
                returnData.recordsTotal = result.count;//返回資料全部記錄
                returnData.recordsFiltered = result.searchcount;//後臺不實現過濾功能，每次查詢均視作全部結果
                returnData.data = result.list;//返回的資料列表
                callback(returnData);
              },200);
            },'json');
          },
          columnDefs:[{
            render:function(data, type, row){
                htmlitem = '<div class="btn-group">';
                htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-btype="'+btype+'" data-no="'+row[0]+'">';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
                htmlitem += '<button type="button" class="btn btn-outline-info" data-toggle="map.position" data-lng="121.303" data-lat="25.0263">';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="查看地圖"><i class="fa fa-map-marker-alt"></i></span></button>';
                htmlitem += '</div>';
              return htmlitem;
            },"targets": 6
          }]
        }
      );
      
    });
    // 路燈btn
    $(document).on('click','.light_tabbtn',function(){
      var btype = $(this).data('type');
      $('.addbtn').data('id','light');
      $('#light_tables').DataTable(
        {
          language:lang, //提示資訊
          bDestroy: true,
          processing: true, //隱藏載入提示,自行處理
          serverSide: true, //啟用伺服器端分頁
          ajax: function(data,callback,setting){
            var param = {};
            param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
            param.start = data.start;//開始的記錄序號
            param.page = (data.start / data.length);//當前頁碼
            param.search = data.search.value;
            param.type = btype;
            $.post('<?=base_url('index/get_msg')?>',{param:param},function(result){
              setTimeout(function(){
                var returnData = {};
                returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
                returnData.recordsTotal = result.count;//返回資料全部記錄
                returnData.recordsFiltered = result.count;//後臺不實現過濾功能，每次查詢均視作全部結果
                returnData.data = result.list;//返回的資料列表
                callback(returnData);
              },200);
            },'json');
          },
          columnDefs:[{
            render:function(data, type, row){
                htmlitem = '<div class="btn-group">';
                htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-btype="'+btype+'" data-no="'+row[0]+'">';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
                htmlitem += '<button type="button" class="btn btn-outline-info" data-toggle="map.position" data-lng="121.303" data-lat="25.0263">';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="查看地圖"><i class="fa fa-map-marker-alt"></i></span></button>';
                htmlitem += '</div>';
              return htmlitem;
            },"targets": 6
          }]
        }
      );
      
    });

    // 路樹btn
    $(document).on('click','.tree_tabbtn',function(){
      
      var btype = $(this).data('type');
      $('.addbtn').data('id','tree');
      $('#tree_tables').DataTable(
        {
          language:lang, //提示資訊
          bDestroy: true,
          processing: true, //隱藏載入提示,自行處理
          serverSide: true, //啟用伺服器端分頁
          ajax: function(data,callback,setting){
            var param = {};
            param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
            param.start = data.start;//開始的記錄序號
            param.page = (data.start / data.length);//當前頁碼
            param.type = btype;
            param.search = data.search.value;
            $.post('<?=base_url('index/get_msg')?>',{param:param},function(result){
              setTimeout(function(){
                var returnData = {};
                returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
                returnData.recordsTotal = result.count;//返回資料全部記錄
                returnData.recordsFiltered = result.count;//後臺不實現過濾功能，每次查詢均視作全部結果
                returnData.data = result.list;//返回的資料列表
                callback(returnData);
              },200);
            },'json');
          },
          columnDefs:[{
            render:function(data, type, row){
                htmlitem = '<div class="btn-group">';
                htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-btype="'+btype+'" data-no="'+row[0]+'" >';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
                htmlitem += '<button type="button" class="btn btn-outline-info" data-toggle="map.position" data-lng="121.303" data-lat="25.0263">';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="查看地圖">';
                htmlitem += '<i class="fa fa-map-marker-alt"></i>';
                htmlitem += '</span>';
                htmlitem += '</button>';
                htmlitem += '<button type="button" class="btn btn-outline-info" data-toggle="map.polygon">';
                htmlitem += '<article class="d-none">[[[121.3031870406291,25.026362330426437],[121.30261686301493,25.026167734114146],[121.30258222252878,25.026255018616162],[121.30254537888405,25.026340538955566],[121.30311591520399,25.026535258488344],[121.30314956650287,25.026449193798236],[121.3031870406291,25.026362330426437]]]</article>';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="查看地圖">';
                htmlitem += '<i class="fa fa-map"></i>';
                htmlitem += '</span>';
                htmlitem += '</button>';
                htmlitem += '</div>';
              return htmlitem;
            },"targets": 8
          }]
        }
      );
    });

    
    // 人行道btn
    $(document).on('click','.sidewalk_tabbtn',function(){
      var btype = $(this).data('type');
      $('.addbtn').data('id','sidewalk');
      $('#sidewalk_tables').DataTable(
        {
          language:lang, //提示資訊
          bDestroy: true,
          processing: true, //隱藏載入提示,自行處理
          serverSide: true, //啟用伺服器端分頁
          ajax: function(data,callback,setting){
            var param = {};
            param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
            param.start = data.start;//開始的記錄序號
            param.page = (data.start / data.length);//當前頁碼
            param.type = btype;
            param.search = data.search.value;
            $.post('<?=base_url('index/get_msg')?>',{param:param},function(result){
              setTimeout(function(){
                var returnData = {};
                returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
                returnData.recordsTotal = result.count;//返回資料全部記錄
                returnData.recordsFiltered = result.count;//後臺不實現過濾功能，每次查詢均視作全部結果
                returnData.data = result.list;//返回的資料列表
                callback(returnData);
              },200);
            },'json');
          },
          columnDefs:[{
            render:function(data, type, row){
                htmlitem = '<div class="btn-group">';
                htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-btype="'+btype+'" data-no="'+row[0]+'" >';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
                htmlitem += '<button type="button" class="btn btn-outline-info" data-toggle="map.position" data-lng="121.303" data-lat="25.0263">';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="查看地圖">';
                htmlitem += '<i class="fa fa-map-marker-alt"></i>';
                htmlitem += '</span>';
                htmlitem += '</button>';
                htmlitem += '<button type="button" class="btn btn-outline-info" data-toggle="map.polygon">';
                htmlitem += '<article class="d-none">[[[121.3031870406291,25.026362330426437],[121.30261686301493,25.026167734114146],[121.30258222252878,25.026255018616162],[121.30254537888405,25.026340538955566],[121.30311591520399,25.026535258488344],[121.30314956650287,25.026449193798236],[121.3031870406291,25.026362330426437]]]</article>';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="查看地圖">';
                htmlitem += '<i class="fa fa-map"></i>';
                htmlitem += '</span>';
                htmlitem += '</button>';
                htmlitem += '</div>';
              return htmlitem;
            },"targets": 9
          }]
        }
      );
    });
    
    // 側溝btn
    $(document).on('click','.groove_tabbtn',function(){
      var btype = $(this).data('type');
      $('.addbtn').data('id','groove');
      $('#groove_tables').DataTable(
        {
          language:lang, //提示資訊
          bDestroy: true,
          processing: true, //隱藏載入提示,自行處理
          serverSide: true, //啟用伺服器端分頁
          ajax: function(data,callback,setting){
            var param = {};
            param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
            param.start = data.start;//開始的記錄序號
            param.page = (data.start / data.length);//當前頁碼
            param.type = btype;
            param.search = data.search.value;
            $.post('<?=base_url('index/get_msg')?>',{param:param},function(result){
              console.log(result);
              setTimeout(function(){
                var returnData = {};
                returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
                returnData.recordsTotal = result.count;//返回資料全部記錄
                returnData.recordsFiltered = result.searchcount;//後臺不實現過濾功能，每次查詢均視作全部結果
                returnData.data = result.list;//返回的資料列表
                callback(returnData);
              },200);
            },'json');
          },
          columnDefs:[{
            render:function(data, type, row){
                htmlitem = '<div class="btn-group">';
                htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-btype="'+btype+'" data-no="'+row[0]+'" >';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
                htmlitem += '<button type="button" class="btn btn-outline-info" data-toggle="map.position" data-lng="121.303" data-lat="25.0263">';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="查看地圖">';
                htmlitem += '<i class="fa fa-map-marker-alt"></i>';
                htmlitem += '</span>';
                htmlitem += '</button>';
                htmlitem += '<button type="button" class="btn btn-outline-info" data-toggle="map.polygon">';
                htmlitem += '<article class="d-none">[[[121.3031870406291,25.026362330426437],[121.30261686301493,25.026167734114146],[121.30258222252878,25.026255018616162],[121.30254537888405,25.026340538955566],[121.30311591520399,25.026535258488344],[121.30314956650287,25.026449193798236],[121.3031870406291,25.026362330426437]]]</article>';
                htmlitem += '<span data-toggle="tooltip" title="" data-original-title="查看地圖">';
                htmlitem += '<i class="fa fa-map"></i>';
                htmlitem += '</span>';
                htmlitem += '</button>';
                htmlitem += '</div>';
              return htmlitem;
            },"targets": 8
          }]
        }
      );
    });
    $(document).on('click','.addbtn',function(){
      window.location = '<?=base_url('index/addasset');?>/';
    });
    $(document).on('click','.editbtn',function(){
      window.location = '<?=base_url('index/addasset');?>/'+$(this).data('no')+'/'+$(this).data('btype');
    });
</script>
<br>
<!-- <div class="component row">
		<div class="col-md-4 offset-2">
			<form id="money-form">
			<div class="input-group">
				<input class="form-control" name="money" placeholder="金額試算..">
				<div class="input-group-append">
					<button class="btn btn-outline-primary" type="submit">
						<i class="fas fa-search"></i>
					</button>
				</div>
			</div>
			</form>
    </div>
    <div class="col-md-2">
      <form id="renovate-form" method="post" action="http://localhost/user/renovate">
        <div class="btn-group">
          <button type="submit" class="btn btn-outline-danger" data-toggle="tooltip" title="" data-original-title="將所選擇的資產設為維修項目">
            <i class="fa fa-plus"></i>
          </button>
        </div>
      </form>
    </div>
    <div class="col-md-1">
      <ul class="list-group list-group-horizontal">
        <li class="list-group-item px-1 px-sm-3 py-1 my-auto">需要金額
          <span id="output-money" class="badge badge-light">0</span>
        </li>
      </ul>
    </div>
    <div class="col-md-1">
      <ul class="list-group list-group-horizontal">
        <li class="list-group-item px-1 px-sm-3 py-1 my-auto">選擇項目
          <span id="output-count" class="badge badge-light">0</span>
        </li>
      </ul>
    </div>
</div> -->
	<div class="content">
		<div class="container">
				<div class="col-md-12">
					<h3>資產列表</h3>
				</div>
      <div class="col-md-12">
          <div class="d-flex justify-content-end mb-2">
            <button type="button" class="btn btn-success addbtn" data-toggle="modal" title="新增" data-id="road"><i class="fa fa-plus"></i></button>
          </div>
      </div>
    </div>
  </div>
				
<div class="row">
  <div class="col-md-8 offset-2">
    <div class="card">
      <div class="card-header p-2">
        <ul class="nav nav-pills">
          <li class="nav-item"><a class="road_tabbtn nav-link active" data-type="road" href="#road" data-toggle="tab">道路</a></li>
          <li class="nav-item"><a class="bridge_tabbtn nav-link" data-type="bridge" href="#bridge" data-toggle="tab">橋梁</a></li>
          <li class="nav-item"><a class="light_tabbtn nav-link" data-type="light" href="#light" data-toggle="tab">路燈</a></li>
          <li class="nav-item"><a class="tree_tabbtn nav-link" data-type="tree" href="#tree" data-toggle="tab">路樹</a></li>
          <li class="nav-item"><a class="sidewalk_tabbtn nav-link" data-type="sidewalk" href="#sidewalk" data-toggle="tab">人行道</a></li>
          <li class="nav-item"><a class="groove_tabbtn nav-link" data-type="groove" href="#groove" data-toggle="tab">邊溝/側溝</a></li>
          <li class="nav-item"><a class="cable_tabbtn nav-link" data-type="cable" href="#cable" data-toggle="tab">天空纜線</a></li>
        </ul>
      </div><!-- /.card-header -->
      <div class="card-body">
        <div class="tab-content">
          <!-- 道路 -->
          <div class="tab-pane active " id="road">
              <table id="road_tables" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>編號</th>
                    <th>路名</th>
                    <th>道路層級</th>
                    <th>行政區</th>
                    <th>建置金額</th>
                    <th>維護金額</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>1</td>
                    <td>環西路二段</td>
                    <td>縣道</td>
                    <td>中壢區</td>
                    <td>100</td>
                    <td>100</td>
                    <td>
                      <div class="btn-group">
                        <button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-no="id" >
                          <span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>
                        <button type="button" class="btn btn-outline-info" data-toggle="map.position" data-lng="121.303" data-lat="25.0263">
                          <span data-toggle="tooltip" title="" data-original-title="查看地圖">
                            <i class="fa fa-map-marker-alt"></i>
                          </span>
                        </button>
                        <button type="button" class="btn btn-outline-info" data-toggle="map.polygon">
                          <article class="d-none">[[[121.3031870406291,25.026362330426437],[121.30261686301493,25.026167734114146],[121.30258222252878,25.026255018616162],[121.30254537888405,25.026340538955566],[121.30311591520399,25.026535258488344],[121.30314956650287,25.026449193798236],[121.3031870406291,25.026362330426437]]]</article>
                          <span data-toggle="tooltip" title="" data-original-title="查看地圖">
                            <i class="fa fa-map"></i>
                          </span>
                        </button>
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
          </div>
          <!-- 橋梁 -->
          <div class="tab-pane" id="bridge">
            <table id="bridge_tables" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                      <th>編號</th>
                      <th>名稱</th>
                      <th>河流</th>
                      <th>區</th>
                      <th>建置金額</th>
                      <th>維護金額</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                   
                  </tbody>
                </table>
          </div>
          <!-- 路燈 -->
          <div class="tab-pane" id="light">
            <table id="light_tables" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>編號</th>
                  <th>路燈編號</th>
                  <th>行政區</th>
                  <th>路名</th>
                  <th>建置金額</th>
                  <th>維護金額</th>
                  <th></th>
                </tr>
              </thead>
              <tbody>
                
              </tbody>
            </table>
          </div>
          <!-- 路樹 -->
          <div class="tab-pane" id="tree">
            <table id="tree_tables" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>編號</th>
                    <th>路樹編號</th>
                    <th>行政區</th>
                    <th>路名</th>
                    <th>品種</th>
                    <th>管理單位</th>
                    <th>建置金額</th>
                    <th>維護金額</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
          </div>
          <!-- 人行道 -->
          <div class="tab-pane" id="sidewalk">
            <table id="sidewalk_tables" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>編號</th>
                    <th>行政區</th>
                    <th>路名</th>
                    <th>道路起點</th>
                    <th>道路迄點</th>
                    <th>人行道淨寬(m)</th>
                    <th>鋪面類型</th>
                    <th>建置金額</th>
                    <th>維護金額</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
          </div>
          <!-- 邊溝/側溝 -->
          <div class="tab-pane" id="groove">
            <table id="groove_tables" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>編號</th>
                    <th>設施編號</th>
                    <th>公路編號</th>
                    <th>行政區</th>
                    <th>村里名</th>
                    <th>結構種類</th>
                    <th>建置金額</th>
                    <th>維護金額</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
          </div>
          <!-- 天空纜線 -->
          <div class="tab-pane" id="cable">
            <table id="cable_tables" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>編號</th>
                    <th>設施編號</th>
                    <th>行政區</th>
                    <th>道路名稱</th>
                    <th>纜線長度</th>
                    <th>建置金額</th>
                    <th>維護金額</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  
                </tbody>
              </table>
          </div>
        </div>
      </div><!-- /.card-body -->
    </div>
  </div>
</div>
