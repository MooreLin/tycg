	<script>
		 var lang = {
            "sProcessing": "處理中...",
            "sLengthMenu": "每頁 _MENU_ 項",
            "sZeroRecords": "沒有匹配結果",
            "sInfo": "當前顯示第 _START_ 至 _END_ 項，共 _TOTAL_ 項。",
            "sInfoEmpty": "當前顯示第 0 至 0 項，共 0 項",
            "sInfoFiltered": "(由 _MAX_ 項結果過濾)",
            "sInfoPostFix": "",
            "sSearch": "搜尋:",
            "sUrl": "",
            "sEmptyTable": "無資料",
            "sLoadingRecords": "載入中...",
            "sInfoThousands": ",",
            "oPaginate": {
            "sFirst": "首頁",
            "sPrevious": "上頁",
            "sNext": "下頁",
            "sLast": "末頁",
            "sJump": "跳轉"
          },
          "oAria": {
            "sSortAscending": ": 以升序排列此列",
            "sSortDescending": ": 以降序排列此列"
          }
        };
		
		$(function () {
			$('#road_tables').DataTable({
				language:lang, //提示資訊
				responsive: true,
			});
			$('#cable_tables').DataTable({
				language:lang, //提示資訊
			});
		});
		
		// addbtn
		$(document).on('click','.addbtn',function(){
			$('#modal-primary').modal('show');
			$('#modal-primary .modal-title').text('新增維修');

		});
		$(document).ready(function(){
			$('#renovate_class').select2({
				theme: "bootstrap4",
			});
		});
		var town_select_html = '<select class="form-control" id="asset_town" name="asset_town">';
		<?php foreach ($area_msg as $value) {?>
			town_select_html += '<option value="<?=$value['area_name']?>"><?=$value['area_name']?></option>';
		<?php }?>
		town_select_html +='</select>';
		// 橋梁btn
			$(document).on('click','.bridge_tabbtn',function(){
				var atype = $(this).data('type');
				$('.addbtn').data('id','bridge');
				$('#bridge_tables').DataTable(
					{
					language:lang, //提示資訊
					responsive: true,
					bDestroy: true,
					processing: true, //隱藏載入提示,自行處理
					serverSide: true, //啟用伺服器端分頁
					ajax: function(data,callback,setting){
						var param = {};
						param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
						param.start = data.start;//開始的記錄序號
						param.page = (data.start / data.length);//當前頁碼
						param.search = data.search.value;
						param.type = atype;
						$.post('<?=base_url('index/get_msg_renovate')?>',{param:param},function(result){
						setTimeout(function(){
							var returnData = {};
							returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
							returnData.recordsTotal = result.count;//返回資料全部記錄
							returnData.recordsFiltered = result.searchcount;//後臺不實現過濾功能，每次查詢均視作全部結果
							returnData.data = result.list;//返回的資料列表
							callback(returnData);
						},200);
						},'json');
					},
					columnDefs:[{
						render:function(data, type, row){
							htmlitem = '<div class="btn-group">';
							htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'">';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
							htmlitem += '<button type="button" class="btn btn-info detilebtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'" >';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="詳細"><i class="fa fa-info-circle" ></i></button>';
							htmlitem += '</div>';
						return htmlitem;
						},"targets": 6
					}]
					}
				);
				
			});
		// 路燈btn
			$(document).on('click','.light_tabbtn',function(){
				var atype = $(this).data('type');
				$('.addbtn').data('id','light');
				$('#light_tables').DataTable(
					{
					language:lang, //提示資訊
					bDestroy: true,
					processing: true, //隱藏載入提示,自行處理
					serverSide: true, //啟用伺服器端分頁
					ajax: function(data,callback,setting){
						var param = {};
						param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
						param.start = data.start;//開始的記錄序號
						param.page = (data.start / data.length);//當前頁碼
						param.search = data.search.value;
						param.type = atype;
						$.post('<?=base_url('index/get_msg')?>',{param:param},function(result){
						setTimeout(function(){
							var returnData = {};
							returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
							returnData.recordsTotal = result.count;//返回資料全部記錄
							returnData.recordsFiltered = result.count;//後臺不實現過濾功能，每次查詢均視作全部結果
							returnData.data = result.list;//返回的資料列表
							callback(returnData);
						},200);
						},'json');
					},
					columnDefs:[{
						render:function(data, type, row){
							htmlitem = '<div class="btn-group">';
							htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'">';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
							htmlitem += '<button type="button" class="btn btn-info detilebtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'" >';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="詳細"><i class="fa fa-info-circle" ></i></button>';
							htmlitem += '</div>';
						return htmlitem;
						},"targets": 5
					}]
					}
				);
			
			});

		// 路樹btn
			$(document).on('click','.tree_tabbtn',function(){
			
				var atype = $(this).data('type');
				$('.addbtn').data('id','tree');
				$('#tree_tables').DataTable(
					{
					language:lang, //提示資訊
					bDestroy: true,
					processing: true, //隱藏載入提示,自行處理
					serverSide: true, //啟用伺服器端分頁
					ajax: function(data,callback,setting){
						var param = {};
						param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
						param.start = data.start;//開始的記錄序號
						param.page = (data.start / data.length);//當前頁碼
						param.type = atype;
						param.search = data.search.value;
						$.post('<?=base_url('index/get_msg_renovate')?>',{param:param},function(result){
						setTimeout(function(){
							var returnData = {};
							returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
							returnData.recordsTotal = result.count;//返回資料全部記錄
							returnData.recordsFiltered = result.count;//後臺不實現過濾功能，每次查詢均視作全部結果
							returnData.data = result.list;//返回的資料列表
							callback(returnData);
						},200);
						},'json');
					},
					columnDefs:[{
						render:function(data, type, row){
							htmlitem = '<div class="btn-group">';
							htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'" >';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
							htmlitem += '<button type="button" class="btn btn-info detilebtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'" >';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="詳細"><i class="fa fa-info-circle" ></i></button>';
							htmlitem += '</div>';
						return htmlitem;
						},"targets": 7
					}]
					}
				);
			});

		
		// 人行道btn
			$(document).on('click','.sidewalk_tabbtn',function(){
				var atype = $(this).data('type');
				$('.addbtn').data('id','sidewalk');
				$('#sidewalk_tables').DataTable(
					{
					language:lang, //提示資訊
					bDestroy: true,
					processing: true, //隱藏載入提示,自行處理
					serverSide: true, //啟用伺服器端分頁
					ajax: function(data,callback,setting){
						var param = {};
						param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
						param.start = data.start;//開始的記錄序號
						param.page = (data.start / data.length);//當前頁碼
						param.type = atype;
						param.search = data.search.value;
						$.post('<?=base_url('index/get_msg_renovate')?>',{param:param},function(result){
						setTimeout(function(){
							var returnData = {};
							returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
							returnData.recordsTotal = result.count;//返回資料全部記錄
							returnData.recordsFiltered = result.count;//後臺不實現過濾功能，每次查詢均視作全部結果
							returnData.data = result.list;//返回的資料列表
							callback(returnData);
						},200);
						},'json');
					},
					columnDefs:[{
						render:function(data, type, row){
							htmlitem = '<div class="btn-group">';
							htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'" >';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
							htmlitem += '<button type="button" class="btn btn-info detilebtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'" >';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="詳細"><i class="fa fa-info-circle" ></i></button>';
							htmlitem += '</div>';
						return htmlitem;
						},"targets": 9
					}]
					}
				);
			});
		
		// 側溝btn
			$(document).on('click','.groove_tabbtn',function(){
				var atype = $(this).data('type');
				$('.addbtn').data('id','groove');
				$('#groove_tables').DataTable(
					{
					language:lang, //提示資訊
					bDestroy: true,
					processing: true, //隱藏載入提示,自行處理
					serverSide: true, //啟用伺服器端分頁
					ajax: function(data,callback,setting){
						var param = {};
						param.limit = data.length;//頁面顯示記錄條數，在頁面顯示每頁顯示多少項的時候
						param.start = data.start;//開始的記錄序號
						param.page = (data.start / data.length);//當前頁碼
						param.type = atype;
						param.search = data.search.value;
						$.post('<?=base_url('index/get_msg_renovate')?>',{param:param},function(result){
						setTimeout(function(){
							var returnData = {};
							returnData.draw = data.draw;//這裡直接自行返回了draw計數器,應該由後臺返回
							returnData.recordsTotal = result.count;//返回資料全部記錄
							returnData.recordsFiltered = result.searchcount;//後臺不實現過濾功能，每次查詢均視作全部結果
							returnData.data = result.list;//返回的資料列表
							callback(returnData);
						},200);
						},'json');
					},
					columnDefs:[{
						render:function(data, type, row){
							htmlitem = '<div class="btn-group">';
							htmlitem += '<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'" >';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>';
							htmlitem += '<button type="button" class="btn btn-info detilebtn" data-toggle="modal" title="" data-notype="'+atype+'" data-no="'+row[0]+'" >';
							htmlitem += '<span data-toggle="tooltip" title="" data-original-title="詳細"><i class="fa fa-info-circle" ></i></button>';
							htmlitem += '</div>';
						return htmlitem;
						},"targets": 7
					}]
					}
				);
			});
		// 詳細btn
			$(document).on('click','.detilebtn',function(){
				$('.modal_save_btn').hide();
				atype = $(this).data('notype');
				id = $(this).data('no');
				var htmlitem_body = '';
				$.post("<?=base_url('index/get_msg_for_id')?>",{atype:atype,id:id},function(json){
					switch (atype) {
						case 'road':
							break;
						case 'bridge':
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>名稱</th>";
								htmlitem_body +="<th>"+json['name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>河流</th>";
								htmlitem_body +="<th>"+json['river']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>區域</th>";
								htmlitem_body +="<th>"+json['bridge_town']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='40%;'>上部結構名稱/類別</th>";
								htmlitem_body +="<th>"+json['structure_t']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>下部結構名稱/類別</th>";
								htmlitem_body +="<th>"+json['structure_b']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>建置成本</th>";
								htmlitem_body +="<th>"+json['bridge_repair_cost']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>維護成本</th>";
								htmlitem_body +="<th>"+json['bridge_build_cost']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>DERU</th>";
								htmlitem_body +="<th>"+json['bridge_deru']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測時間</th>";
								htmlitem_body +="<th>"+json['bridge_test_date']+"</th>";
							htmlitem_body +="</tr>";
							break;
						case 'light':
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>設施編號</th>";
								htmlitem_body +="<th>"+json['light_numid']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>區域名稱</th>";
								htmlitem_body +="<th>"+json['light_town']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>里程</th>";
								htmlitem_body +="<th>"+json['light_m']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>公路名稱</th>";
								htmlitem_body +="<th>"+json['light_road_name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測日期</th>";
								htmlitem_body +="<th>"+json['light_test_date']+"</th>";
							htmlitem_body +="</tr>";
							break;
						case 'tree':
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>路樹編號</th>";
								htmlitem_body +="<th>"+json['tree_no']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>行政區</th>";
								htmlitem_body +="<th>"+json['tree_no']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>樹種</th>";
								htmlitem_body +="<th>"+json['tree_type_name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>樹胸徑</th>";
								htmlitem_body +="<th>"+json['tree_diameter']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>樹高</th>";
								htmlitem_body +="<th>"+json['tree_high']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>樹冠投影面積</th>";
								htmlitem_body +="<th>"+json['tree_crown']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>路段</th>";
								htmlitem_body +="<th>"+json['tree_road']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>生長狀況</th>";
								htmlitem_body +="<th>"+json['tree_growing_type']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>管理單位</th>";
								htmlitem_body +="<th>"+json['tree_manageunit']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測日期</th>";
								htmlitem_body +="<th>"+json['tree_test_date']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>URL</th>";
								htmlitem_body +="<th><a href='"+json['tree_url']+"' target='_blank' rel='noopener noreferrer'>連結</a></th>";
							htmlitem_body +="</tr>";
							break;
						case 'sidewalk':
							if(json['sw_road_direction'] == '1'){
								direction = '東/南';
							}else if(json['sw_road_direction'] == '2'){
								direction = '西/北';
							}else{
								direction = '徒步區';
							}
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>路名</th>";
								htmlitem_body +="<th>"+json['sw_road_name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道流水號</th>";
								htmlitem_body +="<th>"+json['sw_serial_num']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>行政區</th>";
								htmlitem_body +="<th>"+json['sw_town']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>FCI分數</th>";
								htmlitem_body +="<th>"+json['sidewalk_fci']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路起點</th>";
								htmlitem_body +="<th>"+json['sw_road_start']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路迄點</th>";
								htmlitem_body +="<th>"+json['sw_road_end']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路方向</th>";
								htmlitem_body +="<th>"+direction+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路長度</th>";
								htmlitem_body +="<th>"+json['sw_length']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路寬度(包含人行道)</th>";
								htmlitem_body +="<th>"+json['sw_width_u']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路寬度(不含人行道)</th>";
								htmlitem_body +="<th>"+json['sw_width_c']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道長度</th>";
								htmlitem_body +="<th>"+json['sw_leng']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道總寬度</th>";
								htmlitem_body +="<th>"+json['sw_wth']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道淨寬</th>";
								htmlitem_body +="<th>"+json['sww_wth']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>鋪面類型</th>";
								htmlitem_body +="<th>"+json['sw_pave']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道面積</th>";
								htmlitem_body +="<th>"+json['shape_ar']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道上設施物所占面積</th>";
								htmlitem_body +="<th>"+json['sw_area']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定桿類設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_l']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定箱類設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_b']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定出入口設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_e']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定停車設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_p']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道其他固定設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_o']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道人手孔數量</th>";
								htmlitem_body +="<th>"+json['sw_hole']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道樹穴數量</th>";
								htmlitem_body +="<th>"+json['sw_tree']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>路緣斜坡數量</th>";
								htmlitem_body +="<th>"+json['sw_ramp']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>橫越人行道之穿越道數量</th>";
								htmlitem_body +="<th>"+json['sw_carramp']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定阻礙物名稱</th>";
								htmlitem_body +="<th>"+json['sw_blok_name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測日期</th>";
								htmlitem_body +="<th>"+json['sidwealk_test_date']+"</th>";
							htmlitem_body +="</tr>";
							break;
						case 'groove':
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>設施編號</th>";
								htmlitem_body +="<th>"+json['groove_numid']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>區域名稱</th>";
								htmlitem_body +="<th>"+json['groove_town']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路名稱</th>";
								htmlitem_body +="<th>"+json['groove_road']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>邊溝長度</th>";
								htmlitem_body +="<th>"+json['groove_length']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>邊溝寬度</th>";
								htmlitem_body +="<th>"+json['groove_width']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>邊溝位置</th>";
								htmlitem_body +="<th>"+json['groove_direction']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>邊溝材料</th>";
								htmlitem_body +="<th>"+json['groove_type']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>村里名</th>";
								htmlitem_body +="<th>"+json['groove_village']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>里程</th>";
								htmlitem_body +="<th>"+json['groove_m']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測日期</th>";
								htmlitem_body +="<th>"+json['groove_test_date']+"</th>";
							htmlitem_body +="</tr>";
							break;
						case 'cable':
							break;
						default:
							break;
					}
					
					$('#modal-data-body').html(htmlitem_body);
				},'json');
				$('#modal_msg').modal('show');
			});

		// 修改btn
			$(document).on('click','.editbtn',function(){
				$('.modal_save_btn').show();
				atype = $(this).data('notype');
				id = $(this).data('no');
				$('.modal-title').text('修改資料');
				var htmlitem_body = '';
				$.post("<?=base_url('index/get_msg_for_id')?>",{atype:atype,id:id},function(json){
					switch (atype) {
						case 'road':
							break;
						case 'bridge':
							htmlitem_body +="<input type='hidden' name='id' id='id' value='"+json['bridge_id']+"'>";
							htmlitem_body +="<input type='hidden' name='list_type' id='list_type' value='bridge'>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>名稱</th>";
								htmlitem_body +="<th>"+json['name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>河流</th>";
								htmlitem_body +="<th>"+json['river']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>區域</th>";
								htmlitem_body +="<th>"+json['bridge_town']+"</th>";
								// htmlitem_body += "<th>"+town_select_html+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='40%;'>上部結構名稱/類別</th>";
								htmlitem_body +="<th>"+json['structure_t']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>下部結構名稱/類別</th>";
								htmlitem_body +="<th>"+json['structure_b']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>建置成本</th>";
								htmlitem_body +="<th>"+json['bridge_repair_cost']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>維護成本</th>";
								htmlitem_body +="<th>"+json['bridge_build_cost']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次DERU</th>";
								htmlitem_body +="<th>"+json['bridge_deru']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>DERU</th>";
								htmlitem_body +="<th><input type='text' class='form-control' name='bridge_deru'></input></th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測時間</th>";
								htmlitem_body +="<th>"+json['bridge_test_date']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>檢測時間</th>";
								htmlitem_body +="<th><input type='date' class='form-control' name='bridge_test_date'></input></th>";
							htmlitem_body +="</tr>";
							break;
						case 'light':
							htmlitem_body +="<input type='hidden' name='id' id='id' value='"+json['light_id']+"'>";
							htmlitem_body +="<input type='hidden' name='list_type' id='list_type' value='light'>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>設施編號</th>";
								htmlitem_body +="<th>"+json['light_numid']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>區域名稱</th>";
								htmlitem_body +="<th>"+json['light_town']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>里程</th>";
								htmlitem_body +="<th>"+json['light_m']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>公路名稱</th>";
								htmlitem_body +="<th>"+json['light_road_name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測日期</th>";
								htmlitem_body +="<th>"+json['light_test_date']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>檢測日期</th>";
								htmlitem_body +="<th><input type='date' class='form-control' name='light_test_date'></input></th>";
							htmlitem_body +="</tr>";
							break;
						case 'tree':
							htmlitem_body +="<input type='hidden' name='id' id='id' value='"+json['tree_id']+"'>";
							htmlitem_body +="<input type='hidden' name='list_type' id='list_type' value='tree'>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>路樹編號</th>";
								htmlitem_body +="<th>"+json['tree_no']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>行政區</th>";
								htmlitem_body +="<th>"+json['tree_no']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>樹種</th>";
								htmlitem_body +="<th>"+json['tree_type_name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>樹胸徑</th>";
								htmlitem_body +="<th>"+json['tree_diameter']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>樹高</th>";
								htmlitem_body +="<th>"+json['tree_high']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>樹冠投影面積</th>";
								htmlitem_body +="<th>"+json['tree_crown']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>路段</th>";
								htmlitem_body +="<th>"+json['tree_road']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>生長狀況</th>";
								htmlitem_body +="<th>"+json['tree_growing_type']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>管理單位</th>";
								htmlitem_body +="<th>"+json['tree_manageunit']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測日期</th>";
								htmlitem_body +="<th>"+json['tree_test_date']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>檢測日期</th>";
								htmlitem_body +="<th><input type='date' class='form-control' name='tree_test_date'></input></th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>URL</th>";
								htmlitem_body +="<th><a href='"+json['tree_url']+"' target='_blank' rel='noopener noreferrer'>連結</a></th>";
							htmlitem_body +="</tr>";
							break;
						case 'sidewalk':
							if(json['sw_road_direction'] == '1'){
								direction = '東/南';
							}else if(json['sw_road_direction'] == '2'){
								direction = '西/北';
							}else{
								direction = '徒步區';
							}
							htmlitem_body +="<input type='hidden' name='id' id='id' value='"+json['sidewalk_id']+"'>";
							htmlitem_body +="<input type='hidden' name='list_type' id='list_type' value='sidewalk'>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>路名</th>";
								htmlitem_body +="<th>"+json['sw_road_name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道流水號</th>";
								htmlitem_body +="<th>"+json['sw_serial_num']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>行政區</th>";
								htmlitem_body +="<th>"+json['sw_town']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>FCI分數</th>";
								htmlitem_body +="<th>"+json['sidewalk_fci']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路起點</th>";
								htmlitem_body +="<th>"+json['sw_road_start']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路迄點</th>";
								htmlitem_body +="<th>"+json['sw_road_end']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路方向</th>";
								htmlitem_body +="<th>"+direction+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路長度</th>";
								htmlitem_body +="<th>"+json['sw_length']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路寬度(包含人行道)</th>";
								htmlitem_body +="<th>"+json['sw_width_u']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路寬度(不含人行道)</th>";
								htmlitem_body +="<th>"+json['sw_width_c']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道長度</th>";
								htmlitem_body +="<th>"+json['sw_leng']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道總寬度</th>";
								htmlitem_body +="<th>"+json['sw_wth']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道淨寬</th>";
								htmlitem_body +="<th>"+json['sww_wth']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>鋪面類型</th>";
								htmlitem_body +="<th>"+json['sw_pave']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道面積</th>";
								htmlitem_body +="<th>"+json['shape_ar']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道上設施物所占面積</th>";
								htmlitem_body +="<th>"+json['sw_area']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定桿類設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_l']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定箱類設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_b']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定出入口設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_e']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定停車設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_p']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道其他固定設施數量</th>";
								htmlitem_body +="<th>"+json['sw_bk_o']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道人手孔數量</th>";
								htmlitem_body +="<th>"+json['sw_hole']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道樹穴數量</th>";
								htmlitem_body +="<th>"+json['sw_tree']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>路緣斜坡數量</th>";
								htmlitem_body +="<th>"+json['sw_ramp']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>橫越人行道之穿越道數量</th>";
								htmlitem_body +="<th>"+json['sw_carramp']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>人行道固定阻礙物名稱</th>";
								htmlitem_body +="<th>"+json['sw_blok_name']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次FCI</th>";
								htmlitem_body +="<th>"+json['sidwealk_fci']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>FCI</th>";
								htmlitem_body +="<th><input type='text' class='form-control' name='sidwealk_fci'></input></th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測日期</th>";
								htmlitem_body +="<th>"+json['sidwealk_test_date']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>檢測日期</th>";
								htmlitem_body +="<th><input type='date' class='form-control' name='sidwealk_test_date'></input></th>";
							htmlitem_body +="</tr>";
							break;
						case 'groove':
							htmlitem_body +="<input type='hidden' name='id' id='id' value='"+json['groove_id']+"'>";
							htmlitem_body +="<input type='hidden' name='list_type' id='list_type' value='groove'>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>設施編號</th>";
								htmlitem_body +="<th>"+json['groove_numid']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>區域名稱</th>";
								htmlitem_body +="<th>"+json['groove_town']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>道路名稱</th>";
								htmlitem_body +="<th>"+json['groove_road']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>邊溝長度</th>";
								htmlitem_body +="<th>"+json['groove_length']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>邊溝寬度</th>";
								htmlitem_body +="<th>"+json['groove_width']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>邊溝位置</th>";
								htmlitem_body +="<th>"+json['groove_direction']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>邊溝材料</th>";
								htmlitem_body +="<th>"+json['groove_type']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>村里名</th>";
								htmlitem_body +="<th>"+json['groove_village']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>里程</th>";
								htmlitem_body +="<th>"+json['groove_m']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>上次檢測日期</th>";
								htmlitem_body +="<th>"+json['groove_test_date']+"</th>";
							htmlitem_body +="</tr>";
							htmlitem_body +="<tr>";
								htmlitem_body +="<th width='20%;'>檢測日期</th>";
								htmlitem_body +="<th><input type='date' class='form-control' name='groove_test_date'></input></th>";
							htmlitem_body +="</tr>";
							break;
						case 'cable':
							break;
						default:
							break;
					}
					
					$('#modal-data-body').html(htmlitem_body);
				},'json');
				$('#modal_msg').modal('show');
			});
			$(document).on('click','.modal_save_btn',function(){
				$.ajax({
					url: "<?=base_url('index/edit_renovate')?>",
					data: $('#groupform').serialize(),
					type:"POST",
					error:function(xhr, ajaxOptions, thrownError){ 
						console.log(thrownError)
						alert(xhr.responseText);
					},
					success:function(json){
						if(json == true)
						{
							Swal.fire({
									title: '修改成功',
									icon: 'success',
								}).then(function(){
									location.reload();
								});
							
						}else{
							Swal.fire('修改時發生錯誤','','error');
							return false;
						}
					}
				});

				return false;
			});
	</script>
	
	<!-- 詳細modal -->
	<div class="content">
		<div class="container">
			<div class="row">
			<form class="form-signin" id="groupform" method="post" role="form">
				<div class="modal fade " id="modal_msg" style="display: none;" data-backdrop="static">
					<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title"></h4>
							<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">×</span></button>
						</div>
						<div class="modal-body">
							<table class="table table-bordered table-striped">
								<thead id="modal-data-body">
								</thead>
							</table>
						</div>
						<div class="modal-footer">
						
							<button type="submit" class="btn btn-primary pull-right modal_save_btn" data-dismiss="modal">儲存</button>
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						</div>
					</div>
					<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
			</form>
				<div class="col-md-12">
				<br>
					<h3>工程履歷</h3>
					<hr>
				</div>
			</div>
		</div>
	</div>
	
			
	<div class="content">
		<div class="row">
			<div class="col-md-8 offset-2">
				<div class="card">
				<div class="card-header p-2">
					<ul class="nav nav-pills">
					<li class="nav-item"><a class="road_tabbtn nav-link active" data-type="road" href="#road" data-toggle="tab">道路</a></li>
					<li class="nav-item"><a class="bridge_tabbtn nav-link" data-type="bridge" href="#bridge" data-toggle="tab">橋梁</a></li>
					<li class="nav-item"><a class="light_tabbtn nav-link" data-type="light" href="#light" data-toggle="tab">路燈</a></li>
					<li class="nav-item"><a class="tree_tabbtn nav-link" data-type="tree" href="#tree" data-toggle="tab">路樹</a></li>
					<li class="nav-item"><a class="sidewalk_tabbtn nav-link" data-type="sidewalk" href="#sidewalk" data-toggle="tab">人行道</a></li>
					<li class="nav-item"><a class="groove_tabbtn nav-link" data-type="groove" href="#groove" data-toggle="tab">邊溝/側溝</a></li>
					<li class="nav-item"><a class="cable_tabbtn nav-link" data-type="cable" href="#cable" data-toggle="tab">天空纜線</a></li>
					</ul>
				</div><!-- /.card-header -->
				<div class="card-body">
					<div class="tab-content">
					<!-- 道路 -->
					<div class="tab-pane active " id="road">
						<table id="road_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>路名</th>
								<th>道路層級</th>
								<th>行政區</th>
								<th>IRI</th>
								<th>PCI</th>
								<th>SN</th>
								<th>上次檢測時間</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1</td>
								<td>環西路二段</td>
								<td>縣道</td>
								<td>中壢區</td>
								<td>100</td>
								<td>100</td>
								<td>100</td>
								<td>2020/12/25</td>
								<td>
								<div class="btn-group">
									<button type="button" class="btn btn-success editbtn" data-toggle="modal" title="" data-no="id" >
									<span data-toggle="tooltip" title="" data-original-title="編輯"><i class="fa fa-pen" ></i></button>
									<button type="button" class="btn btn-info detilebtn" data-toggle="modal" title="" data-notype="" data-no="" >
									<span data-toggle="tooltip" title="" data-original-title="詳細"><i class="fa fa-info-circle" ></i></button>
								</div>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!-- 橋梁 -->
					<div class="tab-pane" id="bridge">
						<table id="bridge_tables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>編號</th>
									<th>名稱</th>
									<th>河流</th>
									<th>區</th>
									<th>DERU</th>
									<th>上次檢測時間</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							
							</tbody>
							</table>
					</div>
					<!-- 路燈 -->
					<div class="tab-pane" id="light">
						<table id="light_tables" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>編號</th>
								<th>路燈編號</th>
								<th>行政區</th>
								<th>路名</th>
								<th>上次檢測日期</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							
						</tbody>
						</table>
					</div>
					<!-- 路樹 -->
					<div class="tab-pane" id="tree">
						<table id="tree_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>路樹編號</th>
								<th>行政區</th>
								<th>路名</th>
								<th>品種</th>
								<th>管理單位</th>
								<th>上次檢測日期</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
					</div>
					<!-- 人行道 -->
					<div class="tab-pane" id="sidewalk">
						<table id="sidewalk_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>行政區</th>
								<th>路名</th>
								<th>道路起點</th>
								<th>道路迄點</th>
								<th>人行道淨寬(m)</th>
								<th>鋪面類型</th>
								<th>FCI</th>
								<th>上次檢測日期</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
					</div>
					<!-- 邊溝/側溝 -->
					<div class="tab-pane" id="groove">
						<table id="groove_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>設施編號</th>
								<th>公路編號</th>
								<th>行政區</th>
								<th>村里名</th>
								<th>結構種類</th>
								<th>上次檢測日期</th>
								<th></th>
							</tr>
							</thead>
							<tbody>

							</tbody>
						</table>
					</div>
					<!-- 天空纜線 -->
					<div class="tab-pane" id="cable">
						<table id="cable_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>設施編號</th>
								<th>行政區</th>
								<th>道路名稱</th>
								<th>纜線長度</th>
								<th>建置金額</th>
								<th>維護金額</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							
							</tbody>
						</table>
					</div>
					</div>
				</div><!-- /.card-body -->
				</div>
			</div>
		</div>
	</div>
	