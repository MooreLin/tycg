	<script>
		var lang = {
            "sProcessing": "處理中...",
            "sLengthMenu": "每頁 _MENU_ 項",
            "sZeroRecords": "沒有匹配結果",
            "sInfo": "當前顯示第 _START_ 至 _END_ 項，共 _TOTAL_ 項。",
            "sInfoEmpty": "當前顯示第 0 至 0 項，共 0 項",
            "sInfoFiltered": "(由 _MAX_ 項結果過濾)",
            "sInfoPostFix": "",
            "sSearch": "搜尋:",
            "sUrl": "",
            "sEmptyTable": "無資料",
            "sLoadingRecords": "載入中...",
            "sInfoThousands": ",",
            "oPaginate": {
            "sFirst": "首頁",
            "sPrevious": "上頁",
            "sNext": "下頁",
            "sLast": "末頁",
            "sJump": "跳轉"
          },
          "oAria": {
            "sSortAscending": ": 以升序排列此列",
            "sSortDescending": ": 以降序排列此列"
          }
        };
		var alljsonlist = new Array();
		var se_type ='';
		$(function () {
			// $('#bridge_tables').DataTable();
			
			$('#renovate_class').select2({
				theme: "bootstrap4",
			});
		});
		// 橋梁
		$(document).on('click','.bridge_tabbtn',function(){
			var type = $(this).data('type');
			var item_html = '';
			var checkone_money = 0;
			var checkone_count = 0;
			$.post('<?=base_url('index/get_budget_msg')?>',{type:type},function(json){
				
				json = JSON.parse(json);
				checkone_count = Object.keys(json).length;
				for(i in json){
					checkone_money =  checkone_money +parseInt(json[i]['bridge_repair_cost']);
					item_html +='<tr>';
					item_html +='<td>'+json[i]['bridge_id']+'</td>';
					item_html +='<td>'+json[i]['name']+'</td>';
					item_html +='<td>'+json[i]['river']+'</td>';
					item_html +='<td>'+json[i]['bridge_town']+'</td>';
					item_html +='<td>'+json[i]['bridge_build_cost']+'</td>';
					item_html +='<td>'+json[i]['bridge_repair_cost']+'</td>';
					item_html +='<td>';
					item_html += '<button type="button" class="btn btn-danger delbtn" data-toggle="modal" title="" data-notype="bridge" data-no="'+json[i]['bridge_id']+'">';
					item_html += '<span data-toggle="tooltip" title="" data-original-title="刪除"><i class="fas fa-trash-alt" ></i></button></td>';
					item_html +='</tr>';
				}
				$('#bridge_tables #bridge_tbody').html(item_html);
				datatable = $('#bridge_tables').DataTable({
					'bDestroy': true,
					'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
					'columnDefs': [{
						'targets': [0],
						'searchable': false,
						'orderable': false
					}],
					"language": {
						"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
					}
				});
				$('#checkmoney').text(checkone_money);
				$('#checknum').text(checkone_count);
			});
		});
		// 路燈
		$(document).on('click','.light_tabbtn',function(){
			var type = $(this).data('type');
			var item_html = '';
			var checkone_money = 0;
			var checkone_count = 0;
			$.post('<?=base_url('index/get_budget_msg')?>',{type:type},function(json){
				
				json = JSON.parse(json);
				checkone_count = Object.keys(json).length;
				for(i in json){
					checkone_money =  checkone_money +parseInt(json[i]['bridge_repair_cost']);
					item_html +='<tr>';
					item_html +='<td>'+json[i]['light_id']+'</td>';
					item_html +='<td>'+json[i]['light_numid']+'</td>';
					item_html +='<td>'+json[i]['light_town']+'</td>';
					item_html +='<td>'+json[i]['light_road_name']+'</td>';
					item_html +='<td>'+json[i]['light_build_cost']+'</td>';
					item_html +='<td>'+json[i]['light_repair_cost']+'</td>';
					item_html +='<td>';
					item_html += '<button type="button" class="btn btn-danger delbtn" data-toggle="modal" title="" data-notype="light" data-no="'+json[i]['light_id']+'">';
					item_html += '<span data-toggle="tooltip" title="" data-original-title="刪除"><i class="fas fa-trash-alt" ></i></button></td>';
					item_html +='</tr>';
				}
				$('#light_tables #light_tbody').html(item_html);
				datatable = $('#light_tables').DataTable({
					'bDestroy': true,
					'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
					'columnDefs': [{
						'targets': [0],
						'searchable': false,
						'orderable': false
					}],
					"language": {
						"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
					}
				});
				$('#checkmoney').text(checkone_money);
				$('#checknum').text(checkone_count);
			});
		});
		// 路樹
		$(document).on('click','.tree_tabbtn',function(){
			var type = $(this).data('type');
			var item_html = '';
			var checkone_money = 0;
			var checkone_count = 0;
			$.post('<?=base_url('index/get_budget_msg')?>',{type:type},function(json){
				
				json = JSON.parse(json);
				checkone_count = Object.keys(json).length;
				for(i in json){
					checkone_money =  checkone_money +parseInt(json[i]['bridge_repair_cost']);
					item_html +='<tr>';
					item_html +='<td>'+json[i]['tree_id']+'</td>';
					item_html +='<td>'+json[i]['tree_no']+'</td>';
					item_html +='<td>'+json[i]['tree_town']+'</td>';
					item_html +='<td>'+json[i]['tree_road']+'</td>';
					item_html +='<td>'+json[i]['tree_type_name']+'</td>';
					item_html +='<td>'+json[i]['tree_manageunit']+'</td>';
					item_html +='<td>'+json[i]['tree_build_cost']+'</td>';
					item_html +='<td>'+json[i]['tree_repair_cost']+'</td>';
					item_html +='<td>';
					item_html += '<button type="button" class="btn btn-danger delbtn" data-toggle="modal" title="" data-notype="tree" data-no="'+json[i]['tree_id']+'">';
					item_html += '<span data-toggle="tooltip" title="" data-original-title="刪除"><i class="fas fa-trash-alt" ></i></button></td>';
					item_html +='</tr>';
				}
				$('#tree_tables #tree_tbody').html(item_html);
				datatable = $('#tree_tables').DataTable({
					'bDestroy': true,
					'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
					'columnDefs': [{
						'targets': [0],
						'searchable': false,
						'orderable': false
					}],
					"language": {
						"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
					}
				});
				$('#checkmoney').text(checkone_money);
				$('#checknum').text(checkone_count);
			});
		});
		// 人行道
		$(document).on('click','.sidewalk_tabbtn',function(){
			var type = $(this).data('type');
			var item_html = '';
			var checkone_money = 0;
			var checkone_count = 0;
			$.post('<?=base_url('index/get_budget_msg')?>',{type:type},function(json){
				
				json = JSON.parse(json);
				checkone_count = Object.keys(json).length;
				for(i in json){
					checkone_money =  checkone_money +parseInt(json[i]['bridge_repair_cost']);
					item_html +='<tr>';
					item_html +='<td>'+json[i]['sidewalk_id']+'</td>';
					item_html +='<td>'+json[i]['sw_town']+'</td>';
					item_html +='<td>'+json[i]['sw_road_name']+'</td>';
					item_html +='<td>'+json[i]['sw_road_start']+'</td>';
					item_html +='<td>'+json[i]['sw_road_end']+'</td>';
					item_html +='<td>'+json[i]['sww_wth']+'</td>';
					item_html +='<td>'+json[i]['sw_pave']+'</td>';
					item_html +='<td>'+json[i]['sidewalk_build_cost']+'</td>';
					item_html +='<td>'+json[i]['sidewalk_repair_cost']+'</td>';
					item_html +='<td>';
					item_html += '<button type="button" class="btn btn-danger delbtn" data-toggle="modal" title="" data-notype="sidewalk" data-no="'+json[i]['sidewalk_id']+'">';
					item_html += '<span data-toggle="tooltip" title="" data-original-title="刪除"><i class="fas fa-trash-alt" ></i></button></td>';
					item_html +='</tr>';
				}
				$('#sidewalk_tables #sidewalk_tbody').html(item_html);
				datatable = $('#sidewalk_tables').DataTable({
					'bDestroy': true,
					'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
					'columnDefs': [{
						'targets': [0],
						'searchable': false,
						'orderable': false
					}],
					"language": {
						"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
					}
				});
				$('#checkmoney').text(checkone_money);
				$('#checknum').text(checkone_count);
			});
		});
		// 側溝
		$(document).on('click','.groove_tabbtn',function(){
			var type = $(this).data('type');
			var item_html = '';
			var checkone_money = 0;
			var checkone_count = 0;
			$.post('<?=base_url('index/get_budget_msg')?>',{type:type},function(json){
				
				json = JSON.parse(json);
				checkone_count = Object.keys(json).length;
				for(i in json){
					checkone_money =  checkone_money +parseInt(json[i]['bridge_repair_cost']);
					item_html +='<tr>';
					item_html +='<td>'+json[i]['groove_id']+'</td>';
					item_html +='<td>'+json[i]['groove_numid']+'</td>';
					item_html +='<td>'+json[i]['groove_road']+'</td>';
					item_html +='<td>'+json[i]['groove_town']+'</td>';
					item_html +='<td>'+json[i]['groove_village']+'</td>';
					item_html +='<td>'+json[i]['groove_type']+'</td>';
					item_html +='<td>'+json[i]['groove_build_cost']+'</td>';
					item_html +='<td>'+json[i]['groove_repair_cost']+'</td>';
					item_html +='<td>';
					item_html += '<button type="button" class="btn btn-danger delbtn" data-toggle="modal" title="" data-notype="groove" data-no="'+json[i]['groove_id']+'">';
					item_html += '<span data-toggle="tooltip" title="" data-original-title="刪除"><i class="fas fa-trash-alt" ></i></button></td>';
					item_html +='</tr>';
				}
				$('#groove_tables #groove_tbody').html(item_html);
				datatable = $('#groove_tables').DataTable({
					'bDestroy': true,
					'lengthMenu': [[10, 25, 50, 100, 300, 600, -1], [10, 25, 50, 100, 300, 600, "All"]],
					'columnDefs': [{
						'targets': [0],
						'searchable': false,
						'orderable': false
					}],
					"language": {
						"url": "<?=base_url()?>/vendor/datatables/Chinese-traditional.json"
					}
				});
				$('#checkmoney').text(checkone_money);
				$('#checknum').text(checkone_count);
			});
		});
		// 天空纜線

		// 刪除按鈕
		$(document).on('click','.delbtn',function(){
			var id = $(this).data('no');
			var notype = $(this).data('notype');
			$.post('<?=base_url('index/delete_money_detailed')?>',{id:id,notype:notype},function(json){
				if(json == true){
					// success
					Swal.fire({title: "刪除成功",icon: "success"}).then(function(value){
						window.location.reload();;
					});
				}else{
					// error
					Swal.fire({title: "刪除時發生錯誤",icon: "warning"});
					return false;
				}
			});
		});
	</script>
	
	<br>
	
	<div class="content">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h3>預算列表</h3>
					<div class="button-group">
						<button type="submit" class="btn btn-default" style="float:right;" onclick="history.back()">上一頁</button>
					</div>
					
					<br>
					<hr>
				</div>
				<div class="col-md-3">
					<div class="card card-primary">
						<div class="card-body" style="display:flex; align-items:center;min-height: 290px;height:290px;max-height:290px;">
							<h2>已建立<?=$budget_msg->num_rows();?>/7</h2>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card card-primary">
						<div class="card-body" style="display:flex; align-items:center;min-height: 290px;height:290px;max-height:290px;">
							<h2>預算總金額<br><?=$allmoney;?>元</h2>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="card card-primary">
						<div class="card-body">
						<div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand">
							<div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
							<canvas id="donutChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block; width: 764px;" width="764" height="250" class="chartjs-render-monitor"></canvas>		
						</div>
					</div>
				</div>				
			</div>
		</div>
	</div>
	
	<div class="container">
			<div class="row">
				<div class="col-md-3 offset-3">
					<div class="card">
						<div class="card-body">
							<p>已選取<span style="color:red" id="checknum" name="checknum"></span>項</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card">
						<div class="card-body">
							<p>已選取金額<span style="color:red" id="checkmoney" name="checkmoney"></span>萬</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	<div class="content">
		<div class="container">
							
		<div class="row">
			<div class="col-md-12">
				<div class="card">
				<div class="card-header p-2">
					<ul class="nav nav-pills">
					<li class="nav-item"><a class="road_tabbtn nav-link active" data-type="road" href="#road" data-toggle="tab">道路</a></li>
					<li class="nav-item"><a class="bridge_tabbtn nav-link" data-type="bridge" href="#bridge" data-toggle="tab">橋梁</a></li>
					<li class="nav-item"><a class="light_tabbtn nav-link" data-type="light" href="#light" data-toggle="tab">路燈</a></li>
					<li class="nav-item"><a class="tree_tabbtn nav-link" data-type="tree" href="#tree" data-toggle="tab">路樹</a></li>
					<li class="nav-item"><a class="sidewalk_tabbtn nav-link" data-type="sidewalk" href="#sidewalk" data-toggle="tab">人行道</a></li>
					<li class="nav-item"><a class="groove_tabbtn nav-link" data-type="groove" href="#groove" data-toggle="tab">邊溝/側溝</a></li>
					<li class="nav-item"><a class="cable_tabbtn nav-link" data-type="cable" href="#cable" data-toggle="tab">天空纜線</a></li>
					</ul>
				</div><!-- /.card-header -->
				<div class="card-body">
					<div class="tab-content">
					<!-- 道路 -->
					<div class="tab-pane active " id="road">
						<table id="road_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>路名</th>
								<th>道路層級</th>
								<th>行政區</th>
								<th>建置金額</th>
								<th>維護金額</th>
								<th></th>
							</tr>
							</thead>
							<tbody>
							<tr>
								<td>1</td>
								<td>環西路二段</td>
								<td>縣道</td>
								<td>中壢區</td>
								<td>100</td>
								<td>100</td>
								<td>
									<button type="button" class="btn btn-danger delbtn" data-toggle="modal" title="" data-no="'+json[i]['bridge_id']+'">
									<span data-toggle="tooltip" title="" data-original-title="刪除"><i class="fas fa-trash-alt" ></i></button>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
					<!-- 橋梁 -->
					<div class="tab-pane" id="bridge">
						<table id="bridge_tables" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>編號</th>
									<th>名稱</th>
									<th>河流</th>
									<th>區</th>
									<th>建置金額</th>
									<th>維護金額</th>
									<th></th>
								</tr>
							</thead>
							<tbody id="bridge_tbody">
							
							</tbody>
							</table>
					</div>
					<!-- 路燈 -->
					<div class="tab-pane" id="light">
						<table id="light_tables" class="table table-bordered table-striped">
						<thead>
							<tr>
							<th>編號</th>
							<th>路燈編號</th>
							<th>行政區</th>
							<th>路名</th>
							<th>建置金額</th>
							<th>維護金額</th>
							<th></th>
							</tr>
						</thead>
						<tbody id="light_tbody">
							
						</tbody>
						</table>
					</div>
					<!-- 路樹 -->
					<div class="tab-pane" id="tree">
						<table id="tree_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>路樹編號</th>
								<th>行政區</th>
								<th>路名</th>
								<th>品種</th>
								<th>管理單位</th>
								<th>建置金額</th>
								<th>維護金額</th>
								<th></th>
							</tr>
							</thead>
							<tbody id="tree_tbody">
							
							</tbody>
						</table>
					</div>
					<!-- 人行道 -->
					<div class="tab-pane" id="sidewalk">
						<table id="sidewalk_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>行政區</th>
								<th>路名</th>
								<th>道路起點</th>
								<th>道路迄點</th>
								<th>人行道淨寬(m)</th>
								<th>鋪面類型</th>
								<th>建置金額</th>
								<th>維護金額</th>
								<th></th>
							</tr>
							</thead>
							<tbody id="sidewalk_tbody">
							</tbody>
						</table>
					</div>
					<!-- 邊溝/側溝 -->
					<div class="tab-pane" id="groove">
						<table id="groove_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>設施編號</th>
								<th>公路編號</th>
								<th>行政區</th>
								<th>村里名</th>
								<th>結構種類</th>
								<th>建置金額</th>
								<th>維護金額</th>
								<th></th>
							</tr>
							</thead>
							<tbody id="groove_tbody">

							</tbody>
						</table>
					</div>
					<!-- 天空纜線 -->
					<div class="tab-pane" id="cable">
						<table id="cable_tables" class="table table-bordered table-striped">
							<thead>
							<tr>
								<th>編號</th>
								<th>設施編號</th>
								<th>行政區</th>
								<th>道路名稱</th>
								<th>纜線長度</th>
								<th>建置金額</th>
								<th>維護金額</th>
								<th></th>
							</tr>
							</thead>
							<tbody id="cable_tbody">
							
							</tbody>
						</table>
					</div>
					</div>
				</div><!-- /.card-body -->
				</div>
			</div>
			</div>
		</div>
	<script>
		//-------------
		//- DONUT CHART -
		//-------------
		// Get context with jQuery - using jQuery's .get() method.

		var labels = [];
		var data = [];
		var tjsonfile = <?=json_encode($budget_msg->result_array());?>;
		var jsonfile = JSON.stringify(tjsonfile);
		labels = $.map(JSON.parse(jsonfile),function(item,index){
			return item.budget_type_cn;
		});
		data = $.map(JSON.parse(jsonfile),function(item,index){
			return item.budget_money;
		});
		// labels = jsonfile.jsonarray.map(function(e){
		// 	return e.budget_type;
		// });
		// data = jsonfile.jsonarray.map(function(e){
		// 	return e.budget_money;
		// });
		var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
		var donutData        = {
				labels:labels,
				datasets:[{
					data:data,
					backgroundColor:['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de','#BE77FF']
				}]
			// labels: [
			// 	'道路', 
			// 	'橋梁',
			// 	'路燈', 
			// 	'路樹', 
			// 	'人行道', 
			// 	'邊溝', 
			// 	'天空纜線', 
			// ],
			// datasets: [
			// 	{
			// 	data: [700,500,400,600,300,100,200],
			// 	backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de','#BE77FF'],
			// 	}
			// ]
		}
		var donutOptions     = {
			maintainAspectRatio : false,
			responsive : true,
		}
		//Create pie or douhnut chart
		// You can switch between pie and douhnut using the method below.
		var donutChart = new Chart(donutChartCanvas, {
			type: 'doughnut',
			data: donutData,
			options: donutOptions      
		})

	</script>