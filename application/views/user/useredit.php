	<script>
		
		$(function () {
			$('#user_tables').DataTable();
		});
		
		$(document).ready(function(){
			$('#renovate_class').select2({
				theme: "bootstrap4",
			});
		});
	</script>
	<br>
	
	<div class="content">
		<div class="container">
			<div class="row">
				
				<div class="col-md-12">
					<h3>個人資訊設定</h3>
					<br>
					<hr>
				</div>
				<div class="col-md-12">
					<div class="row">
					<div class="col-5 col-sm-3">
						<div class="nav flex-column nav-tabs" id="vert-tabs-tab" role="tablist" aria-orientation="vertical">
						<a class="nav-link active" id="vert-tabs-home-tab" data-toggle="pill" href="#vert-tabs-home" role="tab" aria-controls="vert-tabs-home" aria-selected="false">個人設定</a>
						<a class="nav-link" id="vert-tabs-profile-tab" data-toggle="pill" href="#vert-tabs-profile" role="tab" aria-controls="vert-tabs-profile" aria-selected="false">密碼</a>
						</div>
					</div>
					<div class="col-5 col-sm-6">
						<div class="tab-content" id="vert-tabs-tabContent">
						<div class="tab-pane text-left fade active show" id="vert-tabs-home" role="tabpanel" aria-labelledby="vert-tabs-home-tab">
								<form class="form-signin" id="userform" method="post" role="form">
									<div class="form-group row">
										<div class="col-3">
											<label for="user_name" class="control-label">名稱</label>
										</div>
										<div class="col-9">
											<input type="text" name="user_name" id="user_name" class="form-control">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-3">
											<label for="user_mail" class="control-label">信箱</label>
										</div>
										<div class="col-9">
											<input type="text" name="user_mail" id="user_mail" class="form-control">
										</div>
									</div>
									
									<button type="submit" class="btn btn-primary">儲存</button>
								</form>
						</div>
						<div class="tab-pane fade" id="vert-tabs-profile" role="tabpanel" aria-labelledby="vert-tabs-profile-tab">
								<form class="form-signin" id="pwdform" method="post" role="form">
									<div class="form-group row">
										<div class="col-3">
											<label for="user_pwd" class="control-label">密碼</label>
										</div>
										<div class="col-9">
											<input type="text" name="user_pwd" id="user_pwd" class="form-control">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-3">
											<label for="user_checkpwd" class="control-label">確認密碼</label>
										</div>
										<div class="col-9">
											<input type="text" name="user_checkpwd" id="user_checkpwd" class="form-control">
										</div>
									</div>
									
									<button type="submit" class="btn btn-primary">儲存</button>
								</form>
						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	