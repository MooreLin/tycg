<?php
require("TCPDF/tcpdf.php");
class Myroll_tcpdf 
{

	function Myroll_tcpdf()
	{
		$this->ci =& get_instance();
	}
	
	public function tcpdf_html($html, $title)
	{	
		// create new PDF document
		$pdf = new TCPDF('L', PDF_UNIT, 'A4', true, 'UTF-8', false);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('');
		$pdf->SetTitle($title);
		$pdf->SetSubject('');
		$pdf->SetKeywords('');
		
		// set default header data
		$pdf->setPrintHeader(false);
		$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', 10));
		
		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
		// set margins
		//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetMargins(10, 10, 10);
		$pdf->SetHeaderMargin(0);
		$pdf->SetFooterMargin(10);
		
		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, 10);
		
		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		// set some language-dependent strings (optional)
		if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
			require_once(dirname(__FILE__).'/lang/eng.php');
			$pdf->setLanguageArray($l);
		}   
		
		// set font
		$pdf->SetFont('msjh','',1);

		foreach ($html as $item)
		{

		// add a page
		$pdf->AddPage();

		// output the HTML content
		$pdf->writeHTML($item, true, false, true, false, '');

		}

		// reset pointer to the last page
        $pdf->lastPage();

		// ---------------------------------------------------------

		//Close and output PDF document
		$pdf->Output("valuation.pdf", 'I');
	}	
	
}	

/* End of file Myroll_tcpdf.php */
/* Location: ./application/libraries/Myroll_tcpdf.php */
